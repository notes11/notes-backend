package pl.mm.notes;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.ResultActions;
import pl.mm.notes.feature.note.domain.cqrs.command.createnote.CreateNoteCommand;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.test.model.User;

import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.mm.notes.test.utils.TestUtils.getAuthorities;
import static pl.mm.notes.test.utils.TestUtils.getRoles;
import static pl.mm.notes.test.utils.TestUtils.objectToJson;

public abstract class RestBaseTest extends NotesApplicationTest {

    protected final ResultActions createUser(final CreateUserAccountCommand command) throws Exception {
        final ClassPathResource classPathResource = new ClassPathResource("test-data/create-user.json");
        final String content = Files.readString(classPathResource.getFile().toPath())
                .replace("${username}", command.getUsername())
                .replace("${password}", command.getPassword())
                .replace("${addressEmail}", command.getAddressEmail());
        return mockMvc.perform(post("/user")
                        .content(content)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());
    }

    protected final ResultActions createNote(CreateNoteCommand command, User user) throws Exception {
        return mockMvc.perform(post("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(command))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    protected final ResultActions createNoteImage(final UUID noteUuid, final User user, final MockMultipartFile image) throws Exception {
        return mockMvc.perform(multipart("/note/{noteUuid}/image".replace("{noteUuid}", noteUuid.toString()))
                        .file(image)
                        .contentType(MediaType.MULTIPART_MIXED_VALUE)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    protected final void createNote(List<CreateNoteCommand> commandList, pl.mm.notes.test.model.User user) throws Exception {
        for (CreateNoteCommand command : commandList) {
            createNote(command, user);
        }
    }

}
