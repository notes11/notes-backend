package pl.mm.notes.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;
import pl.mm.notes.core.domain.component.JwtConfiguration;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQueryHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilterTest extends RestBaseTest {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtConfiguration jwtConfiguration;
    @Autowired
    private FindUserByUsernameQueryHandler queryHandler;

    @Test
    public void testDoFilterInternalMissingHeader() throws ServletException, IOException {
        // given
        JwtAuthorizationFilter authorizationFilter = new JwtAuthorizationFilter(authenticationManager, jwtConfiguration, queryHandler);
        FilterChain filterChain = new MockFilterChain();
        HttpServletRequest request = new MockHttpServletRequest();
        HttpServletResponse response = new MockHttpServletResponse();

        // when
        authorizationFilter.doFilterInternal(request, response, filterChain);

        // then
        Assert.assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void testDoFilterInternalAuthorizationHeaderExistsEmpty() throws ServletException, IOException {
        // given
        JwtAuthorizationFilter authorizationFilter = new JwtAuthorizationFilter(authenticationManager, jwtConfiguration, queryHandler);
        FilterChain filterChain = new MockFilterChain();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(jwtConfiguration.getHeader(), "");
        HttpServletResponse response = new MockHttpServletResponse();

        // when
        authorizationFilter.doFilterInternal(request, response, filterChain);

        // then
        Assert.assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    @Test
    public void testDoFilterInternalAuthorizationHeaderExistsWithPrefix() throws ServletException, IOException {
        // given
        JwtAuthorizationFilter authorizationFilter = new JwtAuthorizationFilter(authenticationManager, jwtConfiguration, queryHandler);
        FilterChain filterChain = new MockFilterChain();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(jwtConfiguration.getHeader(), jwtConfiguration.getTokenPrefix());
        HttpServletResponse response = new MockHttpServletResponse();

        // when
        authorizationFilter.doFilterInternal(request, response, filterChain);

        // then
        Assert.assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

}