package pl.mm.notes.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;
import pl.mm.notes.core.domain.component.JwtConfiguration;
import pl.mm.notes.core.domain.service.jwt.JwtTokenGeneratorService;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQueryHandler;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationFilterTest extends RestBaseTest {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtConfiguration jwtConfiguration;
    @Autowired
    private FindUserByUsernameQueryHandler queryHandler;
    @Autowired
    private JwtTokenGeneratorService jwtTokenGeneratorService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testAttemptAuthentication() throws IOException {
        // given
        final String authenticationRequestBody = "{\n" +
                "  \"username\": \"" + getAdminUsername() + "\",\n" +
                "  \"password\": \"" + getAdminPassword() + "\"\n" +
                "}";

        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager, jwtConfiguration,
                jwtTokenGeneratorService, queryHandler, objectMapper);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContent(authenticationRequestBody.getBytes());
        HttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = new MockFilterChain();

        // when
        Authentication authentication = jwtAuthenticationFilter.attemptAuthentication(request, response);
        jwtAuthenticationFilter.successfulAuthentication(request, response, filterChain, authentication);

        // then
        final String authorizationHeader = response.getHeader(jwtConfiguration.getHeader());
        Assert.assertNotNull(authorizationHeader);
    }

    @Test(expectedExceptions = BadCredentialsException.class, expectedExceptionsMessageRegExp = "Failed to authenticate - not found 'username' in the request body")
    public void testAttemptAuthenticationNoUsernameInJson() {
        // given
        final String authenticationRequestBody = "{}";

        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager, jwtConfiguration,
                jwtTokenGeneratorService, queryHandler, objectMapper);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContent(authenticationRequestBody.getBytes());
        HttpServletResponse response = new MockHttpServletResponse();

        // when
        jwtAuthenticationFilter.attemptAuthentication(request, response);
    }

    @Test(expectedExceptions = BadCredentialsException.class, expectedExceptionsMessageRegExp = "Failed to authenticate - not found 'password' in the request body")
    public void testAttemptAuthenticationNoPasswordInJson() {
        // given
        final String authenticationRequestBody = "{\n" +
                "  \"username\": \"" + getAdminUsername() + "\"\n" +
                "}";

        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager, jwtConfiguration,
                jwtTokenGeneratorService, queryHandler, objectMapper);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContent(authenticationRequestBody.getBytes());
        HttpServletResponse response = new MockHttpServletResponse();

        // when
        jwtAuthenticationFilter.attemptAuthentication(request, response);
    }

    @Test(expectedExceptions = BadCredentialsException.class, expectedExceptionsMessageRegExp = "Failed to authenticate - could not load authentication body")
    public void testAttemptAuthenticationIncorrectJsonFormat() {
        // given
        final String authenticationRequestBody = "{" +
                "  \"username\": \"" + getAdminUsername() + "\", incorrect json\n" +
                "}";

        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(authenticationManager, jwtConfiguration,
                jwtTokenGeneratorService, queryHandler, objectMapper);
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContent(authenticationRequestBody.getBytes());
        HttpServletResponse response = new MockHttpServletResponse();

        // when
        jwtAuthenticationFilter.attemptAuthentication(request, response);
    }

}