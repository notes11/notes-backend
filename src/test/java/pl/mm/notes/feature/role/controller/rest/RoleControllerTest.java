package pl.mm.notes.feature.role.controller.rest;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;
import pl.mm.notes.core.domain.Role;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.test.data.UserTestData;
import pl.mm.notes.test.model.User;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertBaseInfoDtoResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertPageResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertRoleDtoResponse;
import static pl.mm.notes.test.utils.TestUtils.getAuthorities;
import static pl.mm.notes.test.utils.TestUtils.getRoles;

public class RoleControllerTest extends RestBaseTest {

    @Test
    public void testGetAllRoles() throws Exception {
        // given
        final User adminUser = UserTestData.ADMIN;

        // when & then
        final ResultActions resultActions = mockMvc.perform(get("/role")
                        .with(user(adminUser.getUsername())
                                .roles(getRoles(adminUser))
                                .authorities(getAuthorities(adminUser)))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        // then
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertRoleDtoResponse(resultActions, "content[0].");
    }

    @Test
    public void testGetAllRolesWithFilter() throws Exception {
        // given
        final User adminUser = UserTestData.ADMIN;
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(Role.USER)
                .build();

        // when & then
        final ResultActions resultActions = mockMvc.perform(get("/role")
                        .with(user(adminUser.getUsername())
                                .roles(getRoles(adminUser))
                                .authorities(getAuthorities(adminUser)))
                        .param("roleName", Role.USER)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("numberOfElements").value(1));

        // then
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertRoleDtoResponse(resultActions, "content[0].", expectedResult);
    }

}
