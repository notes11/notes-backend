package pl.mm.notes.feature.role.domain.entity;

import io.jsonwebtoken.lang.Assert;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.UUID;

public class RoleTest {

    @Test
    public void testRole() {
        Role role = Role.builder()
                .id(1L)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("")
                .createdTimestamp(Instant.now())
                .modifiedBy("")
                .modificationTimestamp(Instant.now())
                .entryRelatedToTable("")
                .roleName("")
                .build();
        Assert.notNull(role);
    }

}
