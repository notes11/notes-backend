package pl.mm.notes.feature.role.domain.cqrs.query.result;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;

import java.time.Instant;
import java.util.UUID;

public class RoleDtoCommandAndQueryResultTest {

    @Test
    public void testNoArgConstructor() {
        // when
        final RoleDtoCommandAndQueryResult defaultRoleQueryResult = new RoleDtoCommandAndQueryResult();

        // then
        Assert.assertNotNull(defaultRoleQueryResult);
        Assert.assertNull(defaultRoleQueryResult.getRoleName());
    }

    @Test
    public void testRoleNameOnlyConstructor() {
        // when
        final RoleDtoCommandAndQueryResult defaultRoleQueryResult = new RoleDtoCommandAndQueryResult("role-name");

        // then
        Assert.assertNotNull(defaultRoleQueryResult);
        Assert.assertNotNull(defaultRoleQueryResult.getRoleName());
    }

    @Test
    public void testAllArgConstructor() {
        // when
        final RoleDtoCommandAndQueryResult defaultRoleQueryResult = new RoleDtoCommandAndQueryResult(UUID.randomUUID(),
                "createdBy", Instant.now(), "modifiedBy", Instant.now(), "roleName");

        // then
        Assert.assertNotNull(defaultRoleQueryResult);
        Assert.assertNotNull(defaultRoleQueryResult.getRoleName());
        Assert.assertNotNull(defaultRoleQueryResult.getCreatedTimestamp());
    }

}