package pl.mm.notes.feature.acl.aclclass.domain.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.NotesApplicationTest;
import pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass;
import pl.mm.notes.feature.acl.aclclass.domain.repository.AclClassRepository;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.entity.NoteImage;

import java.util.ArrayList;
import java.util.List;

public class AclClassTableInitTest extends NotesApplicationTest {

    @Autowired
    private AclClassRepository aclClassRepository;
    @Autowired
    private AclClassTableInit aclClassTableInit;
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void shouldCreateAclClassEntries() {
        // given
        final int expectedNumberOfEntries = 2;
        // when
        final List<AclClass> aclClassList = aclClassRepository.findAll();
        // then
        Assert.assertEquals(aclClassList.size(), expectedNumberOfEntries);
    }

    @Test
    public void shouldNotCreateDuplicatesWithTheSecondRun() {
        // given
        //noinspection serial
        final List<String> expectedAclClassList = new ArrayList<>() {{
            add(Note.class.getName());
            add(NoteImage.class.getName());
        }};
        // when
        aclClassTableInit.onApplicationEvent(new ContextRefreshedEvent(applicationContext));
        aclClassTableInit.onApplicationEvent(new ContextRefreshedEvent(applicationContext));
        // then
        final List<AclClass> aclClassList = aclClassRepository.findAll();
        aclClassList.forEach(a -> {
            Assert.assertTrue(expectedAclClassList.contains(a.getAclClass()));
        });
    }

}