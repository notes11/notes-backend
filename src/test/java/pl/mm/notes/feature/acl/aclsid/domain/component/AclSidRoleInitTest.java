package pl.mm.notes.feature.acl.aclsid.domain.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.NotesApplicationTest;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid.SearchAclSidQuery;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.repository.AclSidRepository;
import pl.mm.notes.feature.acl.aclsid.domain.specification.AclSidSpecification;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static pl.mm.notes.core.domain.Role.ROLES;

public class AclSidRoleInitTest extends NotesApplicationTest {

    @Autowired
    private AclSidRepository aclSidRepository;
    @Autowired
    private AclSidRoleInit aclSidRoleInit;
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void verifyThatRolesHasBeenCreatedInAclSid() {
        final List<AclSid> aclSidList = aclSidRepository.findAll();
        Arrays.stream(ROLES).forEach(role -> {
            Optional<AclSid> aclSidOptional = aclSidList.stream().filter(aclSid -> aclSid.getSid().equals(role)).findFirst();
            Assert.assertTrue(aclSidOptional.isPresent());
        });
    }

    @Test
    public void testThatNoDuplicatesAreNotCreatedAfterSecondRun() {
        final int expectedNumberOfEntries = 2;
        aclSidRoleInit.onApplicationEvent(new ContextRefreshedEvent(applicationContext));
        final List<AclSid> aclSidList = aclSidRepository.findAll(new AclSidSpecification(SearchAclSidQuery.builder()
                .principal(false)
                .build()));
        Assert.assertEquals(aclSidList.size(), expectedNumberOfEntries);
    }

}