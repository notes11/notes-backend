package pl.mm.notes.feature.acl.aclsid.domain.specification;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class AclSidFilterTest {

    @Test
    public void testAclSidFilterDefaultConstructor() {
        AclSidFilter aclSidFilter = new AclSidFilter();
        assertNotNull(aclSidFilter);
    }

    @Test
    public void testAclSidFilterBuilder() {
        AclSidFilter aclSidFilter = AclSidFilter.builder()
                .principal(true)
                .sid("test")
                .build();

        assertNotNull(aclSidFilter.getPrincipal());
        assertNotNull(aclSidFilter.getSid());
    }

}