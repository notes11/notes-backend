package pl.mm.notes.feature.note.domain.dto;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.UUID;

public class NoteDtoCommandAndQueryResultTest {

    @Test
    public void testNoArgConstructor() {
        // when
        final NoteDtoCommandAndQueryResult noteDefaultQueryResult = new NoteDtoCommandAndQueryResult();
        // then
        Assert.assertNotNull(noteDefaultQueryResult);
        Assert.assertNull(noteDefaultQueryResult.getUuid());
        Assert.assertNull(noteDefaultQueryResult.getCreatedBy());
        Assert.assertNull(noteDefaultQueryResult.getCreatedTimestamp());
        Assert.assertNull(noteDefaultQueryResult.getModifiedBy());
        Assert.assertNull(noteDefaultQueryResult.getModificationTimestamp());
        Assert.assertNull(noteDefaultQueryResult.getTitle());
        Assert.assertNull(noteDefaultQueryResult.getNoteText());
    }

    @Test
    public void testBasicConstructor() {
        // given
        final String title = "Title";
        final String noteText = "Some note";
        final Boolean isPublic = false;
        // when
        final NoteDtoCommandAndQueryResult noteDefaultQueryResult = new NoteDtoCommandAndQueryResult(title, noteText, isPublic);
        // then
        Assert.assertNotNull(noteDefaultQueryResult);
        Assert.assertNull(noteDefaultQueryResult.getUuid());
        Assert.assertNull(noteDefaultQueryResult.getCreatedBy());
        Assert.assertNull(noteDefaultQueryResult.getCreatedTimestamp());
        Assert.assertNull(noteDefaultQueryResult.getModifiedBy());
        Assert.assertNull(noteDefaultQueryResult.getModificationTimestamp());
        Assert.assertEquals(noteDefaultQueryResult.getTitle(), title);
        Assert.assertEquals(noteDefaultQueryResult.getNoteText(), noteText);
        Assert.assertEquals(noteDefaultQueryResult.getIsPublic(), isPublic);
    }

    @Test
    public void testAllArgConstructor() {
        // given
        final UUID uuid = UUID.randomUUID();
        final String createdBy = "test";
        final Instant createdTimestamp = Instant.now();
        final String modifiedBy = "test";
        final Instant modificationTimestamp = Instant.now();
        final String title = "Title";
        final String noteText = "Some note";
        final Boolean isPublic = false;
        // when
        final NoteDtoCommandAndQueryResult noteDefaultQueryResult = new NoteDtoCommandAndQueryResult(uuid, createdBy, createdTimestamp,
                modifiedBy, modificationTimestamp, title, noteText, isPublic);
        // then
        Assert.assertNotNull(noteDefaultQueryResult);
        Assert.assertEquals(noteDefaultQueryResult.getUuid(), uuid);
        Assert.assertEquals(noteDefaultQueryResult.getCreatedBy(), createdBy);
        Assert.assertEquals(noteDefaultQueryResult.getCreatedTimestamp(), createdTimestamp);
        Assert.assertEquals(noteDefaultQueryResult.getModifiedBy(), modifiedBy);
        Assert.assertEquals(noteDefaultQueryResult.getModificationTimestamp(), modificationTimestamp);
        Assert.assertEquals(noteDefaultQueryResult.getTitle(), title);
        Assert.assertEquals(noteDefaultQueryResult.getNoteText(), noteText);
        Assert.assertEquals(noteDefaultQueryResult.getIsPublic(), isPublic);
    }

}