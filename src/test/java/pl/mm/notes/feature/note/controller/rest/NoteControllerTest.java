package pl.mm.notes.feature.note.controller.rest;

import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;
import pl.mm.notes.feature.acl.aclentry.domain.entity.AclEntry;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.note.domain.cqrs.command.createnote.CreateNoteCommand;
import pl.mm.notes.feature.note.domain.cqrs.command.updatenote.UpdateNoteCommand;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.entity.NoteImage;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.test.data.NoteTestData;
import pl.mm.notes.test.data.UserTestData;
import pl.mm.notes.test.model.User;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static pl.mm.notes.test.data.NoteTestData.CREATE_NOTE_COMMAND_LIST;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertBaseInfoDtoResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertErrorResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertNoteDtoResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertNoteImageDtoResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertPageResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertUserDtoResponse;
import static pl.mm.notes.test.utils.TestUtils.getAuthorities;
import static pl.mm.notes.test.utils.TestUtils.getRoles;
import static pl.mm.notes.test.utils.TestUtils.objectToJson;

public class NoteControllerTest extends RestBaseTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void shouldCreateNote() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(createNoteCommand.getTitle())
                .noteText(createNoteCommand.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(post("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(createNoteCommand))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        resultActions.andExpect(status().isCreated());
        assertBaseInfoDtoResponse(resultActions);
        assertNoteDtoResponse(resultActions, "", expectedResult);
        resultActions.andExpect(jsonPath("owner").hasJsonPath());
        assertUserDtoResponse(resultActions, "owner.");

        List<Note> noteList = noteRepository.findAll();
        assertFalse(noteList.isEmpty());
        Note note = noteList.get(0);

        assertNotNull(note.getUuid());
        assertNotNull(note.getVersion());
        assertEquals(note.getCreatedBy(), user.getUsername());
        assertNotNull(note.getCreatedTimestamp());
        assertEquals(note.getEntryRelatedToTable(), "note");
        assertEquals(note.getTitle(), createNoteCommand.getTitle());
        assertEquals(note.getNoteText(), createNoteCommand.getNoteText());
        assertNotNull(note.getOwner());
    }

    @Test
    public void shouldCreatePublicNote() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_PUBLIC_NOTE_COMMAND;

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(createNoteCommand.getTitle())
                .noteText(createNoteCommand.getNoteText())
                .isPublic(true)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(post("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(createNoteCommand))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        resultActions.andExpect(status().isCreated());
        assertBaseInfoDtoResponse(resultActions);
        assertNoteDtoResponse(resultActions, "", expectedResult);
        resultActions.andExpect(jsonPath("owner").hasJsonPath());
        assertUserDtoResponse(resultActions, "owner.");

        List<Note> noteList = noteRepository.findAll();
        assertFalse(noteList.isEmpty());
        Note note = noteList.get(0);

        assertNotNull(note.getUuid());
        assertNotNull(note.getVersion());
        assertEquals(note.getCreatedBy(), user.getUsername());
        assertNotNull(note.getCreatedTimestamp());
        assertEquals(note.getEntryRelatedToTable(), "note");
        assertEquals(note.getTitle(), createNoteCommand.getTitle());
        assertEquals(note.getNoteText(), createNoteCommand.getNoteText());
        assertTrue(note.getIsPublic());
        assertNotNull(note.getOwner());
    }

    @Test
    public void shouldCreateNoteAclEntryInAclObjectIdentity() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;

        // when
        mockMvc.perform(post("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(createNoteCommand))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());

        // then
        final List<Note> noteList = noteRepository.findAll();
        assertFalse(noteList.isEmpty());
        final Note note = noteList.get(0);

        final List<AclObjectIdentity> aclObjectIdentityList = aclObjectIdentityRepository.findAll();
        assertFalse(aclObjectIdentityList.isEmpty());
        final AclObjectIdentity aclObjectIdentity = aclObjectIdentityList.get(0);

        assertNotNull(aclObjectIdentity.getId());
        assertEquals(aclObjectIdentity.getAclClass().getAclClass(), Note.class.getName());
        assertEquals(aclObjectIdentity.getObjectIdIdentity(), note.getId());
        assertNull(aclObjectIdentity.getParentObject());
        assertEquals(aclObjectIdentity.getOwner().getSid(), note.getOwner().getUsername());
        assertFalse(aclObjectIdentity.getEntriesInheriting());

        final List<AclEntry> aclEntryRepositoryList = aclEntryRepository.findAll();
        assertFalse(aclEntryRepositoryList.isEmpty());
        final AclEntry aclEntry = aclEntryRepositoryList.get(0);

        assertNotNull(aclEntry.getId());
        assertNotNull(aclEntry.getAclObjectIdentity());
        assertEquals(aclEntry.getAclObjectIdentity().getId(), aclObjectIdentity.getId());
        assertNotNull(aclEntry.getAceOrder());
        assertNotNull(aclEntry.getSid());
        assertNotNull(aclEntry.getMask());
        assertNotNull(aclEntry.getGranting());
        assertNotNull(aclEntry.getAuditSuccess());
        assertNotNull(aclEntry.getAuditFailure());
    }

    @Test
    public void shouldDeleteNoteAndAlsoDeleteEntryInAclObjectIdentity() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;

        final MvcResult mvcResult = mockMvc.perform(post("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(createNoteCommand))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();

        final String noteUuid = JsonPath.parse(mvcResult.getResponse().getContentAsString())
                .read("uuid").toString();

        // when
        mockMvc.perform(delete("/note/" + noteUuid)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("uuid").isString())
                .andExpect(jsonPath("title").isString())
                .andExpect(jsonPath("noteText").isString());

        // then
        final List<Note> noteList = noteRepository.findAll();
        assertTrue(noteList.isEmpty());
        final List<AclObjectIdentity> aclObjectIdentityList = aclObjectIdentityRepository.findAll();
        assertTrue(aclObjectIdentityList.isEmpty());

        final List<AclEntry> aclEntryRepositoryList = aclEntryRepository.findAll();
        assertTrue(aclEntryRepositoryList.isEmpty());
    }

    @Test
    public void findNotesShouldBeAccessibleWithoutLogin() throws Exception {
        mockMvc.perform(get("/note"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void privateNotesShouldBeVisibleOnlyByItOwners() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note"))
                .andDo(print());
        //then
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("content[0]").doesNotHaveJsonPath())
                .andExpect(jsonPath("empty").value(true));
    }

    @Test
    public void publicNotesShouldBeVisibleToEveryone() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_PUBLIC_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(createNoteCommand.getTitle())
                .noteText(createNoteCommand.getNoteText())
                .isPublic(true)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note"))
                .andDo(print())
                .andExpect(status().isOk());

        // then
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);
    }

    @Test
    public void shouldFindNotes() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(createNoteCommand.getTitle())
                .noteText(createNoteCommand.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isOk());

        // then
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);
    }

    @Test
    public void shouldFindOnlyMyNotes() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        final CreateUserAccountCommand createUserAccountCommand2 = UserTestData.CREATE_USER_ACCOUNT_COMMAND_2;
        final User user2 = UserTestData.USER_2;
        createUser(createUserAccountCommand2);
        final CreateNoteCommand createNoteCommand2 = NoteTestData.CREATE_NOTE_COMMAND_2;
        createNote(createNoteCommand2, user2);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(createNoteCommand.getTitle())
                .noteText(createNoteCommand.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isOk());

        // then
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);

        final int expectedNumberOfElements = 1;
        resultActions.andExpect(jsonPath("numberOfElements").value(expectedNumberOfElements));
    }

    @Test
    public void shouldFindNoteByUuid() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);

        final CreateNoteCommand noteToFind = CreateNoteCommand.builder()
                .title("Special note")
                .noteText("Special test for this special note.")
                .build();
        createNote(noteToFind, user);

        final String uuidToFind;
        Optional<Note> note = noteRepository.findAll().stream().findFirst();
        uuidToFind = note.map(n -> n.getUuid().toString()).orElse(null);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(noteToFind.getTitle())
                .noteText(noteToFind.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("page", "0")
                        .param("size", "5")
                        .param("uuid", uuidToFind))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);
        resultActions.andExpect(jsonPath("numberOfElements").value(1));

        final int numberOfAllNotes = noteRepository.findAll().size();
        Assert.assertEquals(numberOfAllNotes, 1);
    }

    @Test
    public void shouldFindNoteByTitle() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);

        createNote(CREATE_NOTE_COMMAND_LIST, user);

        final CreateNoteCommand noteToFind = CreateNoteCommand.builder()
                .title("Special note")
                .noteText("Special test for this special note.")
                .build();
        createNote(noteToFind, user);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(noteToFind.getTitle())
                .noteText(noteToFind.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("page", "0")
                        .param("size", "4")
                        .param("title", noteToFind.getTitle()))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);
        resultActions.andExpect(jsonPath("numberOfElements").value(1));

        final int numberOfAllNotes = noteRepository.findAll().size();
        Assert.assertEquals(numberOfAllNotes, CREATE_NOTE_COMMAND_LIST.size() + 1);
    }

    @Test
    public void shouldFindNoteByNoteText() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);

        createNote(CREATE_NOTE_COMMAND_LIST, user);

        final CreateNoteCommand noteToFind = CreateNoteCommand.builder()
                .title("Special note")
                .noteText("Special test for this special note.")
                .build();
        createNote(noteToFind, user);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(noteToFind.getTitle())
                .noteText(noteToFind.getNoteText())
                .isPublic(false)
                .build();

        // when & then
        final ResultActions resultActions = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("page", "0")
                        .param("size", "5")
                        .param("noteText", noteToFind.getNoteText()))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);
        resultActions.andExpect(jsonPath("numberOfElements").value(1));

        final int numberOfAllNotes = noteRepository.findAll().size();
        Assert.assertEquals(numberOfAllNotes, CREATE_NOTE_COMMAND_LIST.size() + 1);
    }

    @Test
    public void shouldFindNoteByCreatedDateTime() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);

        createNote(CREATE_NOTE_COMMAND_LIST, user);

        final CreateNoteCommand noteToFind = CreateNoteCommand.builder()
                .title("Special note")
                .noteText("Special test for this special note.")
                .build();
        createNote(noteToFind, user);

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(noteToFind.getTitle())
                .noteText(noteToFind.getNoteText())
                .isPublic(false)
                .build();

        final String newDateTimeToSet = "2022-01-20 20:20:20";
        Optional<Note> noteOptional = noteRepository.findAll().stream()
                .filter(n -> n.getTitle().equals(noteToFind.getTitle()))
                .findFirst();

        //noinspection SqlResolve
        noteOptional.ifPresent(note -> jdbcTemplate.update("UPDATE base_info SET CreatedTimestamp = ? WHERE Uuid = ?",
                Timestamp.valueOf(newDateTimeToSet), note.getUuid()));

        final String createdTimestampAfter = "2022-01-20T19:20:20.000Z";
        final String createdTimestampBefore = "2022-01-20T21:20:20.000Z";

        // when & then
        final ResultActions resultActions = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("page", "0")
                        .param("size", "5")
                        .param("createdTimestampAfter", createdTimestampAfter)
                        .param("createdTimestampBefore", createdTimestampBefore))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertNoteDtoResponse(resultActions, "content[0].", expectedResult);
        resultActions.andExpect(jsonPath("numberOfElements").value(1));

        final int numberOfAllNotes = noteRepository.findAll().size();
        Assert.assertEquals(numberOfAllNotes, CREATE_NOTE_COMMAND_LIST.size() + 1);
    }

    @Test
    public void testUpdateNote() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        MvcResult mvcResult = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        final String noteUuid = JsonPath.parse(mvcResult.getResponse().getContentAsString())
                .read("content[0].uuid").toString();

        final UpdateNoteCommand updateNoteCommand = UpdateNoteCommand.builder()
                .noteUuid(UUID.fromString(noteUuid))
                .title("Some new title")
                .noteText("Some new note")
                .build();

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(updateNoteCommand.getTitle())
                .noteText(updateNoteCommand.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(put("/note/" + noteUuid)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(updateNoteCommand))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertBaseInfoDtoResponse(resultActions);
        assertNoteDtoResponse(resultActions, "", expectedResult);

        Optional<Note> noteOptional = noteRepository.findByUuid(UUID.fromString(noteUuid));
        assertTrue(noteOptional.isPresent());
        Note note = noteOptional.get();

        assertNotNull(note.getUuid());
        assertNotNull(note.getVersion());
        assertNotNull(note.getCreatedBy());
        assertNotNull(note.getCreatedTimestamp());
        assertEquals(note.getEntryRelatedToTable(), "note");
        assertEquals(note.getTitle(), updateNoteCommand.getTitle());
        assertEquals(note.getNoteText(), updateNoteCommand.getNoteText());
        assertNotNull(note.getOwner());
    }

    @Test
    public void shouldFailedToUpdateNoneExistingNote() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);

        final String uuidOfNoneExistingNote = UUID.randomUUID().toString();
        final UpdateNoteCommand updateNoteCommand = UpdateNoteCommand.builder()
                .noteUuid(UUID.fromString(uuidOfNoneExistingNote))
                .title("Some new title")
                .noteText("Some new note")
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(put("/note/" + updateNoteCommand.getNoteUuid())
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .content(objectToJson(updateNoteCommand))
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        resultActions.andExpect(status().isNotFound());
        assertErrorResponse(resultActions, HttpStatus.NOT_FOUND);
    }

    @Test
    public void testDeleteNoteConfirmed() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        MvcResult mvcResult = mockMvc.perform(get("/note")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        final String noteUuid = JsonPath.parse(mvcResult.getResponse().getContentAsString())
                .read("content[0].uuid").toString();

        final NoteDtoCommandAndQueryResult expectedResult = NoteDtoCommandAndQueryResult.builder()
                .title(createNoteCommand.getTitle())
                .noteText(createNoteCommand.getNoteText())
                .isPublic(false)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(delete("/note/" + noteUuid)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isNoContent());
        assertBaseInfoDtoResponse(resultActions);
        assertNoteDtoResponse(resultActions, "", expectedResult);

        final int expectedNumberOfNotes = 0;
        List<Note> noteList = noteRepository.findAll();
        Assert.assertEquals(noteList.size(), expectedNumberOfNotes);
    }

    @Test
    public void testDeleteNoteUserNotLoggedIn() throws Exception {
        // given
        final String noteUuid = UUID.randomUUID().toString();

        // when & then
        mockMvc.perform(delete("/note/" + noteUuid))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldAddImageToNote() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        final ResultActions resultActions = createNote(createNoteCommand, user);
        final NoteDtoCommandAndQueryResult noteDtoResult = objectMapper
                .readValue(resultActions.andReturn().getResponse().getContentAsString(), NoteDtoCommandAndQueryResult.class);

        final ClassPathResource classPathImage = new ClassPathResource("icons8-note-taking-96.png");
        final MockMultipartFile image = new MockMultipartFile("image", classPathImage.getFilename(), MediaType.MULTIPART_MIXED_VALUE, classPathImage.getInputStream());

        final NoteImageDtoCommandAndQueryResult expectedResult = NoteImageDtoCommandAndQueryResult.builder()
                .imageName(classPathImage.getFilename())
                .build();

        //when
        final ResultActions createNoteImageResultActions = createNoteImage(noteDtoResult.getUuid(), user, image);

        //then
        assertBaseInfoDtoResponse(createNoteImageResultActions);
        assertNoteImageDtoResponse(createNoteImageResultActions, "", expectedResult);

        final List<NoteImage> noteImageList = noteImageRepository.findAll();
        Assert.assertEquals(noteImageList.size(), 1);
        final NoteImage noteImage = noteImageList.get(0);
        Assert.assertEquals(noteImage.getImageName(), classPathImage.getFilename());
    }

    @Test
    public void shouldFiledToAddImageToNote() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        createNote(createNoteCommand, user);

        final ClassPathResource classPathImage = new ClassPathResource("icons8-note-taking-96.png");
        final MockMultipartFile image = new MockMultipartFile("image", classPathImage.getFilename(), MediaType.MULTIPART_MIXED_VALUE, classPathImage.getInputStream());
        final UUID someNotExistingUuid = UUID.randomUUID();

        //when
        final ResultActions resultActions = mockMvc.perform(
                        multipart("/note/{noteUuid}/image".replace("{noteUuid}", someNotExistingUuid.toString()))
                                .file(image)
                                .contentType(MediaType.MULTIPART_MIXED_VALUE)
                                .with(user(user.getUsername())
                                        .roles(getRoles(user))
                                        .authorities(getAuthorities(user))))
                .andDo(print());

        //then
        resultActions.andExpect(status().isNotFound());
        assertErrorResponse(resultActions, HttpStatus.NOT_FOUND);
        final List<NoteImage> noteImageList = noteImageRepository.findAll();
        Assert.assertTrue(noteImageList.isEmpty());
    }

    @Test
    public void shouldFindImages() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        final ResultActions resultActions = createNote(createNoteCommand, user);
        final NoteDtoCommandAndQueryResult noteDtoResult = objectMapper
                .readValue(resultActions.andReturn().getResponse().getContentAsString(), NoteDtoCommandAndQueryResult.class);

        final ClassPathResource classPathImage = new ClassPathResource("icons8-note-taking-96.png");
        final MockMultipartFile image = new MockMultipartFile("image", classPathImage.getFilename(), MediaType.MULTIPART_MIXED_VALUE, classPathImage.getInputStream());

        createNoteImage(noteDtoResult.getUuid(), user, image);
        createNoteImage(noteDtoResult.getUuid(), user, image);

        //when
        mockMvc.perform(get("/note/image")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("page", "0")
                        .param("size", "5")
                        .param("imageName", classPathImage.getFilename()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("content[0].uuid").hasJsonPath())
                .andExpect(jsonPath("content[0].imageName").hasJsonPath())
                .andExpect(jsonPath("numberOfElements").value(2));

        // then
        final List<NoteImage> noteImageList = noteImageRepository.findAll();
        Assert.assertEquals(noteImageList.size(), 2);
        final NoteImage noteImage = noteImageList.get(0);
        Assert.assertEquals(noteImage.getImageName(), classPathImage.getFilename());
    }

    @Test
    public void shouldDownloadNoteImage() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        final ResultActions resultActions = createNote(createNoteCommand, user);
        final NoteDtoCommandAndQueryResult noteDtoResult = objectMapper
                .readValue(resultActions.andReturn().getResponse().getContentAsString(), NoteDtoCommandAndQueryResult.class);

        final ClassPathResource classPathImage = new ClassPathResource("icons8-note-taking-96.png");
        final MockMultipartFile image = new MockMultipartFile("image", classPathImage.getFilename(), MediaType.MULTIPART_MIXED_VALUE, classPathImage.getInputStream());

        createNoteImage(noteDtoResult.getUuid(), user, image);

        final ResultActions imageNameResultActions = mockMvc.perform(get("/note/image")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("imageName", classPathImage.getFilename()))
                .andDo(print())
                .andExpect(status().isOk());

        final String imageContentAsString = imageNameResultActions.andReturn().getResponse().getContentAsString();
        final String imageUuidAsString = objectMapper.readTree(imageContentAsString).get("content")
                .get(0)
                .get("uuid").asText();
        final UUID imageUuid = UUID.fromString(imageUuidAsString);

        // when
        mockMvc.perform(get("/note/image/" + imageUuid)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(result -> Assert.assertFalse(result.getResponse().getContentAsString().isEmpty()));
    }

    @Test
    public void shouldDeleteImage() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        final ResultActions resultActionsCreateNote = createNote(createNoteCommand, user);
        final NoteDtoCommandAndQueryResult noteDtoResult = objectMapper
                .readValue(resultActionsCreateNote.andReturn().getResponse().getContentAsString(), NoteDtoCommandAndQueryResult.class);

        final ClassPathResource classPathImage = new ClassPathResource("icons8-note-taking-96.png");
        final MockMultipartFile image = new MockMultipartFile("image", classPathImage.getFilename(), MediaType.MULTIPART_MIXED_VALUE, classPathImage.getInputStream());

        createNoteImage(noteDtoResult.getUuid(), user, image);

        final ResultActions imageNameResultActions = mockMvc.perform(get("/note/image")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("imageName", classPathImage.getFilename()))
                .andDo(print())
                .andExpect(status().isOk());

        final String imageContentAsString = imageNameResultActions.andReturn().getResponse().getContentAsString();
        final String imageUuidAsString = objectMapper.readTree(imageContentAsString).get("content")
                .get(0)
                .get("uuid").asText();
        final UUID imageUuid = UUID.fromString(imageUuidAsString);

        // when
        final ResultActions resultActions = mockMvc.perform(delete("/note/image/{imageUuid}", imageUuid)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print());
        // then
        resultActions.andExpect(status().isNoContent());
        assertBaseInfoDtoResponse(resultActions);
        assertNoteImageDtoResponse(resultActions);

        final List<NoteImage> noteImageList = noteImageRepository.findAll();
        Assert.assertTrue(noteImageList.isEmpty());
    }

    @Test
    public void shouldFiledToDownloadNotExistingNoteImage() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final User user = UserTestData.USER;
        createUser(createUserAccountCommand);
        final CreateNoteCommand createNoteCommand = NoteTestData.CREATE_NOTE_COMMAND;
        final ResultActions resultActionsCreateNote = createNote(createNoteCommand, user);
        final NoteDtoCommandAndQueryResult noteDtoResult = objectMapper
                .readValue(resultActionsCreateNote.andReturn().getResponse().getContentAsString(), NoteDtoCommandAndQueryResult.class);

        final ClassPathResource classPathImage = new ClassPathResource("icons8-note-taking-96.png");
        final MockMultipartFile image = new MockMultipartFile("image", classPathImage.getFilename(), MediaType.MULTIPART_MIXED_VALUE, classPathImage.getInputStream());

        createNoteImage(noteDtoResult.getUuid(), user, image);

        mockMvc.perform(get("/note/image")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user)))
                        .param("imageName", classPathImage.getFilename()))
                .andDo(print())
                .andExpect(status().isOk());

        final UUID notExistingUuid = UUID.randomUUID();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/note/image/" + notExistingUuid)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print());

        //then
        resultActions.andExpect(status().isNotFound());
        assertErrorResponse(resultActions, HttpStatus.NOT_FOUND);
    }
}