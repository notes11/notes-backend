package pl.mm.notes.feature.note.domain.entity;

import io.jsonwebtoken.lang.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.feature.user.domain.entity.User;

import java.time.Instant;
import java.util.UUID;

public class NoteTest {

    @Test
    public void testNote() {
        Note note = Note.builder()
                .id(1L)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("")
                .createdTimestamp(Instant.now())
                .modifiedBy("")
                .modificationTimestamp(Instant.now())
                .entryRelatedToTable("")
                .title("")
                .noteText("")
                .owner(new User())
                .build();
        Assert.notNull(note);
    }

}
