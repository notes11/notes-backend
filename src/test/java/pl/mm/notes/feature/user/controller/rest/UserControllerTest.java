package pl.mm.notes.feature.user.controller.rest;

import com.jayway.jsonpath.JsonPath;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;
import pl.mm.notes.core.domain.Role;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.feature.user.domain.cqrs.command.updateuser.UpdateUserCommand;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.test.data.UserTestData;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;
import static pl.mm.notes.core.domain.Role.ADMIN;
import static pl.mm.notes.core.domain.Role.USER;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertBaseInfoDtoResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertErrorResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertPageResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertRoleDtoResponse;
import static pl.mm.notes.test.utils.AssertDtoResponse.assertUserDtoResponse;
import static pl.mm.notes.test.utils.TestUtils.getAuthorities;
import static pl.mm.notes.test.utils.TestUtils.getRoles;
import static pl.mm.notes.test.utils.TestUtils.objectToJson;

public class UserControllerTest extends RestBaseTest {

    @Test
    public void shouldCreateUserAccount() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(createUserAccountCommand.getUsername())
                .addressEmail(createUserAccountCommand.getAddressEmail())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedUserRoleResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(USER)
                .build();

        // when
        final ResultActions resultActions = createUser(createUserAccountCommand)
                .andExpect(status().isCreated());

        // then
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[0].");
        assertRoleDtoResponse(resultActions, "roles[0].", expectedUserRoleResult);

        Optional<User> createdUser = userRepository.findByUsername(createUserAccountCommand.getUsername());

        assertTrue(createdUser.isPresent());
        assertEquals(createdUser.get().getUsername(), createUserAccountCommand.getUsername());
        assertEquals(createdUser.get().getAddressEmail(), createUserAccountCommand.getAddressEmail());
        int numberOfExpectedRoles = 1;
        assertEquals(createdUser.get().getRoles().size(), numberOfExpectedRoles);
    }

    @Test
    public void shouldFailToCreateUserAccountWithEmptyRequest() throws Exception {
        // given

        // when
        final ResultActions resultActions = mockMvc.perform(post("/user")
                        .content("")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print());

        // then
        assertErrorResponse(resultActions, HttpStatus.BAD_REQUEST);

        List<User> userList = userRepository.findAll();
        int numberOfExpectedUsers = 1;
        assertEquals(userList.size(), numberOfExpectedUsers);
    }

    @Test
    public void shouldFailToCreateUserAccountWithEmptyFields() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = CreateUserAccountCommand.builder()
                .username("")
                .password("")
                .addressEmail("")
                .build();

        // when
        final ResultActions resultActions = createUser(createUserAccountCommand);

        // then
        assertErrorResponse(resultActions, HttpStatus.BAD_REQUEST);

        List<User> userList = userRepository.findAll();
        int numberOfExpectedUsers = 1;
        assertEquals(userList.size(), numberOfExpectedUsers);
    }

    @Test
    public void shouldSearchUsers() throws Exception {
        // given
        final pl.mm.notes.test.model.User adminUser = UserTestData.ADMIN;

        // when & then
        ResultActions resultActions = mockMvc.perform(get("/user/search")
                        .with(user(adminUser.getUsername())
                                .roles(getRoles(adminUser))
                                .authorities(getAuthorities(adminUser))))
                .andDo(print())
                .andExpect(status().isOk());


        // then
        assertPageResponse(resultActions);
        assertBaseInfoDtoResponse(resultActions, "content[0].");
        assertUserDtoResponse(resultActions, "content[0].");
        assertBaseInfoDtoResponse(resultActions, "content[0].roles[0].");
        assertRoleDtoResponse(resultActions, "content[0].roles[0].");

        final String roles = JsonPath.parse(resultActions.andReturn().getResponse().getContentAsString())
                .read("content[0].roles[*]").toString();

        assertTrue(roles.contains(Role.ADMIN));
        assertTrue(roles.contains(Role.USER));
    }

    @Test
    public void shouldFailedToSearchUsersWhenNotAuthenticated() throws Exception {
        // when & then
        mockMvc.perform(get("/user/search"))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldFailedToSearchUsersWithUserRole() throws Exception {
        // given
        final pl.mm.notes.test.model.User user = UserTestData.USER;

        // when & then
        mockMvc.perform(get("/user/search")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldGetInformationAboutLoggedUser() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        createUser(createUserAccountCommand);

        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(createUserAccountCommand.getUsername())
                .addressEmail(createUserAccountCommand.getAddressEmail())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(UserTestData.USER.getRoles().get(0))
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(get("/user")
                        .with(user(UserTestData.USER.getUsername())
                                .roles(getRoles(UserTestData.USER))
                                .authorities(getAuthorities(UserTestData.USER))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[0].");
        assertRoleDtoResponse(resultActions, "roles[0].", expectedResult);
        resultActions.andExpect(jsonPath("roles[1].roleName").doesNotExist());
    }

    @Test
    public void shouldFailedToGetInformationAboutLoggedUserWhenNotAuthenticated() throws Exception {
        // when & then
        mockMvc.perform(get("/user"))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldFailedToGetInformationAboutLoggedUserNoneExistingUser() throws Exception {
        // when
        final ResultActions resultActions = mockMvc.perform(get("/user")
                        .with(user(UserTestData.USER.getUsername())
                                .roles(getRoles(UserTestData.USER))
                                .authorities(getAuthorities(UserTestData.USER))))
                .andDo(print())
                .andExpect(status().isNotFound());
        // then
        resultActions.andExpect(jsonPath("timestamp").isString());
        assertErrorResponse(resultActions, HttpStatus.NOT_FOUND, "User 'test-user' not found");
    }

    @Test
    public void shouldDeleteMyAccount() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final pl.mm.notes.test.model.User user = UserTestData.USER;
        createUser(createUserAccountCommand);

        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(createUserAccountCommand.getUsername())
                .addressEmail(createUserAccountCommand.getAddressEmail())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(UserTestData.USER.getRoles().get(0))
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(delete("/user")
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isNoContent());
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[0].");
        assertRoleDtoResponse(resultActions, "roles[0].", expectedResult);

        Optional<User> deletedUser = userRepository.findByUsername(createUserAccountCommand.getUsername());
        assertTrue(deletedUser.isEmpty());
    }

    @Test
    public void shouldChangePassword() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final pl.mm.notes.test.model.User user = UserTestData.USER;
        final String newPassword = "{\"newPassword\": \"newPassword\"}";

        createUser(createUserAccountCommand);

        Optional<User> createdUser = userRepository.findByUsername(createUserAccountCommand.getUsername());
        assertTrue(createdUser.isPresent());
        final String userUuid = createdUser.get().getUuid().toString();

        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(createUserAccountCommand.getUsername())
                .addressEmail(createUserAccountCommand.getAddressEmail())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(UserTestData.USER.getRoles().get(0))
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(patch("/user/" + userUuid + "/change-password")
                        .content(newPassword)
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[0].");
        assertRoleDtoResponse(resultActions, "roles[0].", expectedResult);

        Optional<User> userWithUpdatedPassword = userRepository.findByUsername(createUserAccountCommand.getUsername());
        assertTrue(userWithUpdatedPassword.isPresent());
        assertNotEquals(userWithUpdatedPassword.get().getPassword(), createdUser.get().getPassword());
    }

    @Test
    public void shouldLoginAsAdmin() throws Exception {
        // given
        final String loginJson = "{" +
                "\"username\": \"" + getAdminUsername() + "\"," +
                "\"password\": \"" + getAdminPassword() + "\"" +
                "}";

        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(UserTestData.ADMIN.getUsername())
                .addressEmail(UserTestData.ADMIN.getAddressEmail())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(ADMIN)
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(post("/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(loginJson))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        resultActions.andExpect(header().exists("Authorization"));
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[1].");
        assertRoleDtoResponse(resultActions, "roles[1].", expectedResult);
    }

    @Test
    public void shouldAssignRoleToUser() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final pl.mm.notes.test.model.User adminUser = UserTestData.ADMIN;

        createUser(createUserAccountCommand);
        Optional<User> createdUser = userRepository.findByUsername(createUserAccountCommand.getUsername());
        assertTrue(createdUser.isPresent());
        final String userUuid = createdUser.get().getUuid().toString();

        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(createUserAccountCommand.getUsername())
                .addressEmail(createUserAccountCommand.getAddressEmail())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(UserTestData.USER.getRoles().get(0))
                .build();

        // when
        final ResultActions resultActions = mockMvc.perform(patch("/user/" + userUuid + "/assign-role/" + ADMIN)
                        .with(user(adminUser.getUsername())
                                .roles(getRoles(adminUser))
                                .authorities(getAuthorities(adminUser))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[0].");
        assertRoleDtoResponse(resultActions, "roles[0].", expectedResult);
        resultActions.andExpect(jsonPath("roles[1].roleName").isString());

        Optional<User> createdUserWithAssignRole = userRepository.findByUsername(createdUser.get().getUsername());
        assertTrue(createdUserWithAssignRole.isPresent());
        int numberOfExpectedRolesAfterAssign = 2;
        assertEquals(createdUserWithAssignRole.get().getRoles().size(), numberOfExpectedRolesAfterAssign);
    }

    @Test
    public void shouldBeFailedOnTryingAddNoneExistingRoleToUser() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final pl.mm.notes.test.model.User adminUser = UserTestData.ADMIN;

        createUser(createUserAccountCommand);
        Optional<User> createdUser = userRepository.findByUsername(createUserAccountCommand.getUsername());
        assertTrue(createdUser.isPresent());
        final String userUuid = createdUser.get().getUuid().toString();

        // when
        final ResultActions resultActions = mockMvc.perform(patch("/user/" + userUuid + "/assign-role/SOME_NONE_EXISTING_ROLE")
                        .with(user(adminUser.getUsername())
                                .roles(getRoles(adminUser))
                                .authorities(getAuthorities(adminUser))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isNotFound());
        assertErrorResponse(resultActions, HttpStatus.NOT_FOUND, "Role 'SOME_NONE_EXISTING_ROLE' not found");

        Optional<User> createdUserWithAssignRole = userRepository.findByUsername(createdUser.get().getUsername());
        assertTrue(createdUserWithAssignRole.isPresent());
        int numberOfExpectedRolesAfterAssign = 1;
        assertEquals(createdUserWithAssignRole.get().getRoles().size(), numberOfExpectedRolesAfterAssign);
        assertEquals(new ArrayList<>(createdUserWithAssignRole.get().getRoles()).get(0).getRoleName(), USER);
    }

    @Test
    public void shouldCreateUserAndAddAclSidEntry() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;

        // when
        createUser(createUserAccountCommand);

        // then
        final int numberOfExpectedEntries = 4;
        final List<AclSid> aclSidList = aclSidRepository.findAll();
        Assert.assertEquals(aclSidList.size(), numberOfExpectedEntries);
        Optional<AclSid> aclSidOptional = aclSidList.stream()
                .filter(aclSid -> aclSid.getSid().equals(createUserAccountCommand.getUsername()))
                .findFirst();
        Assert.assertTrue(aclSidOptional.isPresent());
        Assert.assertEquals(aclSidOptional.get().getSid(), createUserAccountCommand.getUsername());
    }

    @Test
    public void testUpdateUser() throws Exception {
        // given
        final CreateUserAccountCommand createUserAccountCommand = UserTestData.CREATE_USER_ACCOUNT_COMMAND;
        final UpdateUserCommand updateUserCommand = UpdateUserCommand.builder()
                .firstName("updated-first-name")
                .lastName("updated-last-name")
                .build();
        final pl.mm.notes.test.model.User user = UserTestData.USER;

        createUser(createUserAccountCommand);

        final UserDtoCommandAndQueryResult expectedUserResponse = UserDtoCommandAndQueryResult.builder()
                .username(createUserAccountCommand.getUsername())
                .addressEmail(createUserAccountCommand.getAddressEmail())
                .firstName(updateUserCommand.getFirstName())
                .lastName(updateUserCommand.getLastName())
                .isActive(false)
                .build();
        final RoleDtoCommandAndQueryResult expectedResult = RoleDtoCommandAndQueryResult.builder()
                .roleName(UserTestData.USER.getRoles().get(0))
                .build();


        // when
        final ResultActions resultActions = mockMvc.perform(patch("/user")
                        .content(objectToJson(updateUserCommand))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(user.getUsername())
                                .roles(getRoles(user))
                                .authorities(getAuthorities(user))))
                .andDo(print());

        // then
        resultActions.andExpect(status().isOk());
        assertBaseInfoDtoResponse(resultActions);
        assertUserDtoResponse(resultActions, "", expectedUserResponse);
        assertBaseInfoDtoResponse(resultActions, "roles[0].");
        assertRoleDtoResponse(resultActions, "roles[0].", expectedResult);

        Optional<pl.mm.notes.feature.user.domain.entity.User> updatedUser = userRepository.findByUsername(createUserAccountCommand.getUsername());
        assertTrue(updatedUser.isPresent());
        assertEquals(updatedUser.get().getFirstName(), updateUserCommand.getFirstName());
        assertEquals(updatedUser.get().getLastName(), updateUserCommand.getLastName());
    }

}