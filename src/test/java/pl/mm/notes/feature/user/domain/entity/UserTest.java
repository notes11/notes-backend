package pl.mm.notes.feature.user.domain.entity;

import io.jsonwebtoken.lang.Assert;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.UUID;

public class UserTest {

    @Test
    public void testUser() {
        User user = User.builder()
                .id(1L)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("")
                .createdTimestamp(Instant.now())
                .modifiedBy("")
                .modificationTimestamp(Instant.now())
                .entryRelatedToTable("")
                .username("")
                .password("")
                .firstName("")
                .lastName("")
                .addressEmail("")
                .isActive(false)
                .roles(new LinkedHashSet<>())
                .notes(new ArrayList<>())
                .build();
        Assert.notNull(user);
    }

}
