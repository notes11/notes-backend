package pl.mm.notes.feature.appinfo.controller.rest.query;

import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EnvironmentControllerTest extends RestBaseTest {

    @Test
    public void testGetEnvironment() throws Exception {
        // given & when & then
        mockMvc.perform((get("/environment")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("environment").exists())
                .andExpect(jsonPath("environment").value("local"));
    }
}