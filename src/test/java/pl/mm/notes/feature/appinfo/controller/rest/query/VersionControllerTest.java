package pl.mm.notes.feature.appinfo.controller.rest.query;

import org.testng.annotations.Test;
import pl.mm.notes.RestBaseTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VersionControllerTest extends RestBaseTest {

    @Test
    public void testGetVersion() throws Exception {
        // when & then
        mockMvc.perform(get("/version"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("version").exists());
    }

}
