package pl.mm.notes.core.domain.exception;

public abstract class AbstractHttpNotesRuntimeExceptionTest {

    public abstract void shouldThrowExceptionUsingDefaultConstructor();

    public abstract void shouldThrowExceptionUsingConstructorWithMessage();

    public abstract void shouldThrowExceptionUsingConstructorWithMessageAndThrowable();

    public abstract void shouldThrowExceptionUsingConstructorWithThrowable();

    public abstract void shouldThrowExceptionUsingConstructorWithAllArgsConstructor();
}