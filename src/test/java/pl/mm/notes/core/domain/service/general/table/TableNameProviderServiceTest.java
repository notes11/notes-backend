package pl.mm.notes.core.domain.service.general.table;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.mm.notes.NotesApplicationTest;
import pl.mm.notes.core.domain.service.table.BaseInfoTableNameProviderService;
import pl.mm.notes.core.domain.service.table.NoteTableNameProviderService;
import pl.mm.notes.core.domain.service.table.RoleTableNameProviderService;
import pl.mm.notes.core.domain.service.table.TableNameProviderService;
import pl.mm.notes.core.domain.service.table.UserTableNameProviderService;

public class TableNameProviderServiceTest extends NotesApplicationTest {

    @Autowired
    private BaseInfoTableNameProviderService baseInfoTableNameProvider;
    @Autowired
    private UserTableNameProviderService userTableNameProvider;
    @Autowired
    private RoleTableNameProviderService roleTableNameProvider;
    @Autowired
    private NoteTableNameProviderService noteTableNameProvider;

    @Test(dataProvider = "testDataProvider")
    public void testGetTableName(String expectedName, TableNameProviderService tableNameProviderService) {
        String actualName = tableNameProviderService.getTableName();
        Assert.assertEquals(expectedName, actualName);
    }

    @DataProvider
    private Object[][] testDataProvider() {
        return new Object[][]{
                {"base_info", baseInfoTableNameProvider},
                {"user", userTableNameProvider},
                {"role", roleTableNameProvider},
                {"note", noteTableNameProvider}
        };
    }


}
