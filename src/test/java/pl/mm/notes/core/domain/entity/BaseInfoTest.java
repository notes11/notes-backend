package pl.mm.notes.core.domain.entity;

import io.jsonwebtoken.lang.Assert;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.UUID;

public class BaseInfoTest {

    @Test
    public void testBaseInfo() {
        BaseInfo baseInfo = BaseInfo.builder()
                .id(1L)
                .uuid(UUID.randomUUID())
                .version(1L)
                .createdBy("")
                .createdTimestamp(Instant.now())
                .modifiedBy("")
                .modificationTimestamp(Instant.now())
                .entryRelatedToTable("")
                .build();
        Assert.notNull(baseInfo);
    }

}
