package pl.mm.notes.core.domain.exception;

import org.testng.annotations.Test;

public class Http404NotFoundRuntimeExceptionTest extends AbstractHttpNotesRuntimeExceptionTest {

    @Test(expectedExceptions = {Http404NotFoundRuntimeException.class})
    @Override
    public void shouldThrowExceptionUsingDefaultConstructor() {
        throw new Http404NotFoundRuntimeException();
    }

    @Test(expectedExceptions = {Http404NotFoundRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 404 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithMessage() {
        throw new Http404NotFoundRuntimeException("Some 404 error");
    }

    @Test(expectedExceptions = {Http404NotFoundRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 404 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithMessageAndThrowable() {
        throw new Http404NotFoundRuntimeException("Some 404 error", new Exception("Some 404 error from Throwable"));
    }

    @Test(expectedExceptions = {Http404NotFoundRuntimeException.class}, expectedExceptionsMessageRegExp = "java.lang.Exception: Some 404 error from Throwable")
    @Override
    public void shouldThrowExceptionUsingConstructorWithThrowable() {
        throw new Http404NotFoundRuntimeException(new Exception("Some 404 error from Throwable"));
    }

    @Test(expectedExceptions = {Http404NotFoundRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 404 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithAllArgsConstructor() {
        throw new Http404NotFoundRuntimeException("Some 404 error", new Exception("Some 404 error from Throwable"), false, false);
    }

}