package pl.mm.notes.core.domain.exception;

import org.testng.annotations.Test;

public class Http403ForbiddenRuntimeExceptionTest extends AbstractHttpNotesRuntimeExceptionTest {

    @Test(expectedExceptions = {Http403ForbiddenRuntimeException.class})
    @Override
    public void shouldThrowExceptionUsingDefaultConstructor() {
        throw new Http403ForbiddenRuntimeException();
    }

    @Test(expectedExceptions = {Http403ForbiddenRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 403 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithMessage() {
        throw new Http403ForbiddenRuntimeException("Some 403 error");
    }

    @Test(expectedExceptions = {Http403ForbiddenRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 403 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithMessageAndThrowable() {
        throw new Http403ForbiddenRuntimeException("Some 403 error", new Exception("Some 403 error from Throwable"));
    }

    @Test(expectedExceptions = {Http403ForbiddenRuntimeException.class}, expectedExceptionsMessageRegExp = "java.lang.Exception: Some 403 error from Throwable")
    @Override
    public void shouldThrowExceptionUsingConstructorWithThrowable() {
        throw new Http403ForbiddenRuntimeException(new Exception("Some 403 error from Throwable"));
    }

    @Test(expectedExceptions = {Http403ForbiddenRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 403 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithAllArgsConstructor() {
        throw new Http403ForbiddenRuntimeException("Some 403 error", new Exception("Some 403 error from Throwable"), false, false);
    }
}