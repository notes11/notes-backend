package pl.mm.notes.core.domain.entity.converter;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class UUIDConverterTest {

    @Test
    public void testConvertToDatabaseColumn() {
        UUID uuid = UUID.randomUUID();
        UUIDConverter uuidConverter = new UUIDConverter();
        String databaseColumn = uuidConverter.convertToDatabaseColumn(uuid);
        Assert.assertEquals(databaseColumn, uuid.toString());
    }

    @Test
    public void testConvertToEntityAttribute() {
        String databaseColumn = UUID.randomUUID().toString();
        UUIDConverter uuidConverter = new UUIDConverter();
        UUID uuid = uuidConverter.convertToEntityAttribute(databaseColumn);
        Assert.assertEquals(uuid.toString(), databaseColumn);
    }
}
