package pl.mm.notes.core.domain.exception;

import org.testng.annotations.Test;

public class Http500InternalServerErrorRuntimeExceptionTest extends AbstractHttpNotesRuntimeExceptionTest {

    @Test(expectedExceptions = {Http500InternalServerErrorRuntimeException.class})
    @Override
    public void shouldThrowExceptionUsingDefaultConstructor() {
        throw new Http500InternalServerErrorRuntimeException();
    }

    @Test(expectedExceptions = {Http500InternalServerErrorRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 500 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithMessage() {
        throw new Http500InternalServerErrorRuntimeException("Some 500 error");
    }

    @Test(expectedExceptions = {Http500InternalServerErrorRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 500 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithMessageAndThrowable() {
        throw new Http500InternalServerErrorRuntimeException("Some 500 error", new Exception("Some 500 error from Throwable"));
    }

    @Test(expectedExceptions = {Http500InternalServerErrorRuntimeException.class}, expectedExceptionsMessageRegExp = "java.lang.Exception: Some 500 error from Throwable")
    @Override
    public void shouldThrowExceptionUsingConstructorWithThrowable() {
        throw new Http500InternalServerErrorRuntimeException(new Exception("Some 500 error from Throwable"));
    }

    @Test(expectedExceptions = {Http500InternalServerErrorRuntimeException.class}, expectedExceptionsMessageRegExp = "Some 500 error")
    @Override
    public void shouldThrowExceptionUsingConstructorWithAllArgsConstructor() {
        throw new Http500InternalServerErrorRuntimeException("Some 500 error", new Exception("Some 500 error from Throwable"), false, false);
    }
}