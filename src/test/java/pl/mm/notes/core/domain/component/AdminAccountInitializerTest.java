package pl.mm.notes.core.domain.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.notes.NotesApplicationTest;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid.SearchAclSidQuery;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.specification.AclSidSpecification;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.List;
import java.util.Optional;

public class AdminAccountInitializerTest extends NotesApplicationTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdminAccountInitializer adminAccountInitializer;
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void testInitAdminAccount() {
        // given
        Optional<User> adminUserToDeleteIfExists = userRepository.findByUsername(getAdminUsername());
        adminUserToDeleteIfExists.ifPresent(au -> userRepository.delete(au));

        final List<AclSid> aclSidList = aclSidRepository.findAll(new AclSidSpecification(SearchAclSidQuery.builder()
                .sid(getAdminUsername())
                .principal(true).build()));
        aclSidRepository.delete(aclSidList.get(0));

        // when
        adminAccountInitializer.onApplicationEvent(new ContextRefreshedEvent(applicationContext));

        // then
        Optional<User> foundAdminUser = userRepository.findByUsername(getAdminUsername());
        Assert.assertTrue(foundAdminUser.isPresent());
        Assert.assertEquals(foundAdminUser.get().getUsername(), getAdminUsername());
    }

    @Test(dependsOnMethods = "testInitAdminAccount")
    public void testInitAdminAccountAlreadyExists() {
        // given

        // when
        adminAccountInitializer.onApplicationEvent(new ContextRefreshedEvent(applicationContext));

        // then
        Optional<User> foundAdminUser = userRepository.findByUsername(getAdminUsername());
        Assert.assertTrue(foundAdminUser.isPresent());
        Assert.assertEquals(foundAdminUser.get().getUsername(), getAdminUsername());
    }

    @Test
    public void shouldCreateAclSidEntryForAdminAccount() {
        // when
        adminAccountInitializer.onApplicationEvent(new ContextRefreshedEvent(applicationContext));
        // then
        final List<AclSid> aclSidList = aclSidRepository.findAll(new AclSidSpecification(SearchAclSidQuery.builder()
                .sid(getAdminUsername())
                .principal(true).build()));
        Assert.assertEquals(aclSidList.get(0).getSid(), getAdminUsername());
        Assert.assertTrue(aclSidList.get(0).getPrincipal());
    }

}