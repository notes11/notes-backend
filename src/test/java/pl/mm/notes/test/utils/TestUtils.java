package pl.mm.notes.test.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.mm.notes.feature.role.domain.entity.Role;
import pl.mm.notes.feature.user.domain.entity.User;

public abstract class TestUtils {

    @SuppressWarnings("serial")
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper() {{
        configure(MapperFeature.USE_ANNOTATIONS, false);
    }};

    private TestUtils() {
    }

    public static String objectToJson(Object obj) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(obj);
    }

    public static String[] getRoles(User user) {
        return user.getRoles().stream()
                .map(Role::getRoleName)
                .toArray(String[]::new);
    }

    public static GrantedAuthority[] getAuthorities(User user) {
        return user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority(role.getRoleName()))
                .toArray(SimpleGrantedAuthority[]::new);
    }

    public static String[] getRoles(pl.mm.notes.test.model.User user) {
        return user.getRoles().toArray(String[]::new);
    }

    public static GrantedAuthority[] getAuthorities(pl.mm.notes.test.model.User user) {
        return user.getRoles().stream()
                .map(SimpleGrantedAuthority::new)
                .toArray(SimpleGrantedAuthority[]::new);
    }

}
