package pl.mm.notes.test.utils;

import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.ResultActions;
import pl.mm.notes.core.domain.dto.BaseInfoDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public abstract class AssertDtoResponse {

    private AssertDtoResponse() {
    }

    public static ResultActions assertPageResponse(final ResultActions resultActions) throws Exception {
        resultActions.andExpect(jsonPath("content").isArray())
                .andExpect(jsonPath("pageable.sort.sorted").isBoolean())
                .andExpect(jsonPath("pageable.sort.unsorted").isBoolean())
                .andExpect(jsonPath("pageable.sort.empty").isBoolean())
                .andExpect(jsonPath("pageable.pageNumber").isNumber())
                .andExpect(jsonPath("pageable.pageSize").isNumber())
                .andExpect(jsonPath("pageable.offset").isNumber())
                .andExpect(jsonPath("pageable.unpaged").isBoolean())
                .andExpect(jsonPath("pageable.paged").isBoolean())
                .andExpect(jsonPath("last").isBoolean())
                .andExpect(jsonPath("totalPages").isNumber())
                .andExpect(jsonPath("totalElements").isNumber())
                .andExpect(jsonPath("sort.sorted").isBoolean())
                .andExpect(jsonPath("sort.unsorted").isBoolean())
                .andExpect(jsonPath("sort.empty").isBoolean())
                .andExpect(jsonPath("number").isNumber())
                .andExpect(jsonPath("size").isNumber())
                .andExpect(jsonPath("numberOfElements").isNumber())
                .andExpect(jsonPath("first").isBoolean())
                .andExpect(jsonPath("empty").isBoolean());
        return resultActions;
    }

    public static ResultActions assertErrorResponse(final ResultActions resultActions, final HttpStatus httpStatus, final String message) throws Exception {
        assertValueOrIsString(resultActions, "timestamp", null);
        assertValueOrIsString(resultActions, "status", httpStatus.value());
        assertValueOrIsString(resultActions, "error", httpStatus.getReasonPhrase());
        assertValueOrIsString(resultActions, "message", message);
        assertValueOrIsString(resultActions, "path", null);
        return resultActions;
    }

    public static ResultActions assertErrorResponse(final ResultActions resultActions, final HttpStatus httpStatus) throws Exception {
        return assertErrorResponse(resultActions, httpStatus, null);
    }

    public static ResultActions assertErrorResponse(final ResultActions resultActions) throws Exception {
        return assertErrorResponse(resultActions, null);
    }

    private static void assertValueOrIsString(final ResultActions resultActions, final String jsonPath, final Object result) throws Exception {
        if (result != null) {
            resultActions.andExpect(jsonPath(jsonPath).value(result));
        } else {
            resultActions.andExpect(jsonPath(jsonPath).isString());
        }
    }

    private static void assertValueOrHasJsonPath(final ResultActions resultActions, final String jsonPath, final Object result) throws Exception {
        if (result != null) {
            resultActions.andExpect(jsonPath(jsonPath).value(result));
        } else {
            resultActions.andExpect(jsonPath(jsonPath).hasJsonPath());
        }
    }

    private static void assertValueOrIsBoolean(final ResultActions resultActions, final String jsonPath, final Object result) throws Exception {
        if (result != null) {
            resultActions.andExpect(jsonPath(jsonPath).value(result));
        } else {
            resultActions.andExpect(jsonPath(jsonPath).isBoolean());
        }
    }

    private static void assertValueOrIsArray(final ResultActions resultActions, final String jsonPath, final Object result) throws Exception {
        if (result != null) {
            resultActions.andExpect(jsonPath(jsonPath).value(result));
        } else {
            resultActions.andExpect(jsonPath(jsonPath).isArray());
        }
    }

    public static ResultActions assertBaseInfoDtoResponse(final ResultActions resultActions, final String prefix,
                                                          final BaseInfoDtoCommandAndQueryResult result) throws Exception {
        resultActions.andExpect(jsonPath(prefix + "version").doesNotHaveJsonPath())
                .andExpect(jsonPath(prefix + "entryRelatedToTable").doesNotHaveJsonPath());

        assertValueOrIsString(resultActions, prefix + "uuid", result.getUuid());
        assertValueOrIsString(resultActions, prefix + "createdBy", result.getCreatedBy());
        assertValueOrIsString(resultActions, prefix + "createdTimestamp", result.getCreatedTimestamp());
        assertValueOrHasJsonPath(resultActions, prefix + "modifiedBy", result.getCreatedTimestamp());
        assertValueOrHasJsonPath(resultActions, prefix + "modificationTimestamp", result.getCreatedTimestamp());
        return resultActions;
    }

    public static ResultActions assertBaseInfoDtoResponse(final ResultActions resultActions, final String prefix) throws Exception {
        return assertBaseInfoDtoResponse(resultActions, prefix, new BaseInfoDtoCommandAndQueryResult());
    }

    public static ResultActions assertBaseInfoDtoResponse(final ResultActions resultActions) throws Exception {
        return assertBaseInfoDtoResponse(resultActions, "");
    }

    public static ResultActions assertRoleDtoResponse(final ResultActions resultActions, final String prefix,
                                                      final RoleDtoCommandAndQueryResult result) throws Exception {
        assertValueOrIsString(resultActions, prefix + "roleName", result.getRoleName());
        return resultActions;
    }

    public static ResultActions assertRoleDtoResponse(final ResultActions resultActions, final String prefix) throws Exception {
        return assertRoleDtoResponse(resultActions, prefix, new RoleDtoCommandAndQueryResult());
    }

    public static ResultActions assertRoleDtoResponse(final ResultActions resultActions) throws Exception {
        return assertRoleDtoResponse(resultActions, "");
    }

    public static ResultActions assertUserDtoResponse(final ResultActions resultActions, final String prefix,
                                                      final UserDtoCommandAndQueryResult result) throws Exception {
        assertValueOrIsString(resultActions, prefix + "username", result.getUsername());
        assertValueOrHasJsonPath(resultActions, prefix + "firstName", result.getFirstName());
        assertValueOrHasJsonPath(resultActions, prefix + "lastName", result.getLastName());
        assertValueOrIsString(resultActions, prefix + "addressEmail", result.getAddressEmail());
        assertValueOrIsBoolean(resultActions, prefix + "isActive", result.getIsActive());
        assertValueOrIsArray(resultActions, prefix + "roles", result.getRoles());
        return resultActions;
    }

    public static ResultActions assertUserDtoResponse(final ResultActions resultActions, final String prefix) throws Exception {
        return assertUserDtoResponse(resultActions, prefix, new UserDtoCommandAndQueryResult());
    }

    public static ResultActions assertUserDtoResponse(final ResultActions resultActions) throws Exception {
        return assertUserDtoResponse(resultActions, "");
    }

    public static ResultActions assertNoteDtoResponse(final ResultActions resultActions, final String prefix,
                                                      final NoteDtoCommandAndQueryResult result) throws Exception {
        assertValueOrIsString(resultActions, prefix + "title", result.getTitle());
        assertValueOrIsString(resultActions, prefix + "noteText", result.getNoteText());
        assertValueOrIsBoolean(resultActions, prefix + "isPublic", result.getIsPublic());
        return resultActions;
    }

    public static ResultActions assertNoteDtoResponse(final ResultActions resultActions, final String prefix) throws Exception {
        return assertNoteDtoResponse(resultActions, prefix, new NoteDtoCommandAndQueryResult());
    }

    public static ResultActions assertNoteDtoResponse(final ResultActions resultActions) throws Exception {
        return assertNoteDtoResponse(resultActions, "");
    }

    public static ResultActions assertNoteImageDtoResponse(final ResultActions resultActions, final String prefix,
                                                          final NoteImageDtoCommandAndQueryResult result) throws Exception {
        assertValueOrIsString(resultActions, prefix + "imageName", result.getImageName());
        return resultActions;
    }

    public static ResultActions assertNoteImageDtoResponse(final ResultActions resultActions, final String prefix) throws Exception {
        return assertNoteImageDtoResponse(resultActions, prefix, new NoteImageDtoCommandAndQueryResult());
    }

    public static ResultActions assertNoteImageDtoResponse(final ResultActions resultActions) throws Exception {
        return assertNoteImageDtoResponse(resultActions, "");
    }
}
