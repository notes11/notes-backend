package pl.mm.notes.test.data;

import pl.mm.notes.core.domain.Role;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.test.model.User;

import java.util.ArrayList;

public abstract class UserTestData {

    private UserTestData() {
    }

    @SuppressWarnings("serial")
    public static final User ADMIN = User.builder()
            .username("admin")
            .password("admin-password")
            .addressEmail("noaddressemail@sys.com")
            .roles(new ArrayList<>() {{
                add(Role.USER);
                add(Role.ADMIN);
            }})
            .build();

    @SuppressWarnings("serial")
    public static final User USER = User.builder()
            .username("test-user")
            .password("test-user-password")
            .addressEmail("test-user@gmail.com")
            .roles(new ArrayList<>() {{
                add(Role.USER);
            }})
            .build();

    @SuppressWarnings("serial")
    public static final User USER_2 = User.builder()
            .username("test-user-2")
            .password("test-user-2-password")
            .addressEmail("test-user-2@gmail.com")
            .roles(new ArrayList<>() {{
                add(Role.USER);
            }})
            .build();

    public static final CreateUserAccountCommand CREATE_USER_ACCOUNT_COMMAND = CreateUserAccountCommand.builder()
            .username(USER.getUsername())
            .password(USER.getPassword())
            .addressEmail(USER.getAddressEmail())
            .build();

    public static final CreateUserAccountCommand CREATE_USER_ACCOUNT_COMMAND_2 = CreateUserAccountCommand.builder()
            .username(USER_2.getUsername())
            .password(USER_2.getPassword())
            .addressEmail(USER_2.getAddressEmail())
            .build();

}
