package pl.mm.notes.test.data;

import pl.mm.notes.feature.note.domain.cqrs.command.createnote.CreateNoteCommand;

import java.util.ArrayList;
import java.util.List;

public abstract class NoteTestData {

    private NoteTestData() {
    }

    public final static CreateNoteCommand CREATE_NOTE_COMMAND = CreateNoteCommand.builder()
            .title("Lorem Ipsum")
            .noteText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elit lorem, lacinia eget cursus eget, tincidunt a diam.")
            .isPublic(false)
            .build();

    public final static CreateNoteCommand CREATE_NOTE_COMMAND_2 = CreateNoteCommand.builder()
            .title("Lorem Ipsum - 2")
            .noteText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elit lorem, lacinia eget cursus eget, tincidunt a diam.")
            .isPublic(false)
            .build();

    public final static CreateNoteCommand CREATE_PUBLIC_NOTE_COMMAND = CreateNoteCommand.builder()
            .title("Lorem Ipsum - Public note")
            .noteText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean elit lorem, lacinia eget cursus eget, tincidunt a diam.")
            .isPublic(true)
            .build();

    public final static int numberOfNotesToCreate = 50;

    @SuppressWarnings("serial")
    public final static List<CreateNoteCommand> CREATE_NOTE_COMMAND_LIST = new ArrayList<>() {{
        for (int i = 0; i < numberOfNotesToCreate; i++) {
            add(CreateNoteCommand.builder()
                    .title(NoteTestData.CREATE_NOTE_COMMAND.getTitle() + " - " + i)
                    .noteText(NoteTestData.CREATE_NOTE_COMMAND.getNoteText())
                    .build());
        }
    }};

}
