package pl.mm.notes.test.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public final class User {

    @Builder.Default
    private final String username = "";
    @Builder.Default
    private final String password = "";
    @Builder.Default
    private final String addressEmail = "";
    @Builder.Default
    private final List<String> roles = new ArrayList<>();

}
