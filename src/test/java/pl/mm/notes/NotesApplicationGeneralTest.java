package pl.mm.notes;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.Test;
import pl.mm.notes.feature.user.domain.repository.UserRepository;
import pl.mm.notes.feature.user.domain.entity.User;

import java.util.Optional;

public class NotesApplicationGeneralTest extends NotesApplicationTest {

    @Autowired
    protected UserRepository userRepository;
    @Value("${app.admin.username}")
    private String adminUsername;


    @Test
    public void testInitAdminAccount() {
        Optional<User> adminAccount = userRepository.findByUsername(adminUsername);
        Assert.isTrue(adminAccount.isPresent(), "Admin account doesn't exist");
    }

}
