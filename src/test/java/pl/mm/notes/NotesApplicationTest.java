package pl.mm.notes;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeMethod;
import pl.mm.notes.feature.acl.aclentry.domain.repository.AclEntryRepository;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.repository.AclObjectIdentityRepository;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid.SearchAclSidQuery;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.repository.AclSidRepository;
import pl.mm.notes.feature.acl.aclsid.domain.specification.AclSidSpecification;
import pl.mm.notes.feature.note.domain.repository.NoteImageRepository;
import pl.mm.notes.feature.note.domain.repository.NoteRepository;
import pl.mm.notes.feature.role.domain.repository.RoleRepository;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
public abstract class NotesApplicationTest extends AbstractTestNGSpringContextTests {

    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected RoleRepository roleRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected NoteRepository noteRepository;
    @Autowired
    protected NoteImageRepository noteImageRepository;
    @Autowired
    protected AclSidRepository aclSidRepository;
    @Autowired
    protected AclObjectIdentityRepository aclObjectIdentityRepository;
    @Autowired
    protected AclEntryRepository aclEntryRepository;

    @Value("${app.admin.username}")
    private String adminUsername;
    @Value("${app.admin.password}")
    private String adminPassword;

    @BeforeMethod
    public final void setupDatabaseBeforeTestMethod() {
        noteImageRepository.deleteAll();
        noteRepository.deleteAll();

        final List<User> userList = userRepository.findAll();
        userList.forEach(user -> {
            if (!user.getUsername().equals(adminUsername)) {
                userRepository.delete(user);
            }
        });

        aclEntryRepository.deleteAll();
        aclObjectIdentityRepository.deleteAll();
        final List<AclSid> aclSidList = aclSidRepository.findAll(new AclSidSpecification(SearchAclSidQuery.builder()
                .principal(true)
                .build()));
        aclSidList.forEach(aclSid -> {
            if (!aclSid.getSid().equals(adminUsername)) {
                aclSidRepository.delete(aclSid);
            }
        });
    }

    protected final String getAdminUsername() {
        return adminUsername;
    }

    protected final String getAdminPassword() {
        return adminPassword;
    }
}
