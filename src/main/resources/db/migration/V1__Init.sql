CREATE TABLE `base_info`
(
    `Id`                    BIGINT      NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Uuid`                  VARCHAR(36) NOT NULL,
    `Version`               BIGINT      NOT NULL,
    `CreatedBy`             VARCHAR(50) NOT NULL,
    `CreatedTimestamp`      TIMESTAMP   NOT NULL,
    `ModifiedBy`            VARCHAR(50),
    `ModificationTimeStamp` TIMESTAMP,
    `EntryRelatedToTable`   VARCHAR(50) NOT NULL,
    CONSTRAINT `UQ_Uuid` UNIQUE (`Uuid`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `user`
(
    `Id`           BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Username`     VARCHAR(50)  NOT NULL,
    `Password`     VARCHAR(80)  NOT NULL,
    `FirstName`    VARCHAR(50),
    `LastName`     VARCHAR(50),
    `AddressEmail` VARCHAR(100) NOT NULL,
    `IsActive`     BIT          NOT NULL DEFAULT 0,
    CONSTRAINT `FK_UserId` FOREIGN KEY (`Id`) REFERENCES `base_info` (`Id`),
    CONSTRAINT `UQ_UserName` UNIQUE (`Username`),
    CONSTRAINT `UQ_AddressEmail` UNIQUE (`AddressEmail`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `role`
(
    `Id`       BIGINT      NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `RoleName` VARCHAR(10) NOT NULL,
    CONSTRAINT `FK_RoleId` FOREIGN KEY (`Id`) REFERENCES `base_info` (`Id`),
    CONSTRAINT `UQ_RoleName` UNIQUE (`RoleName`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

SET @adminUuid = uuid();
INSERT INTO `base_info` (`Uuid`, `Version`, `CreatedBy`, `CreatedTimestamp`, `EntryRelatedToTable`)
VALUES (@adminUuid, 1, 'init-script', CURRENT_TIMESTAMP(), 'role');

SET @adminId = (SELECT `id`
                FROM base_info
                WHERE `Uuid` = @adminUuid);
INSERT INTO `role`(`Id`, `RoleName`)
VALUES (@adminId, 'ADMIN');


SET @userUuid = uuid();
INSERT INTO `base_info` (`Uuid`, `Version`, `CreatedBy`, `CreatedTimestamp`, `EntryRelatedToTable`)
VALUES (@userUuid, 1, 'init-script', CURRENT_TIMESTAMP(), 'role');

SET @userId = (SELECT `id`
               FROM base_info
               WHERE `Uuid` = @userUuid);
INSERT INTO `role`(`Id`, `RoleName`)
VALUES (@userId, 'USER');


CREATE TABLE `user_roles`
(
    `UserId` BIGINT NOT NULL,
    `RoleId` BIGINT NOT NULL,
    PRIMARY KEY (`UserId`, `RoleId`),
    CONSTRAINT `FK_UserRolesUserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`),
    CONSTRAINT `FK_UserRolesRoleId` FOREIGN KEY (`RoleId`) REFERENCES role (`Id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `note`
(
    `Id`       BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Title`    VARCHAR(255) NOT NULL,
    `Note`     TEXT         NOT NULL,
    `OwnerId`  BIGINT       NOT NULL,
    `IsPublic` BIT          NOT NULL,
    CONSTRAINT `FK_NoteId` FOREIGN KEY (`Id`) REFERENCES `base_info` (`Id`),
    CONSTRAINT `FK_NoteOwnerId` FOREIGN KEY (`OwnerId`) REFERENCES `user` (`Id`),
    CONSTRAINT `UQ_NoteTitleOwnerId` UNIQUE (`Title`, `OwnerId`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE note_image
(
    id         BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
    image_name VARCHAR(255) NOT NULL,
    image_blob BLOB         NOT NULL,
    note_id    BIGINT       NOT NULL,
    CONSTRAINT fk_note_image_id FOREIGN KEY (id) REFERENCES base_info (id),
    CONSTRAINT fk_note_image_note_id FOREIGN KEY (note_id) REFERENCES note (id)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE acl_sid
(
    id        BIGINT      NOT NULL PRIMARY KEY AUTO_INCREMENT,
    principal BIT         NOT NULL,
    sid       VARCHAR(50) NOT NULL,
    UNIQUE KEY UQ_SidAndPrincipal (sid, principal)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE acl_class
(
    id    BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
    class VARCHAR(255) NOT NULL,
    CONSTRAINT UQ_AclClass_Class UNIQUE (class)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE acl_object_identity
(
    id                 BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    object_id_class    BIGINT NOT NULL,
    object_id_identity BIGINT NOT NULL,
    parent_object      BIGINT DEFAULT NULL,
    owner_sid          BIGINT DEFAULT NULL,
    entries_inheriting BIT    NOT NULL,
    CONSTRAINT UQ_AclObjectIdentity_ObjectIdClass_ObjectIdIdentity UNIQUE KEY (object_id_class, object_id_identity),
    CONSTRAINT FK_AclObjectIdentity_ObjectIdClass FOREIGN KEY (object_id_class) REFERENCES acl_class (id),
    CONSTRAINT FK_AclObjectIdentity_ParentObject FOREIGN KEY (parent_object) REFERENCES acl_object_identity (id),
    CONSTRAINT FK_AclObjectIdentity_OwnerSid FOREIGN KEY (owner_sid) REFERENCES acl_sid (id)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;


CREATE TABLE acl_entry
(
    id                  BIGINT  NOT NULL PRIMARY KEY AUTO_INCREMENT,
    acl_object_identity BIGINT  NOT NULL,
    ace_order           INT(11) NOT NULL,
    sid                 BIGINT  NOT NULL,
    mask                INT(11) NOT NULL,
    granting            BIT     NOT NULL,
    audit_success       BIT     NOT NULL,
    audit_failure       BIT     NOT NULL,
    CONSTRAINT UQ_AclObjectIdentity_AceOrder UNIQUE KEY (acl_object_identity, ace_order),
    CONSTRAINT FK_AclEntry_AclObjectIdentity FOREIGN KEY (acl_object_identity) REFERENCES acl_object_identity (id),
    CONSTRAINT FK_AclEntry_Sid FOREIGN KEY (sid) REFERENCES acl_sid (id)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4;