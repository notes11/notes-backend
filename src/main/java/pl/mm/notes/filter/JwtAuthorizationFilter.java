package pl.mm.notes.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;
import pl.mm.notes.core.domain.component.JwtConfiguration;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQuery;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQueryHandler;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final static String EMPTY_STRING = "";
    private final JwtConfiguration jwtConfiguration;
    private final FindUserByUsernameQueryHandler queryHandler;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  JwtConfiguration jwtConfiguration,
                                  FindUserByUsernameQueryHandler queryHandler) {
        super(authenticationManager);
        this.jwtConfiguration = jwtConfiguration;
        this.queryHandler = queryHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String authorizationHeader = request.getHeader(jwtConfiguration.getHeader());

        if (authorizationHeader != null && authorizationHeader.startsWith(jwtConfiguration.getTokenPrefix())
                && StringUtils.hasText(getJwtClaims(authorizationHeader))) {
            try {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = getAuthenticationByToken(authorizationHeader);
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                chain.doFilter(request, response);
            } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException |
                    SignatureException | IllegalArgumentException exception) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage());
                response.addHeader("Status Code", HttpStatus.UNAUTHORIZED.toString());
                response.getWriter().flush();
                response.getWriter().close();
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private String getJwtClaims(String authorizationHeader) {
        String jwtClaims = authorizationHeader.replace(jwtConfiguration.getTokenPrefix(), EMPTY_STRING);
        return jwtClaims.trim();
    }

    private UsernamePasswordAuthenticationToken getAuthenticationByToken(String authorizationHeader) {
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey(jwtConfiguration.getSecretKey().getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(getJwtClaims(authorizationHeader));

        String userName = claimsJws.getBody().get(jwtConfiguration.getClaimsJws().getUsername()).toString();
        String roles = claimsJws.getBody().get(jwtConfiguration.getClaimsJws().getRoles()).toString();

        if (checkIfUserExistsInTheSystem(userName)) {
            return new UsernamePasswordAuthenticationToken(userName, null, getAllAuthorities(roles));
        } else {
            return null;
        }
    }

    private boolean checkIfUserExistsInTheSystem(String userName) {
        UserDtoCommandAndQueryResult user = queryHandler.execute(FindUserByUsernameQuery.builder()
                .username(userName)
                .build());
        return user != null && user.getUsername() != null;
    }

    private Set<SimpleGrantedAuthority> getAllAuthorities(String roles) {
        roles = roles.trim();
        roles = roles.replace("[", "").replace("]", "");
        roles = roles.replace("{", "").replace("}", "");
        roles = roles.replace("authority=", "");
        roles = roles.replace(" ", "");
        String[] splitRoles = roles.split(",");

        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = new HashSet<>();

        for (String role : splitRoles) {
            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(role));
        }

        return simpleGrantedAuthorities;
    }


}
