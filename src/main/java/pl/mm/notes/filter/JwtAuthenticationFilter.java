package pl.mm.notes.filter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.mm.notes.core.domain.component.JwtConfiguration;
import pl.mm.notes.core.domain.service.jwt.JwtTokenGeneratorService;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQuery;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQueryHandler;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;


public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final JwtConfiguration jwtConfiguration;
    private final JwtTokenGeneratorService jwtTokenGeneratorService;
    private final AuthenticationManager authenticationManager;
    private final ObjectMapper objectMapper;
    private final FindUserByUsernameQueryHandler queryHandler;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
                                   JwtConfiguration jwtConfiguration,
                                   JwtTokenGeneratorService jwtTokenGeneratorService,
                                   FindUserByUsernameQueryHandler queryHandler,
                                   ObjectMapper objectMapper) {
        this.jwtConfiguration = jwtConfiguration;
        this.jwtTokenGeneratorService = jwtTokenGeneratorService;
        this.authenticationManager = authenticationManager;
        this.objectMapper = objectMapper;
        this.queryHandler = queryHandler;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        final String body;
        final JsonNode jsonNode;
        final String username;
        final String password;
        try {
            body = getBodyFromRequestInputStream(request);
            jsonNode = objectMapper.readTree(body);
        } catch (IOException exception) {
            throw new BadCredentialsException("Failed to authenticate - could not load authentication body", exception);
        }
        validateUsernameAndPassword(jsonNode);
        username = jsonNode.get("username").asText();
        password = jsonNode.get("password").asText();
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password, new ArrayList<>()));
    }

    private void validateUsernameAndPassword(JsonNode jsonNode) {
        if (jsonNode.get("username") == null) {
            throw new BadCredentialsException("Failed to authenticate - not found 'username' in the request body");
        } else if (jsonNode.get("password") == null) {
            throw new BadCredentialsException("Failed to authenticate - not found 'password' in the request body");
        }
    }

    private String getBodyFromRequestInputStream(HttpServletRequest request) throws IOException {
        return request.getReader().lines().collect(Collectors.joining());
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException {
        User user = ((User) authResult.getPrincipal());
        String jwtToken = jwtConfiguration.getTokenPrefix();
        jwtToken += jwtTokenGeneratorService.generateJwtToken(authResult);

        UserDtoCommandAndQueryResult userQueryResult = queryHandler.execute(FindUserByUsernameQuery.builder()
                .username(user.getUsername())
                .build());

        response.addHeader(jwtConfiguration.getHeader(), jwtToken);
        response.getWriter().write(objectMapper.writeValueAsString(userQueryResult));

        response.getWriter().flush();
        response.getWriter().close();
    }


}
