package pl.mm.notes.config;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ModelMapperConfiguration {

    @Bean
    public ModelMapper modelMapper(@Autowired final List<Converter<?, ?>> converterList) {
        ModelMapper modelMapper = new ModelMapper();
        converterList.forEach(modelMapper::addConverter);
        return modelMapper;
    }

}
