package pl.mm.notes.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import pl.mm.notes.core.domain.component.JwtConfiguration;
import pl.mm.notes.core.domain.service.jwt.JwtTokenGeneratorService;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQueryHandler;
import pl.mm.notes.feature.user.domain.service.UserDetailsServiceImpl;
import pl.mm.notes.filter.JwtAuthenticationFilter;
import pl.mm.notes.filter.JwtAuthorizationFilter;

import java.util.List;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final PasswordEncoder passwordEncoder;
    private final JwtConfiguration jwtConfiguration;
    private final JwtTokenGeneratorService jwtTokenGeneratorService;
    private final ObjectMapper objectMapper;
    private final FindUserByUsernameQueryHandler queryHandler;

    @Autowired
    public WebSecurityConfig(UserDetailsServiceImpl userDetailsServiceImpl,
                             PasswordEncoder passwordEncoder,
                             JwtConfiguration jwtConfiguration,
                             JwtTokenGeneratorService jwtTokenGeneratorService,
                             ObjectMapper objectMapper,
                             FindUserByUsernameQueryHandler queryHandler) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.passwordEncoder = passwordEncoder;
        this.jwtConfiguration = jwtConfiguration;
        this.jwtTokenGeneratorService = jwtTokenGeneratorService;
        this.objectMapper = objectMapper;
        this.queryHandler = queryHandler;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @SuppressWarnings({"java:S4502", "java:S4502 - https://stackoverflow.com/a/43454089/3523102"})
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable()
                .cors()
                .and()
                .addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtConfiguration, jwtTokenGeneratorService, queryHandler, objectMapper))
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), jwtConfiguration, queryHandler));
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource(@Value("${app.cors.allow-credentials}") Boolean allowCredentials,
                                                           @Value("${app.cors.allowed-origins}") List<String> allowedOrigins,
                                                           @Value("${app.cors.allowed-headers}") List<String> allowedHeaders,
                                                           @Value("${app.cors.exposed-headers}") List<String> exposedHeaders,
                                                           @Value("${app.cors.allowed-methods}") List<String> allowedMethods,
                                                           @Value("${app.cors.pattern}") String pattern) {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(allowCredentials);
        corsConfiguration.setAllowedOrigins(allowedOrigins);
        corsConfiguration.setAllowedHeaders(allowedHeaders);
        corsConfiguration.setExposedHeaders(exposedHeaders);
        corsConfiguration.setAllowedMethods(allowedMethods);
        source.registerCorsConfiguration(pattern, corsConfiguration);
        return source;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}
