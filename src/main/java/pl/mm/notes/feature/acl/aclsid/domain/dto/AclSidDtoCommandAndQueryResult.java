package pl.mm.notes.feature.acl.aclsid.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.dto.BaseInfoDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@SuperBuilder
public class AclSidDtoCommandAndQueryResult extends BaseInfoDtoCommandAndQueryResult {
    private static final long serialVersionUID = -3787311706935329644L;
    /**
     * @see AclSid#getPrincipal()
     */
    protected final Boolean principal;
    /**
     * @see AclSid#getSid()
     */
    protected final String sid;
}
