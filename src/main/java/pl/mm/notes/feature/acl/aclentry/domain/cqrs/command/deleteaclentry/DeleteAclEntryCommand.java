package pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.deleteaclentry;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@AllArgsConstructor
@SuperBuilder
public class DeleteAclEntryCommand implements Command, Serializable {
    private static final long serialVersionUID = -7349600218029934448L;

    @NotNull(message = "'aclClass' cannot be empty.")
    private final Class<?> aclClass;

    @NotNull(message = "'objectIdIdentity' cannot be empty.")
    private final Long objectIdIdentity;
}
