package pl.mm.notes.feature.acl.aclentry.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.query.QueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.dto.AclObjectIdentityDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.dto.AclSidDtoCommandAndQueryResult;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@SuperBuilder
public class AclEntryDtoCommandAndQueryResult implements CommandResult, QueryResult {
    private AclObjectIdentityDtoCommandAndQueryResult aclObjectIdentity;
    private Long aceOrder;
    private AclSidDtoCommandAndQueryResult sid;
    private Long mask;
    private Boolean granting;
    private Boolean auditSuccess;
    private Boolean auditFailure;
}
