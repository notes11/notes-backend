package pl.mm.notes.feature.acl.aclobjectidentity.domain.cqrs.command.addaclobjectidentity;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import pl.mm.notes.core.domain.cqrs.command.AbstractDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass;
import pl.mm.notes.feature.acl.aclclass.domain.repository.AclClassRepository;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.dto.AclObjectIdentityDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.repository.AclObjectIdentityRepository;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.repository.AclSidRepository;

import java.util.Optional;

@Service
public class AddAclObjectIdentityCommandHandler
        extends AbstractDbCommandHandler<AddAclObjectIdentityCommand, AclObjectIdentityDtoCommandAndQueryResult, AclObjectIdentityRepository> {

    private final AclClassRepository aclClassRepository;
    private final AclSidRepository aclSidRepository;

    @Autowired
    public AddAclObjectIdentityCommandHandler(AclObjectIdentityRepository aclObjectIdentityRepository,
                                              AclClassRepository aclClassRepository,
                                              AclSidRepository aclSidRepository,
                                              ModelMapper modelMapper) {
        super(aclObjectIdentityRepository, modelMapper);
        this.aclClassRepository = aclClassRepository;
        this.aclSidRepository = aclSidRepository;
    }

    @Transactional
    @Override
    public AclObjectIdentityDtoCommandAndQueryResult execute(final AddAclObjectIdentityCommand command) {
        final AclClass aclClass = aclClassRepository.findByAclClass(command.getAclClass().getName())
                .orElseThrow(() -> new Http404NotFoundRuntimeException("Not found '" + AclClass.class + "' with name '"
                        + command.getAclClass() + "'"));

        final AclObjectIdentity parentObject = getParentObjectIfProvided(command);
        final AclSid aclSid = getOwnerIfProvided(command);

        AclObjectIdentity aclObjectIdentity = AclObjectIdentity.builder()
                .aclClass(aclClass)
                .objectIdIdentity(command.getObjectIdIdentity())
                .parentObject(parentObject)
                .owner(aclSid)
                .entriesInheriting(command.getEntriesInheriting())
                .build();

        aclObjectIdentity = repository().save(aclObjectIdentity);

        return modelMapper.map(aclObjectIdentity, AclObjectIdentityDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<AddAclObjectIdentityCommand> getEventType() {
        return AddAclObjectIdentityCommand.class;
    }

    @Override
    public Class<AclObjectIdentityDtoCommandAndQueryResult> getEventResultType() {
        return AclObjectIdentityDtoCommandAndQueryResult.class;
    }

    private AclObjectIdentity getParentObjectIfProvided(final AddAclObjectIdentityCommand command) {
        if (command.getParentObjectId() != null) {
            final Optional<AclObjectIdentity> parentObjectOptional = repository().findById(command.getParentObjectId());
            if (parentObjectOptional.isEmpty()) {
                throw new Http404NotFoundRuntimeException("Not found '" + AclObjectIdentity.class + "' with id '"
                        + command.getObjectIdIdentity() + "'");
            } else {
                return parentObjectOptional.get();
            }
        }
        return null;
    }

    private AclSid getOwnerIfProvided(final AddAclObjectIdentityCommand command) {
        if (StringUtils.hasText(command.getOwnerUsername())) {
            final Optional<AclSid> ownerOptional = aclSidRepository.findBySid(command.getOwnerUsername());
            if (ownerOptional.isEmpty()) {
                throw new Http404NotFoundRuntimeException("Not found owner '" + AclSid.class + "' with username '"
                        + command.getOwnerUsername() + "'");
            } else {
                return ownerOptional.get();
            }
        }
        return null;
    }

}
