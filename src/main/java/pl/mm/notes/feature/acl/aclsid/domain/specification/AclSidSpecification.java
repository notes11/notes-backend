package pl.mm.notes.feature.acl.aclsid.domain.specification;

import org.springframework.data.jpa.domain.Specification;
import pl.mm.notes.core.domain.entity.BaseInfo_;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid.SearchAclSidQuery;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class AclSidSpecification implements Specification<AclSid> {
    private static final long serialVersionUID = -1030064608615066168L;

    private final SearchAclSidQuery searchAclSidQuery;

    public AclSidSpecification(SearchAclSidQuery searchAclSidQuery) {
        this.searchAclSidQuery = searchAclSidQuery;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Predicate toPredicate(Root<AclSid> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
        final List<Predicate> predicates = new ArrayList<>();

        if (searchAclSidQuery.getId() != null) {
            predicates.add(cb.equal(root.get(BaseInfo_.id), searchAclSidQuery.getSid()));
        }

        if (searchAclSidQuery.getPrincipal() != null) {
            predicates.add(cb.equal(root.get(AclSid_.principal), searchAclSidQuery.getPrincipal()));
        }

        if (searchAclSidQuery.getSid() != null) {
            predicates.add(cb.like(root.get(AclSid_.sid), searchAclSidQuery.getSid() + "%"));
        }

        return cb.and(predicates.toArray(new Predicate[0]));
    }
}
