package pl.mm.notes.feature.acl.aclobjectidentity.domain.cqrs.command.deleteaclobjectidentity;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.command.AbstractDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.dto.AclObjectIdentityDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.repository.AclObjectIdentityRepository;

import java.util.Optional;

@Service
public class DeleteAclObjectIdentityCommandHandler
        extends AbstractDbCommandHandler<DeleteAclObjectIdentityCommand, AclObjectIdentityDtoCommandAndQueryResult, AclObjectIdentityRepository> {

    public DeleteAclObjectIdentityCommandHandler(AclObjectIdentityRepository aclObjectIdentityRepository,
                                                 ModelMapper modelMapper) {
        super(aclObjectIdentityRepository, modelMapper);
    }

    @Override
    public AclObjectIdentityDtoCommandAndQueryResult execute(DeleteAclObjectIdentityCommand command) {
        final Optional<AclObjectIdentity> objectIdIdentityOptional =
                repository().findByAclClassAclClassAndObjectIdIdentity(command.getAclClass().getName(), command.getObjectIdIdentity());

        if (objectIdIdentityOptional.isEmpty()) {
            final String message = String.format("Not found ACL_OBJECT_IDENTITY for class '%s' with object id identity '%d'",
                    command.getAclClass().getName(), command.getObjectIdIdentity());
            throw new Http404NotFoundRuntimeException(message);
        }
        final AclObjectIdentity aclObjectIdentity = objectIdIdentityOptional.get();
        repository().delete(aclObjectIdentity);
        return modelMapper.map(aclObjectIdentity, AclObjectIdentityDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<DeleteAclObjectIdentityCommand> getEventType() {
        return DeleteAclObjectIdentityCommand.class;
    }

    @Override
    public Class<AclObjectIdentityDtoCommandAndQueryResult> getEventResultType() {
        return AclObjectIdentityDtoCommandAndQueryResult.class;
    }
}
