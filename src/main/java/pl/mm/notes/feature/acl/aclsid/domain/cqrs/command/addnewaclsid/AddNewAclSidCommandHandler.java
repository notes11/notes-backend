package pl.mm.notes.feature.acl.aclsid.domain.cqrs.command.addnewaclsid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.feature.acl.aclsid.domain.dto.AclSidDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.repository.AclSidRepository;
import pl.mm.notes.feature.acl.aclsid.domain.service.AclSidTableNameProviderService;

import static pl.mm.notes.core.domain.NotesApplicationGeneral.getUsername;

@Service
public class AddNewAclSidCommandHandler
        extends AbstractBaseInfoDbCommandHandler<AddNewAclSidCommand, AclSidDtoCommandAndQueryResult, AclSidRepository> {

    @Autowired
    public AddNewAclSidCommandHandler(AclSidRepository aclSidRepository,
                                      ModelMapper modelMapper,
                                      AclSidTableNameProviderService aclSidTableNameProviderService) {
        super(aclSidRepository, modelMapper, aclSidTableNameProviderService);
    }

    @Transactional
    @Override
    public AclSidDtoCommandAndQueryResult execute(AddNewAclSidCommand command) {
        final String usernameOfLoggedInUser = getUsername();
        AclSid aclSid = modelMapper.map(command, AclSid.class);
        aclSid = create(aclSid, usernameOfLoggedInUser, usernameOfLoggedInUser);
        aclSid = repository().save(aclSid);
        return modelMapper.map(aclSid, AclSidDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<AddNewAclSidCommand> getEventType() {
        return AddNewAclSidCommand.class;
    }

    @Override
    public Class<AclSidDtoCommandAndQueryResult> getEventResultType() {
        return AclSidDtoCommandAndQueryResult.class;
    }
}
