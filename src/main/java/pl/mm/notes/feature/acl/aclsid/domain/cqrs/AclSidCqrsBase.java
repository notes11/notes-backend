package pl.mm.notes.feature.acl.aclsid.domain.cqrs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;

import java.io.Serializable;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@SuperBuilder
public abstract class AclSidCqrsBase implements Serializable {
    private static final long serialVersionUID = 197600122588025917L;
    /**
     * @see AclSid#getPrincipal()
     */
    protected final Boolean principal;
    /**
     * @see AclSid#getSid()
     */
    protected final String sid;
}
