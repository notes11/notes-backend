package pl.mm.notes.feature.acl.aclclass.domain.cqrs.command.create;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@SuperBuilder
public final class AclClassCreateCommand implements Command {
    @NotBlank(message = "'aclClass' cannot be blank.")
    private final String aclClass;
}
