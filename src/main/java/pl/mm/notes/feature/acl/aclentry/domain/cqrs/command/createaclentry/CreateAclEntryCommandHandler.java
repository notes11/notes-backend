package pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.createaclentry;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.command.AbstractDbCommandHandler;
import pl.mm.notes.feature.acl.aclentry.domain.dto.AclEntryDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclentry.domain.entity.AclEntry;
import pl.mm.notes.feature.acl.aclentry.domain.repository.AclEntryRepository;

@Service
public class CreateAclEntryCommandHandler extends AbstractDbCommandHandler<CreateAclEntryCommand, AclEntryDtoCommandAndQueryResult, AclEntryRepository> {

    public CreateAclEntryCommandHandler(AclEntryRepository aclEntryRepository,
                                        ModelMapper modelMapper) {
        super(aclEntryRepository, modelMapper);
    }

    @Override
    public AclEntryDtoCommandAndQueryResult execute(CreateAclEntryCommand command) {
        AclEntry aclEntry = modelMapper.map(command, AclEntry.class);
        aclEntry = repository().save(aclEntry);
        return modelMapper.map(aclEntry, AclEntryDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<CreateAclEntryCommand> getEventType() {
        return CreateAclEntryCommand.class;
    }

    @Override
    public Class<AclEntryDtoCommandAndQueryResult> getEventResultType() {
        return AclEntryDtoCommandAndQueryResult.class;
    }
}
