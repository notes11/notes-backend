package pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.dto.AclSidDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.specification.AclSidSpecification;
import pl.mm.notes.feature.acl.aclsid.domain.repository.AclSidRepository;

import java.util.ArrayList;

@Service
public class SearchAclSidQueryHandler
        extends AbstractBaseInfoDbQueryHandler<SearchAclSidQuery, PageQueryResult<AclSidDtoCommandAndQueryResult>, AclSidRepository> {
    @Autowired
    public SearchAclSidQueryHandler(AclSidRepository aclSidRepository,
                                    ModelMapper modelMapper) {
        super(aclSidRepository, modelMapper);
    }

    @Transactional(readOnly = true)
    @Override
    public PageQueryResult<AclSidDtoCommandAndQueryResult> execute(final SearchAclSidQuery query) {
        final AclSidSpecification specification = new AclSidSpecification(query);
        Page<AclSid> aclSidResult = repository.findAll(specification, query.getPageable());
        Page<AclSidDtoCommandAndQueryResult> result = aclSidResult.map(as -> modelMapper.map(as, AclSidDtoCommandAndQueryResult.class));
        return new PageQueryResult<>(result.getContent(), query.getPageable(), aclSidResult.getTotalElements());
    }

    @Override
    public Class<SearchAclSidQuery> getEventType() {
        return SearchAclSidQuery.class;
    }

    @Override
    public Class<PageQueryResult<AclSidDtoCommandAndQueryResult>> getEventResultType() {
        PageQueryResult<AclSidDtoCommandAndQueryResult> result = new PageQueryResult<>(new ArrayList<>());
        //noinspection unchecked
        return (Class<PageQueryResult<AclSidDtoCommandAndQueryResult>>) result.getClass();
    }
}
