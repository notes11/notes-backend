package pl.mm.notes.feature.acl.aclclass.domain.component;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation use to mark entity class as entry to add into
 * {@link pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass}.
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface AclClassEntry {
}
