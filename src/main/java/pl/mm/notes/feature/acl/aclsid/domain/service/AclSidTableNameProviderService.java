package pl.mm.notes.feature.acl.aclsid.domain.service;

import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.service.table.AbstractTableNameProviderService;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;

@Service
public class AclSidTableNameProviderService extends AbstractTableNameProviderService {
    protected AclSidTableNameProviderService() {
        super(AclSid.class);
    }
}
