package pl.mm.notes.feature.acl.aclobjectidentity.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;

import java.util.Optional;

@Repository
public interface AclObjectIdentityRepository extends JpaRepository<AclObjectIdentity, Long> {
    Optional<AclObjectIdentity> findByAclClassAclClassAndObjectIdIdentity(String aclClass, Long objectIdIdentity);
}
