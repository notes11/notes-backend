package pl.mm.notes.feature.acl.aclobjectidentity.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.acl.aclclass.domain.dto.AclClassDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.dto.AclObjectIdentityDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.acl.aclsid.domain.dto.AclSidDtoCommandAndQueryResult;

@Component
public class AclObjectIdentityToAclObjectIdentityCommandResult implements Converter<AclObjectIdentity, AclObjectIdentityDtoCommandAndQueryResult> {
    @Override
    public AclObjectIdentityDtoCommandAndQueryResult convert(MappingContext<AclObjectIdentity, AclObjectIdentityDtoCommandAndQueryResult> context) {
        return AclObjectIdentityDtoCommandAndQueryResult.builder()
                .id(context.getSource().getId())
                .aclClass(AclClassDtoCommandAndQueryResult.builder()
                        .id(context.getSource().getAclClass().getId())
                        .aclClass(context.getSource().getAclClass().getAclClass())
                        .build())
                .objectIdIdentity(context.getSource().getObjectIdIdentity())
                .parentObject(context.getSource().getParentObject())
                .owner(AclSidDtoCommandAndQueryResult.builder()
                        .uuid(context.getSource().getOwner().getUuid())
                        .principal(context.getSource().getOwner().getPrincipal())
                        .sid(context.getSource().getOwner().getSid())
                        .build())
                .entriesInheriting(context.getSource().getEntriesInheriting())
                .build();
    }
}
