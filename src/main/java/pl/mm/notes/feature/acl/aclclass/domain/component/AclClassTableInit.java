package pl.mm.notes.feature.acl.aclclass.domain.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.acl.aclclass.domain.cqrs.command.create.AclClassCreateCommand;
import pl.mm.notes.feature.acl.aclclass.domain.cqrs.command.create.AclClassCreateCommandHandler;
import pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass;
import pl.mm.notes.feature.acl.aclclass.domain.repository.AclClassRepository;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import java.util.Optional;

@Component
public class AclClassTableInit implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AclClassTableInit.class);
    private final AclClassRepository aclClassRepository;
    private final AclClassCreateCommandHandler commandHandler;
    private final EntityManager entityManager;

    @Autowired
    public AclClassTableInit(AclClassRepository aclClassRepository,
                             AclClassCreateCommandHandler commandHandler,
                             EntityManager entityManager) {
        this.aclClassRepository = aclClassRepository;
        this.commandHandler = commandHandler;
        this.entityManager = entityManager;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        addAclClassEntryIfNotExist();
    }

    private void addAclClassEntryIfNotExist() {
        if (LOGGER.isDebugEnabled()) {
            int numberOfEntities = entityManager.getMetamodel().getEntities().size();
            LOGGER.debug(String.format("Found '%d' entities", numberOfEntities));
        }
        for (EntityType<?> entityType : entityManager.getMetamodel().getEntities()) {
            final Class<?> entityJavaType = entityType.getJavaType();
            final AclClassEntry annotation = entityJavaType.getAnnotation(AclClassEntry.class);
            if (annotation != null) {
                final Optional<AclClass> aclClass = aclClassRepository.findByAclClass(entityJavaType.getName());
                if (aclClass.isEmpty()) {
                    commandHandler.execute(AclClassCreateCommand.builder()
                            .aclClass(entityJavaType.getName())
                            .build());
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.info(String.format("Class '%s' already exists in table '%s'", entityType.getClass(),
                                AclClass.class));
                    }
                }
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.info(String.format("Skipped to add class '%s' to '%s' as has not annotation '%s'",
                            entityJavaType.getName(), AclClass.class, AclClassEntry.class));
                }
            }
        }
    }
}
