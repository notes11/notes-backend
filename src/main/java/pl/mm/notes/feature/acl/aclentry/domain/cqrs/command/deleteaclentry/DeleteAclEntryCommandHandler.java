package pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.deleteaclentry;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.command.AbstractDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.acl.aclentry.domain.dto.AclEntryDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclentry.domain.entity.AclEntry;
import pl.mm.notes.feature.acl.aclentry.domain.repository.AclEntryRepository;

import java.util.Optional;

@Service
public class DeleteAclEntryCommandHandler
        extends AbstractDbCommandHandler<DeleteAclEntryCommand, AclEntryDtoCommandAndQueryResult, AclEntryRepository> {

    public DeleteAclEntryCommandHandler(AclEntryRepository aclEntryRepository,
                                        ModelMapper modelMapper) {
        super(aclEntryRepository, modelMapper);
    }

    @Override
    public AclEntryDtoCommandAndQueryResult execute(DeleteAclEntryCommand command) {
        final Optional<AclEntry> aclEntryOptional =
                repository().findByAclObjectIdentity_AclClass_AclClassAndAclObjectIdentity_ObjectIdIdentity(command.getAclClass().getName(), command.getObjectIdIdentity());
        if (aclEntryOptional.isEmpty()) {
            final String message = String.format("Acl entry for class '%s' and object id identity '%d' not found", command.getAclClass(), command.getObjectIdIdentity());
            throw new Http404NotFoundRuntimeException(message);
        }
        final AclEntry aclEntry = aclEntryOptional.get();
        repository().delete(aclEntry);
        return modelMapper.map(aclEntry, AclEntryDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<DeleteAclEntryCommand> getEventType() {
        return DeleteAclEntryCommand.class;
    }

    @Override
    public Class<AclEntryDtoCommandAndQueryResult> getEventResultType() {
        return AclEntryDtoCommandAndQueryResult.class;
    }
}
