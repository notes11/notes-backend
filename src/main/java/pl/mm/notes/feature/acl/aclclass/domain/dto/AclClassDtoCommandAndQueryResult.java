package pl.mm.notes.feature.acl.aclclass.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.query.QueryResult;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@SuperBuilder
public class AclClassDtoCommandAndQueryResult implements CommandResult, QueryResult {
    protected final Long id;
    protected final String aclClass;
}
