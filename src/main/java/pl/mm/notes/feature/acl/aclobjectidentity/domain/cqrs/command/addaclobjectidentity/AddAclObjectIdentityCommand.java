package pl.mm.notes.feature.acl.aclobjectidentity.domain.cqrs.command.addaclobjectidentity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@AllArgsConstructor
@SuperBuilder
public class AddAclObjectIdentityCommand implements Command, Serializable {
    private static final long serialVersionUID = -4418543334971183913L;

    @NotNull(message = "'aclClass' cannot be null.")
    private final Class<?> aclClass;

    @NotNull(message = "'objectIdIdentity' cannot be null.")
    private final Long objectIdIdentity;

    private final Long parentObjectId;

    private final String ownerUsername;

    @NotNull(message = "'entriesInheriting' cannot be null.")
    private final Boolean entriesInheriting;
}
