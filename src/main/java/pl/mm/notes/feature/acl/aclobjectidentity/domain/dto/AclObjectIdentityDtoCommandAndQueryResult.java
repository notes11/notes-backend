package pl.mm.notes.feature.acl.aclobjectidentity.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.query.QueryResult;
import pl.mm.notes.feature.acl.aclclass.domain.dto.AclClassDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.acl.aclsid.domain.dto.AclSidDtoCommandAndQueryResult;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@SuperBuilder
public class AclObjectIdentityDtoCommandAndQueryResult implements CommandResult, QueryResult {
    protected final Long id;
    protected final AclClassDtoCommandAndQueryResult aclClass;
    protected final Long objectIdIdentity;
    protected final AclObjectIdentity parentObject;
    protected final AclSidDtoCommandAndQueryResult owner;
    protected final Boolean entriesInheriting;
}
