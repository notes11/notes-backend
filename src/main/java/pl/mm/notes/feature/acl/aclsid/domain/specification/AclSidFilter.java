package pl.mm.notes.feature.acl.aclsid.domain.specification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class AclSidFilter {
    private Boolean principal;
    private String sid;
}
