package pl.mm.notes.feature.acl.aclsid.domain.cqrs.command.addnewaclsid;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.AclSidCqrsBase;

@Getter
@AllArgsConstructor
@SuperBuilder
public final class AddNewAclSidCommand extends AclSidCqrsBase implements Command {
    private static final long serialVersionUID = -2840583998548274151L;
}
