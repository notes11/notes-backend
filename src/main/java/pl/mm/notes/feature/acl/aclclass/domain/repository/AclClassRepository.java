package pl.mm.notes.feature.acl.aclclass.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass;

import java.util.Optional;

@Repository
public interface AclClassRepository extends JpaRepository<AclClass, Long> {
    Optional<AclClass> findByAclClass(@Param("aclClass") final String aclClass);
}
