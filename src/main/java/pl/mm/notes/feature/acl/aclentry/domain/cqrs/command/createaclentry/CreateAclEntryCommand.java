package pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.createaclentry;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@AllArgsConstructor
@SuperBuilder
public class CreateAclEntryCommand implements Command, Serializable {
    private static final long serialVersionUID = 2833109450676713438L;

    @NotNull(message = "'aclObjectIdentityId' cannot be null.")
    private final Long aclObjectIdentityId;

    @NotNull(message = "'aceOrder' cannot be null.")
    private final Integer aceOrder;

    @NotEmpty(message = "'sid' cannot be empty.")
    @NotNull(message = "'sid' cannot be null.")
    private final String sid;

    @NotNull(message = "'mask' cannot be null.")
    private Integer mask;

    @NotNull(message = "'granting' cannot be null.")
    private Boolean granting;

    @NotNull(message = "'auditSuccess' cannot be null.")
    private Boolean auditSuccess;

    @NotNull(message = "'auditFailure' cannot be null.")
    private Boolean auditFailure;
}
