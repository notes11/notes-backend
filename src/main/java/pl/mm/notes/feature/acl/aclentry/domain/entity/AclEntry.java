package pl.mm.notes.feature.acl.aclentry.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Table(name = "acl_entry", uniqueConstraints = {
        @UniqueConstraint(name = "UQ_AclObjectIdentity_AceOrder", columnNames = {"acl_object_identity", "ace_order"})
})
@Entity
public class AclEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(unique = true, nullable = false, updatable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "acl_object_identity", nullable = false)
    private AclObjectIdentity aclObjectIdentity;

    @Column(name = "ace_order", nullable = false)
    private Integer aceOrder;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sid", nullable = false)
    private AclSid sid;

    @Column(name = "mask", nullable = false)
    private Integer mask;

    @Column(name = "granting", nullable = false)
    private Boolean granting;

    @Column(name = "audit_success", nullable = false)
    private Boolean auditSuccess;

    @Column(name = "audit_failure", nullable = false)
    private Boolean auditFailure;
}
