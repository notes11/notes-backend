package pl.mm.notes.feature.acl.aclentry.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mm.notes.feature.acl.aclentry.domain.entity.AclEntry;

import java.util.Optional;

public interface AclEntryRepository extends JpaRepository<AclEntry, Long> {
    Optional<AclEntry> findByAclObjectIdentity_AclClass_AclClassAndAclObjectIdentity_ObjectIdIdentity(String aclObjectIdentity_aclClass_aclClass, Long aclObjectIdentity_objectIdIdentity);
}