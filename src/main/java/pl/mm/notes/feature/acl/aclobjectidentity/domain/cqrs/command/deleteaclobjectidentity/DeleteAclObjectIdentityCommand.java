package pl.mm.notes.feature.acl.aclobjectidentity.domain.cqrs.command.deleteaclobjectidentity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@AllArgsConstructor
@SuperBuilder
public class DeleteAclObjectIdentityCommand implements Command, Serializable {
    private static final long serialVersionUID = 2636289121203139163L;

    @NotNull(message = "'aclClass' cannot be empty.")
    private final Class<?> aclClass;
    
    @NotNull(message = "'objectIdIdentity' cannot be empty.")
    private final Long objectIdIdentity;
}
