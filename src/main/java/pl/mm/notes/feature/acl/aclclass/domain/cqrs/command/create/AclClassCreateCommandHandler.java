package pl.mm.notes.feature.acl.aclclass.domain.cqrs.command.create;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractDbCommandHandler;
import pl.mm.notes.feature.acl.aclclass.domain.dto.AclClassDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass;
import pl.mm.notes.feature.acl.aclclass.domain.repository.AclClassRepository;

@Service
public class AclClassCreateCommandHandler
        extends AbstractDbCommandHandler<AclClassCreateCommand, AclClassDtoCommandAndQueryResult, AclClassRepository> {

    @Autowired
    public AclClassCreateCommandHandler(AclClassRepository aclClassRepository, ModelMapper modelMapper) {
        super(aclClassRepository, modelMapper);
    }

    @Transactional
    @Override
    public AclClassDtoCommandAndQueryResult execute(AclClassCreateCommand command) {
        AclClass aclClass = modelMapper.map(command, AclClass.class);
        aclClass = repository().save(aclClass);
        return modelMapper.map(aclClass, AclClassDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<AclClassCreateCommand> getEventType() {
        return AclClassCreateCommand.class;
    }

    @Override
    public Class<AclClassDtoCommandAndQueryResult> getEventResultType() {
        return AclClassDtoCommandAndQueryResult.class;
    }
}
