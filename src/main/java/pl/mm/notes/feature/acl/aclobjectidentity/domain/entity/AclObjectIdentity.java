package pl.mm.notes.feature.acl.aclobjectidentity.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import pl.mm.notes.feature.acl.aclclass.domain.entity.AclClass;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Table(name = "acl_object_identity", uniqueConstraints = {
        @UniqueConstraint(name = "unique", columnNames = {"object_id_class", "object_id_identity"})
})
@Entity
public class AclObjectIdentity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(unique = true, nullable = false, updatable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "object_id_class", nullable = false)
    private AclClass aclClass;

    @Column(name = "object_id_identity", nullable = false)
    private Long objectIdIdentity;

    @ManyToOne
    @JoinColumn(name = "parent_object")
    private AclObjectIdentity parentObject;

    @ManyToOne
    @JoinColumn(name = "owner_sid")
    private AclSid owner;

    @Column(name = "entries_inheriting", nullable = false)
    private Boolean entriesInheriting;

}
