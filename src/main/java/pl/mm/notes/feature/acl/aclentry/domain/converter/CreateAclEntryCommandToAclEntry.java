package pl.mm.notes.feature.acl.aclentry.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.createaclentry.CreateAclEntryCommand;
import pl.mm.notes.feature.acl.aclentry.domain.entity.AclEntry;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.entity.AclObjectIdentity;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.repository.AclObjectIdentityRepository;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;
import pl.mm.notes.feature.acl.aclsid.domain.repository.AclSidRepository;

import java.util.Optional;

@Component
public class CreateAclEntryCommandToAclEntry implements Converter<CreateAclEntryCommand, AclEntry> {

    private final AclObjectIdentityRepository aclObjectIdentityRepository;
    private final AclSidRepository aclSidRepository;

    public CreateAclEntryCommandToAclEntry(AclObjectIdentityRepository aclObjectIdentityRepository,
                                           AclSidRepository aclSidRepository) {
        this.aclObjectIdentityRepository = aclObjectIdentityRepository;
        this.aclSidRepository = aclSidRepository;
    }

    @Override
    public AclEntry convert(MappingContext<CreateAclEntryCommand, AclEntry> context) {
        final CreateAclEntryCommand command = context.getSource();
        final Optional<AclObjectIdentity> aclObjectIdentityOptional = aclObjectIdentityRepository.findById(command.getAclObjectIdentityId());

        final String message;
        if (aclObjectIdentityOptional.isEmpty()) {
            message = String.format("Acl entry with id '%d' not found", command.getAclObjectIdentityId());
            throw new Http404NotFoundRuntimeException(message);
        }
        final AclObjectIdentity aclObjectIdentity = aclObjectIdentityOptional.get();

        final Optional<AclSid> aclSidOptional = aclSidRepository.findBySid(command.getSid());
        if (aclSidOptional.isEmpty()) {
            message = String.format("Acl sid with name '%s' not found", command.getSid());
            throw new Http404NotFoundRuntimeException(message);
        }
        final AclSid aclSid = aclSidOptional.get();

        return AclEntry.builder()
                .aclObjectIdentity(aclObjectIdentity)
                .aceOrder(command.getAceOrder())
                .sid(aclSid)
                .mask(command.getMask())
                .granting(command.getGranting())
                .auditSuccess(command.getAuditSuccess())
                .auditFailure(command.getAuditFailure())
                .build();
    }
}
