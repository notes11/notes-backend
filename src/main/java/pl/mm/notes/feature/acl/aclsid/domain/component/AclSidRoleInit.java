package pl.mm.notes.feature.acl.aclsid.domain.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.dto.AclSidDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.command.addnewaclsid.AddNewAclSidCommand;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid.SearchAclSidQuery;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid.SearchAclSidQueryHandler;

import static pl.mm.notes.core.domain.Role.ROLES;

@Component
public class AclSidRoleInit implements ApplicationListener<ContextRefreshedEvent> {

    private final SearchAclSidQueryHandler queryHandler;
    private final CommandDispatcher commandDispatcher;

    @Autowired
    public AclSidRoleInit(SearchAclSidQueryHandler queryHandler,
                          @Qualifier("commandDispatcher") CommandDispatcher commandDispatcher) {
        this.queryHandler = queryHandler;
        this.commandDispatcher = commandDispatcher;
    }

    @Override
    public void onApplicationEvent(@SuppressWarnings("NullableProblems") ContextRefreshedEvent event) {
        addRolesToAclSidIfMissing();
    }

    private void addRolesToAclSidIfMissing() {
        for (final String roleName : ROLES) {
            PageQueryResult<AclSidDtoCommandAndQueryResult> result = queryHandler.execute(SearchAclSidQuery.builder()
                    .sid(roleName)
                    .principal(false)
                    .pageable(Pageable.ofSize(20))
                    .build());
            if (result.getContent().stream().findFirst().isEmpty()) {
                commandDispatcher.execute(AddNewAclSidCommand.builder()
                        .principal(false)
                        .sid(roleName)
                        .build(), AclSidDtoCommandAndQueryResult.class);
            }
        }
    }
}
