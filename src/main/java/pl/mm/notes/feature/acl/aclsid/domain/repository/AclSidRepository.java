package pl.mm.notes.feature.acl.aclsid.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.mm.notes.feature.acl.aclsid.domain.entity.AclSid;

import java.util.Optional;

@Repository
public interface AclSidRepository extends JpaRepository<AclSid, Long>, JpaSpecificationExecutor<AclSid> {
    Optional<AclSid> findBySid(@Param("sid") String sid);
}
