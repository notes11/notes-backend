package pl.mm.notes.feature.acl.aclsid.domain.cqrs.query.searchaclsid;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Pageable;
import pl.mm.jutilities.cqrs.query.Query;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.AclSidCqrsBase;

@Getter
@AllArgsConstructor
@SuperBuilder
public final class SearchAclSidQuery extends AclSidCqrsBase implements Query {
    private static final long serialVersionUID = -3152105462252226287L;
    private final Long id;
    private final Pageable pageable;
}
