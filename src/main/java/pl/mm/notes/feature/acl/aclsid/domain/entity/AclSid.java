package pl.mm.notes.feature.acl.aclsid.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.entity.BaseInfo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Table(name = "acl_sid", uniqueConstraints = @UniqueConstraint(columnNames = {"sid", "principal"}))
@Entity
public class AclSid extends BaseInfo {

    /**
     * Indicates if entry is related to the role or to a user.<br/>
     * <ul>
     *     <li><i>false</i> - role</li>
     *     <li><i>true</i> - user</li>
     * </ul>
     */
    @Column(unique = true, nullable = false, updatable = false)
    private Boolean principal;

    /**
     * Role name or username.
     */
    @Column(unique = true, nullable = false, updatable = false, length = 50)
    private String sid;

}
