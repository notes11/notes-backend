package pl.mm.notes.feature.appinfo.controller.rest.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.notes.feature.appinfo.domain.cqrs.query.environment.EnvironmentQuery;
import pl.mm.notes.feature.appinfo.domain.cqrs.query.environment.EnvironmentQueryResult;

@RequestMapping("/environment")
@RestController
public class EnvironmentController {

    private final QueryDispatcher queryDispatcher;

    @Autowired
    protected EnvironmentController(@Qualifier("queryDispatcher") QueryDispatcher queryDispatcher) {
        this.queryDispatcher = queryDispatcher;
    }

    @GetMapping
    public EnvironmentQueryResult getEnvironment() {
        return queryDispatcher.execute(new EnvironmentQuery(), EnvironmentQueryResult.class);
    }

}
