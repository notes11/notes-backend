package pl.mm.notes.feature.appinfo.domain.cqrs.query.environment;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.mm.jutilities.cqrs.query.QueryHandler;

@Service
public class EnvironmentQueryHandler implements QueryHandler<EnvironmentQuery, EnvironmentQueryResult> {

    private final String environment;

    public EnvironmentQueryHandler(@Value("${app.environment:N/A}") String environment) {
        this.environment = environment;
    }

    @Override
    public EnvironmentQueryResult execute(EnvironmentQuery query) {
        return () -> environment;
    }

    @Override
    public Class<EnvironmentQuery> getEventType() {
        return EnvironmentQuery.class;
    }

    @Override
    public Class<EnvironmentQueryResult> getEventResultType() {
        return EnvironmentQueryResult.class;
    }
}
