package pl.mm.notes.feature.appinfo.domain.cqrs.query.environment;

import pl.mm.jutilities.cqrs.query.QueryResult;

public interface EnvironmentQueryResult extends QueryResult {

    String getEnvironment();

}
