package pl.mm.notes.feature.appinfo.domain.cqrs.query.version;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.mm.jutilities.cqrs.query.QueryHandler;

@Service
public final class VersionQueryHandler implements QueryHandler<VersionQuery, VersionQueryResult> {

    private final String appVersion;

    public VersionQueryHandler(@Value("${app.version:N/A}") String appVersion) {
        this.appVersion = appVersion;
    }

    @Override
    public VersionQueryResult execute(VersionQuery query) {
        return () -> appVersion;
    }

    @Override
    public Class<VersionQuery> getEventType() {
        return VersionQuery.class;
    }

    @Override
    public Class<VersionQueryResult> getEventResultType() {
        return VersionQueryResult.class;
    }

}
