package pl.mm.notes.feature.appinfo.controller.rest.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.notes.feature.appinfo.domain.cqrs.query.version.VersionQuery;
import pl.mm.notes.feature.appinfo.domain.cqrs.query.version.VersionQueryResult;

@RequestMapping("/version")
@RestController
public class VersionController {

    private final QueryDispatcher queryDispatcher;

    @Autowired
    public VersionController(@Qualifier("queryDispatcher") QueryDispatcher queryDispatcher) {
        this.queryDispatcher = queryDispatcher;
    }

    @GetMapping
    public VersionQueryResult getVersion() {
        return queryDispatcher.execute(new VersionQuery(), VersionQueryResult.class);
    }

}
