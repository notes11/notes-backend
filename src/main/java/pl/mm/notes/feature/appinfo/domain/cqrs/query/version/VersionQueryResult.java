package pl.mm.notes.feature.appinfo.domain.cqrs.query.version;

import pl.mm.jutilities.cqrs.query.QueryResult;

public interface VersionQueryResult extends QueryResult {

    String getVersion();

}
