package pl.mm.notes.feature.role.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.role.domain.entity.Role;

@Component
public class RoleToRoleDtoCommandAndQueryResult implements Converter<Role, RoleDtoCommandAndQueryResult> {
    @Override
    public RoleDtoCommandAndQueryResult convert(MappingContext<Role, RoleDtoCommandAndQueryResult> context) {
        return RoleDtoCommandAndQueryResult.builder()
                .uuid(context.getSource().getUuid())
                .createdBy(context.getSource().getCreatedBy())
                .createdTimestamp(context.getSource().getCreatedTimestamp())
                .modifiedBy(context.getSource().getModifiedBy())
                .modificationTimestamp(context.getSource().getModificationTimestamp())
                .roleName(context.getSource().getRoleName())
                .build();
    }
}
