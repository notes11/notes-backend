package pl.mm.notes.feature.role.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.dto.BaseInfoDtoCommandAndQueryResult;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@SuperBuilder
public class RoleDtoCommandAndQueryResult extends BaseInfoDtoCommandAndQueryResult {
    private final String roleName;

    public RoleDtoCommandAndQueryResult(UUID uuid, String createdBy, Instant createdTimestamp, String modifiedBy,
                                        Instant modificationTimestamp, String roleName) {
        super(uuid, createdBy, createdTimestamp, modifiedBy, modificationTimestamp);
        this.roleName = roleName;
    }
}
