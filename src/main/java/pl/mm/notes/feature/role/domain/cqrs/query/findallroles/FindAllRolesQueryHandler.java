package pl.mm.notes.feature.role.domain.cqrs.query.findallroles;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.role.domain.entity.Role;
import pl.mm.notes.feature.role.domain.repository.RoleRepository;
import pl.mm.notes.feature.role.domain.search.specification.RoleSpecification;

import java.util.ArrayList;

@Service
public class FindAllRolesQueryHandler
        extends AbstractBaseInfoDbQueryHandler<FindAllRolesQuery, PageQueryResult<RoleDtoCommandAndQueryResult>, RoleRepository> {

    protected FindAllRolesQueryHandler(RoleRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Transactional(readOnly = true)
    @Override
    public PageQueryResult<RoleDtoCommandAndQueryResult> execute(FindAllRolesQuery query) {
        final RoleSpecification specification = new RoleSpecification(query.getFilter());
        final Page<Role> roles = repository.findAll(specification, query.getPageable());
        final Page<RoleDtoCommandAndQueryResult> results = roles.map(role -> modelMapper.map(role, RoleDtoCommandAndQueryResult.class));
        return new PageQueryResult<>(results.getContent(), query.getPageable(), results.getTotalElements());
    }

    @Override
    public Class<FindAllRolesQuery> getEventType() {
        return FindAllRolesQuery.class;
    }

    @Override
    public Class<PageQueryResult<RoleDtoCommandAndQueryResult>> getEventResultType() {
        PageQueryResult<RoleDtoCommandAndQueryResult> result = new PageQueryResult<>(new ArrayList<>());
        //noinspection unchecked
        return (Class<PageQueryResult<RoleDtoCommandAndQueryResult>>) result.getClass();
    }

}
