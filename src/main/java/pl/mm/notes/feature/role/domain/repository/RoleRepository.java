package pl.mm.notes.feature.role.domain.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.mm.notes.core.domain.repositroy.BaseInfoRepository;
import pl.mm.notes.feature.role.domain.entity.Role;

import java.util.Optional;
import java.util.Set;

@Repository
public interface RoleRepository extends BaseInfoRepository<Role, Long>, JpaSpecificationExecutor<Role> {

    Optional<Role> findByRoleName(@Param("roleName") String roleName);

    Set<Role> findByRoleNameIn(Set<String> roleName);

}
