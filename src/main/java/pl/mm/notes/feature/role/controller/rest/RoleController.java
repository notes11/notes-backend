package pl.mm.notes.feature.role.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.role.domain.cqrs.query.findallroles.FindAllRolesQuery;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.role.domain.search.filter.RoleFilter;

import static pl.mm.notes.core.domain.Role.ADMIN;

@RequestMapping("/role")
@RestController
public class RoleController {

    private final QueryDispatcher queryDispatcher;

    @Autowired
    public RoleController(@Qualifier("queryDispatcher") QueryDispatcher queryDispatcher) {
        this.queryDispatcher = queryDispatcher;
    }

    @PreAuthorize("isAuthenticated() and hasRole('" + ADMIN + "')")
    @GetMapping
    public PageQueryResult<RoleDtoCommandAndQueryResult> findAllRoles(Pageable pageable, RoleFilter filter) {
        //noinspection unchecked
        return queryDispatcher.execute(FindAllRolesQuery
                .builder()
                .pageable(pageable)
                .filter(filter)
                .build(), PageQueryResult.class);
    }

}
