package pl.mm.notes.feature.role.domain.search.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.search.filter.BaseFilter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class RoleFilter extends BaseFilter {
    private static final long serialVersionUID = -749771129573067039L;
    private String roleName;
}
