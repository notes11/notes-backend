package pl.mm.notes.feature.role.domain.cqrs.query.findallroles;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.cqrs.query.BaseFilterQuery;
import pl.mm.notes.feature.role.domain.search.filter.RoleFilter;

@SuperBuilder
@Getter
@AllArgsConstructor
public final class FindAllRolesQuery extends BaseFilterQuery<RoleFilter> {
}
