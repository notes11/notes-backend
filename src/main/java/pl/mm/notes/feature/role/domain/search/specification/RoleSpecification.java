package pl.mm.notes.feature.role.domain.search.specification;

import pl.mm.notes.core.domain.search.specification.BaseFilterSpecification;
import pl.mm.notes.feature.role.domain.entity.Role;
import pl.mm.notes.feature.role.domain.entity.Role_;
import pl.mm.notes.feature.role.domain.search.filter.RoleFilter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;

public class RoleSpecification extends BaseFilterSpecification<RoleFilter, Role> {
    private static final long serialVersionUID = -6495822503638628775L;

    public RoleSpecification(RoleFilter filter) {
        super(filter);
    }

    @Override
    public Predicate toPredicate(Root<Role> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
        final Predicate predicateFromSuper = super.toPredicate(r, q, cb);
        final List<Predicate> predicates = new ArrayList<>();
        if (predicateFromSuper != null) {
            predicates.add(predicateFromSuper);
        }

        if (hasText(filter.getRoleName())) {
            predicates.add(cb.like(r.get(Role_.ROLE_NAME), filter.getRoleName() + "%"));
        }

        return cb.and(predicates.toArray(Predicate[]::new));
    }
}
