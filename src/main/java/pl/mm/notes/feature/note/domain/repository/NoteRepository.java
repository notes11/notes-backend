package pl.mm.notes.feature.note.domain.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.mm.notes.core.domain.repositroy.BaseInfoRepository;
import pl.mm.notes.feature.note.domain.entity.Note;

@Repository
public interface NoteRepository extends BaseInfoRepository<Note, Long>, JpaSpecificationExecutor<Note> {
}
