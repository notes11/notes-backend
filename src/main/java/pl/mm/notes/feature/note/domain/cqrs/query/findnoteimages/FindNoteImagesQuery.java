package pl.mm.notes.feature.note.domain.cqrs.query.findnoteimages;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.cqrs.query.BaseFilterQuery;
import pl.mm.notes.feature.note.domain.search.filter.NoteImageFilter;

@Getter
@SuperBuilder
@AllArgsConstructor
public final class FindNoteImagesQuery extends BaseFilterQuery<NoteImageFilter> {
}
