package pl.mm.notes.feature.note.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.entity.BaseInfo;
import pl.mm.notes.feature.acl.aclclass.domain.component.AclClassEntry;
import pl.mm.notes.feature.user.domain.entity.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@AclClassEntry
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "note")
public class Note extends BaseInfo {

    @Column(name = "Title", nullable = false)
    private String title;

    @Lob
    @Column(name = "Note", nullable = false, columnDefinition = "TEXT")
    private String noteText;

    @Column(name = "IsPublic", nullable = false)
    private Boolean isPublic;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OwnerId", nullable = false)
    private User owner;

}
