package pl.mm.notes.feature.note.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.user.domain.converter.UserToUserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

@Component
public class NoteToNoteDtoCommandAndQueryResult implements Converter<Note, NoteDtoCommandAndQueryResult> {

    private final UserToUserDtoCommandAndQueryResult userConverter;

    public NoteToNoteDtoCommandAndQueryResult(UserToUserDtoCommandAndQueryResult userConverter) {
        this.userConverter = userConverter;
    }

    @Override
    public NoteDtoCommandAndQueryResult convert(MappingContext<Note, NoteDtoCommandAndQueryResult> mappingContext) {
        return NoteDtoCommandAndQueryResult.builder()
                .uuid(mappingContext.getSource().getUuid())
                .createdBy(mappingContext.getSource().getCreatedBy())
                .createdTimestamp(mappingContext.getSource().getCreatedTimestamp())
                .modifiedBy(mappingContext.getSource().getModifiedBy())
                .modificationTimestamp(mappingContext.getSource().getModificationTimestamp())
                .title(mappingContext.getSource().getTitle())
                .noteText(mappingContext.getSource().getNoteText())
                .isPublic(mappingContext.getSource().getIsPublic())
                .owner(userConverter.convert(mappingContext.create(mappingContext.getSource().getOwner(), UserDtoCommandAndQueryResult.class)))
                .build();
    }
}
