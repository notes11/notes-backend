package pl.mm.notes.feature.note.domain.search.specification;

import pl.mm.notes.core.domain.search.specification.BaseFilterSpecification;
import pl.mm.notes.feature.note.domain.entity.NoteImage;
import pl.mm.notes.feature.note.domain.entity.NoteImage_;
import pl.mm.notes.feature.note.domain.entity.Note_;
import pl.mm.notes.feature.note.domain.search.filter.NoteImageFilter;
import pl.mm.notes.feature.user.domain.entity.User_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;

public class NoteImageSpecification extends BaseFilterSpecification<NoteImageFilter, NoteImage> {
    private static final long serialVersionUID = -5608817836218343273L;

    private final String owner;

    public NoteImageSpecification(NoteImageFilter filter, String owner) {
        super(filter);
        this.owner = owner;
    }

    @Override
    public Predicate toPredicate(Root<NoteImage> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
        final Predicate predicateFromSuper = super.toPredicate(r, q, cb);
        final List<Predicate> predicates = new ArrayList<>();
        if (predicateFromSuper != null) {
            predicates.add(predicateFromSuper);
        }

        Predicate predicate = cb.equal(r.get(NoteImage_.note).get(Note_.owner).get(User_.username), owner);
        predicate = cb.or(predicate, cb.equal(r.get(NoteImage_.note).get(Note_.isPublic), true));
        predicates.add(predicate);

        if (hasText(filter.getImageName())) {
            predicates.add(cb.like(r.get(NoteImage_.imageName), filter.getImageName() + "%"));
        }

        return cb.and(predicates.toArray(Predicate[]::new));
    }
}
