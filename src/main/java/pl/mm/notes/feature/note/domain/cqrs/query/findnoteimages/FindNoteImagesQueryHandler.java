package pl.mm.notes.feature.note.domain.cqrs.query.findnoteimages;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.note.domain.converter.NoteImageToNoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.NoteImage;
import pl.mm.notes.feature.note.domain.repository.NoteImageRepository;
import pl.mm.notes.feature.note.domain.search.specification.NoteImageSpecification;

import java.util.ArrayList;

@Service
public class FindNoteImagesQueryHandler
        extends AbstractBaseInfoDbQueryHandler<FindNoteImagesQuery, PageQueryResult<NoteImageDtoCommandAndQueryResult>, NoteImageRepository> {

    public FindNoteImagesQueryHandler(NoteImageRepository repository,
                                      ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Override
    public PageQueryResult<NoteImageDtoCommandAndQueryResult> execute(FindNoteImagesQuery query) {
        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        final NoteImageSpecification specification = new NoteImageSpecification(query.getFilter(), username);
        final Page<NoteImage> noteImages = repository.findAll(specification, query.getPageable());
        final Page<NoteImageDtoCommandAndQueryResult> results = noteImages.map(noteImage -> modelMapper.map(noteImage, NoteImageDtoCommandAndQueryResult.class));
        return new PageQueryResult<>(results.getContent(), query.getPageable(), noteImages.getTotalElements());
    }

    @Override
    public Class<FindNoteImagesQuery> getEventType() {
        return FindNoteImagesQuery.class;
    }

    @Override
    public Class<PageQueryResult<NoteImageDtoCommandAndQueryResult>> getEventResultType() {
        PageQueryResult<NoteImageToNoteImageDtoCommandAndQueryResult> result = new PageQueryResult<>(new ArrayList<>());
        //noinspection unchecked
        return (Class<PageQueryResult<NoteImageDtoCommandAndQueryResult>>) result.getClass();
    }
}
