package pl.mm.notes.feature.note.domain.cqrs.query.downloadnoteimage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.query.Query;

import java.util.UUID;

@Getter
@SuperBuilder
@AllArgsConstructor
public class DownloadNoteImageQuery implements Query {
    private final UUID uuid;
}
