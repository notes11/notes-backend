package pl.mm.notes.feature.note.domain.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.mm.notes.core.domain.repositroy.BaseInfoRepository;
import pl.mm.notes.feature.note.domain.entity.NoteImage;

@Repository
public interface NoteImageRepository extends BaseInfoRepository<NoteImage, Long>, JpaSpecificationExecutor<NoteImage> {
}
