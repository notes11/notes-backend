package pl.mm.notes.feature.note.domain.cqrs.command.createnote;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotBlank;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CreateNoteCommand implements Command {

    @Length(min = 1, message = "'title' need contains at least 1 character")
    @NotBlank(message = "'title' cannot be blank.")
    private final String title;

    @Length(min = 1, message = "'noteText' need contains at least 1 character")
    @NotBlank(message = "'noteText' cannot be blank.")
    private final String noteText;

    private final Boolean isPublic;

}
