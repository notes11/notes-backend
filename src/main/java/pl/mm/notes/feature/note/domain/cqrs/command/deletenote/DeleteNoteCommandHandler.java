package pl.mm.notes.feature.note.domain.cqrs.command.deletenote;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.NoteTableNameProviderService;
import pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.deleteaclentry.DeleteAclEntryCommand;
import pl.mm.notes.feature.acl.aclentry.domain.dto.AclEntryDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.cqrs.command.deleteaclobjectidentity.DeleteAclObjectIdentityCommand;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.dto.AclObjectIdentityDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.repository.NoteRepository;

import java.util.Optional;

@Service
public class DeleteNoteCommandHandler
        extends AbstractBaseInfoDbCommandHandler<DeleteNoteCommand, NoteDtoCommandAndQueryResult, NoteRepository> {

    private final CommandDispatcher commandDispatcher;

    @Autowired
    protected DeleteNoteCommandHandler(NoteRepository repository,
                                       ModelMapper modelMapper,
                                       NoteTableNameProviderService tableNameProviderService,
                                       @Qualifier("commandDispatcher") CommandDispatcher commandDispatcher) {
        super(repository, modelMapper, tableNameProviderService);
        this.commandDispatcher = commandDispatcher;
    }

    @Transactional
    @Override
    public NoteDtoCommandAndQueryResult execute(DeleteNoteCommand command) {
        Optional<Note> noteOptional = repository().findByUuid(command.getNoteUuid());
        if (noteOptional.isEmpty()) {
            throw new Http404NotFoundRuntimeException("Note with UUID '" + command.getNoteUuid() + "' not found ");
        }
        Note note = noteOptional.get();

        commandDispatcher.execute(DeleteAclEntryCommand.builder()
                .aclClass(Note.class)
                .objectIdIdentity(note.getId())
                .build(), AclEntryDtoCommandAndQueryResult.class);

        commandDispatcher.execute(DeleteAclObjectIdentityCommand.builder()
                .aclClass(Note.class)
                .objectIdIdentity(note.getId())
                .build(), AclObjectIdentityDtoCommandAndQueryResult.class);

        repository().delete(note);
        return modelMapper.map(note, NoteDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<DeleteNoteCommand> getEventType() {
        return DeleteNoteCommand.class;
    }

    @Override
    public Class<NoteDtoCommandAndQueryResult> getEventResultType() {
        return NoteDtoCommandAndQueryResult.class;
    }

}
