package pl.mm.notes.feature.note.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.dto.BaseInfoDtoCommandAndQueryResult;

@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@SuperBuilder
public class NoteImageDtoCommandAndQueryResult extends BaseInfoDtoCommandAndQueryResult {
    private final String imageName;
}
