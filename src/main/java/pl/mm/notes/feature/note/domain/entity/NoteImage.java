package pl.mm.notes.feature.note.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Type;
import pl.mm.notes.core.domain.entity.BaseInfo;
import pl.mm.notes.feature.acl.aclclass.domain.component.AclClassEntry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@AclClassEntry
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "note_image")
public class NoteImage extends BaseInfo {

    @Column(name = "image_name", nullable = false)
    private String imageName;

    @Type(type = "org.hibernate.type.ImageType")
    @Lob
    @Column(name = "image_blob", nullable = false, columnDefinition = "BLOB")
    private byte[] imageBlob;

    @JoinColumn(name = "note_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Note note;

}
