package pl.mm.notes.feature.note.domain.cqrs.command.createnoteimage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;
import pl.mm.jutilities.cqrs.command.Command;

import java.util.UUID;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CreateNoteImageCommand implements Command {
    private final UUID noteUuid;
    private final MultipartFile image;
}
