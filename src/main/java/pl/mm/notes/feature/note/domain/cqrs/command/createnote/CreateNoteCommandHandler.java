package pl.mm.notes.feature.note.domain.cqrs.command.createnote;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.NoteTableNameProviderService;
import pl.mm.notes.feature.acl.aclentry.domain.cqrs.command.createaclentry.CreateAclEntryCommand;
import pl.mm.notes.feature.acl.aclentry.domain.dto.AclEntryDtoCommandAndQueryResult;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.cqrs.command.addaclobjectidentity.AddAclObjectIdentityCommand;
import pl.mm.notes.feature.acl.aclobjectidentity.domain.dto.AclObjectIdentityDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.repository.NoteRepository;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class CreateNoteCommandHandler
        extends AbstractBaseInfoDbCommandHandler<CreateNoteCommand, NoteDtoCommandAndQueryResult, NoteRepository> {

    private final CommandDispatcher commandDispatcher;
    private final UserRepository userRepository;

    public CreateNoteCommandHandler(NoteRepository repository,
                                    ModelMapper modelMapper,
                                    NoteTableNameProviderService tableNameProviderService,
                                    @Qualifier("commandDispatcher") CommandDispatcher commandDispatcher,
                                    UserRepository userRepository) {
        super(repository, modelMapper, tableNameProviderService);
        this.commandDispatcher = commandDispatcher;
        this.userRepository = userRepository;
    }

    @Override
    public NoteDtoCommandAndQueryResult execute(CreateNoteCommand command) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new Http404NotFoundRuntimeException("User with username '" + username + "' not found");
        }
        Note note = modelMapper.map(command, Note.class);
        note = create(note, username, username);
        setDefaultValueForIsPublic(note);
        note.setOwner(user.get());

        note = repository().save(note);
        final AclObjectIdentityDtoCommandAndQueryResult objectIdentityEntryResult = addAclObjectIdentityEntry(note, username);
        addAclEntry(username, objectIdentityEntryResult);
        return modelMapper.map(note, NoteDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<CreateNoteCommand> getEventType() {
        return CreateNoteCommand.class;
    }

    @Override
    public Class<NoteDtoCommandAndQueryResult> getEventResultType() {
        return NoteDtoCommandAndQueryResult.class;
    }

    private static void setDefaultValueForIsPublic(Note note) {
        if (note.getIsPublic() == null) {
            note.setIsPublic(false);
        }
    }

    private AclObjectIdentityDtoCommandAndQueryResult addAclObjectIdentityEntry(Note note, String username) {
        final AddAclObjectIdentityCommand command = AddAclObjectIdentityCommand.builder()
                .aclClass(Note.class)
                .objectIdIdentity(note.getId())
                .parentObjectId(null)
                .ownerUsername(username)
                .entriesInheriting(false)
                .build();
        return commandDispatcher.execute(command, AclObjectIdentityDtoCommandAndQueryResult.class);
    }

    private void addAclEntry(String username, AclObjectIdentityDtoCommandAndQueryResult objectIdentityEntryResult) {
        final CreateAclEntryCommand command = CreateAclEntryCommand.builder()
                .aclObjectIdentityId(objectIdentityEntryResult.getId())
                .aceOrder(1)
                .sid(username)
                .mask(1)
                .granting(true)
                .auditSuccess(true)
                .auditFailure(true)
                .build();
        commandDispatcher.execute(command, AclEntryDtoCommandAndQueryResult.class);
    }

}
