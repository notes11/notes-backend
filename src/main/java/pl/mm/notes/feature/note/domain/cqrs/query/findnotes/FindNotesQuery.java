package pl.mm.notes.feature.note.domain.cqrs.query.findnotes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.cqrs.query.BaseFilterQuery;
import pl.mm.notes.feature.note.domain.search.filter.NoteFilter;

@Getter
@SuperBuilder
@AllArgsConstructor
public final class FindNotesQuery extends BaseFilterQuery<NoteFilter> {
}
