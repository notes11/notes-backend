package pl.mm.notes.feature.note.domain.cqrs.command.deletenoteimage;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http403ForbiddenRuntimeException;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.NoteImageTableNameProviderService;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.NoteImage;
import pl.mm.notes.feature.note.domain.repository.NoteImageRepository;

import java.util.Optional;

@Service
public class DeleteNoteImageCommandHandler
        extends AbstractBaseInfoDbCommandHandler<DeleteNoteImageCommand, NoteImageDtoCommandAndQueryResult, NoteImageRepository> {

    public DeleteNoteImageCommandHandler(NoteImageRepository repository,
                                         ModelMapper modelMapper,
                                         NoteImageTableNameProviderService tableNameProviderService) {
        super(repository, modelMapper, tableNameProviderService);
    }

    @Override
    public NoteImageDtoCommandAndQueryResult execute(DeleteNoteImageCommand command) {
        Optional<NoteImage> noteImageOptional = repository().findByUuid(command.getNoteImageUuid());
        if (noteImageOptional.isEmpty()) {
            throw new Http404NotFoundRuntimeException(String.format("Image with UUID '%s' not found", command.getNoteImageUuid().toString()));
        }
        final NoteImage noteImage = noteImageOptional.get();
        validateIfCanBeDeleted(noteImage);
        repository().delete(noteImage);
        return modelMapper.map(noteImage, NoteImageDtoCommandAndQueryResult.class);
    }


    public void validateIfCanBeDeleted(NoteImage noteImage) {
        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!noteImage.getNote().getOwner().getUsername().equals(username)) {
            throw new Http403ForbiddenRuntimeException("You don't have permissions to execute delete action on this resource");
        }
    }

    @Override
    public Class<DeleteNoteImageCommand> getEventType() {
        return DeleteNoteImageCommand.class;
    }

    @Override
    public Class<NoteImageDtoCommandAndQueryResult> getEventResultType() {
        return NoteImageDtoCommandAndQueryResult.class;
    }
}
