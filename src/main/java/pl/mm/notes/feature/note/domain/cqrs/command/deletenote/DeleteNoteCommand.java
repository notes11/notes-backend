package pl.mm.notes.feature.note.domain.cqrs.command.deletenote;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import pl.mm.jutilities.cqrs.command.Command;

import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class DeleteNoteCommand implements Command {
    private final UUID noteUuid;
}
