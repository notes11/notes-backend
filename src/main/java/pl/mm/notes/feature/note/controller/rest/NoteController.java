package pl.mm.notes.feature.note.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.note.domain.cqrs.command.createnote.CreateNoteCommand;
import pl.mm.notes.feature.note.domain.cqrs.command.createnoteimage.CreateNoteImageCommand;
import pl.mm.notes.feature.note.domain.cqrs.command.deletenote.DeleteNoteCommand;
import pl.mm.notes.feature.note.domain.cqrs.command.deletenoteimage.DeleteNoteImageCommand;
import pl.mm.notes.feature.note.domain.cqrs.command.updatenote.UpdateNoteCommand;
import pl.mm.notes.feature.note.domain.cqrs.query.findnoteimages.FindNoteImagesQuery;
import pl.mm.notes.feature.note.domain.cqrs.query.findnotes.FindNotesQuery;
import pl.mm.notes.feature.note.domain.cqrs.query.downloadnoteimage.DownloadNoteImageQuery;
import pl.mm.notes.feature.note.domain.cqrs.query.downloadnoteimage.DownloadNoteImageQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.search.filter.NoteFilter;
import pl.mm.notes.feature.note.domain.search.filter.NoteImageFilter;

import java.util.UUID;

import static pl.mm.notes.core.domain.Role.ADMIN;
import static pl.mm.notes.core.domain.Role.USER;

@RequestMapping("/note")
@RestController
public class NoteController {

    private final CommandDispatcher commandDispatcher;
    private final QueryDispatcher queryDispatcher;

    @Autowired
    public NoteController(@Qualifier("commandDispatcher") CommandDispatcher commandDispatcher,
                          @Qualifier("queryDispatcher") QueryDispatcher queryDispatcher) {
        this.commandDispatcher = commandDispatcher;
        this.queryDispatcher = queryDispatcher;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @PostMapping
    public NoteDtoCommandAndQueryResult createNote(@Validated @RequestBody CreateNoteCommand createNoteCommand) {
        return commandDispatcher.execute(createNoteCommand, NoteDtoCommandAndQueryResult.class);
    }

    @GetMapping
    public PageQueryResult<NoteDtoCommandAndQueryResult> findNotes(NoteFilter filter, Pageable pageable) {
        //noinspection unchecked
        return queryDispatcher.execute(FindNotesQuery.builder()
                .filter(filter)
                .pageable(pageable)
                .build(), PageQueryResult.class);
    }

    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @PutMapping("/{noteUuid}")
    public NoteDtoCommandAndQueryResult updateNote(@Validated @RequestBody UpdateNoteCommand command,
                                                   @PathVariable UUID noteUuid) {
        return commandDispatcher.execute(UpdateNoteCommand.builder()
                .noteUuid(noteUuid)
                .title(command.getTitle())
                .noteText(command.getNoteText())
                .build(), NoteDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{noteUuid}")
    public NoteDtoCommandAndQueryResult deleteNote(@PathVariable UUID noteUuid) {
        return commandDispatcher.execute(DeleteNoteCommand.builder()
                .noteUuid(noteUuid)
                .build(), NoteDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/{noteUuid}/image")
    public NoteImageDtoCommandAndQueryResult addImageToNote(@PathVariable UUID noteUuid,
                                                            @RequestParam MultipartFile image) {
        return commandDispatcher.execute(CreateNoteImageCommand.builder()
                .noteUuid(noteUuid)
                .image(image)
                .build(), NoteImageDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @GetMapping(path = "/image")
    public Page<NoteImageDtoCommandAndQueryResult> findNoteImages(NoteImageFilter filter, Pageable pageable) {
        //noinspection unchecked
        return queryDispatcher.execute(FindNoteImagesQuery.builder()
                .filter(filter)
                .pageable(pageable)
                .build(), Page.class);
    }

    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @GetMapping(path = "/image/{uuid}")
    public byte[] downloadNoteImage(@PathVariable UUID uuid) {
        final DownloadNoteImageQueryResult result = queryDispatcher.execute(DownloadNoteImageQuery.builder()
                .uuid(uuid)
                .build(), DownloadNoteImageQueryResult.class);
        return result.getImageBytes();
    }

    @PreAuthorize("isAuthenticated() and hasAnyAuthority('" + USER + "', '" + ADMIN + "')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/image/{uuid}")
    public NoteImageDtoCommandAndQueryResult deleteNoteImage(@PathVariable UUID uuid) {
        return commandDispatcher.execute(DeleteNoteImageCommand.builder()
                .noteImageUuid(uuid)
                .build(), NoteImageDtoCommandAndQueryResult.class);
    }

}
