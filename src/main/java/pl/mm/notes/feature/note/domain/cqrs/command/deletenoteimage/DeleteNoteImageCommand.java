package pl.mm.notes.feature.note.domain.cqrs.command.deletenoteimage;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import pl.mm.jutilities.cqrs.command.Command;

import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class DeleteNoteImageCommand implements Command {
    private final UUID noteImageUuid;
}
