package pl.mm.notes.feature.note.domain.cqrs.command.updatenote;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.notes.feature.note.domain.cqrs.command.createnote.CreateNoteCommand;

import java.util.UUID;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class UpdateNoteCommand extends CreateNoteCommand implements Command {

    private final UUID noteUuid;

}
