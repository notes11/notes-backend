package pl.mm.notes.feature.note.domain.cqrs.query.downloadnoteimage;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.exception.Http403ForbiddenRuntimeException;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.note.domain.entity.NoteImage;
import pl.mm.notes.feature.note.domain.repository.NoteImageRepository;

import java.util.Optional;

@Service
public class DownloadNoteImageQueryHandler extends AbstractBaseInfoDbQueryHandler<DownloadNoteImageQuery, DownloadNoteImageQueryResult, NoteImageRepository> {

    public DownloadNoteImageQueryHandler(NoteImageRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Override
    public DownloadNoteImageQueryResult execute(DownloadNoteImageQuery query) {
        Optional<NoteImage> noteImageOptional = repository.findByUuid(query.getUuid());
        if (noteImageOptional.isEmpty()) {
            final String message = String.format("Note image with uuid '%s' not found", query.getUuid());
            throw new Http404NotFoundRuntimeException(message);
        }
        NoteImage noteImage = noteImageOptional.get();

        if (userIsNoteOwner(noteImage)) {
            throw new Http403ForbiddenRuntimeException("You cannot access this image");
        }

        return DownloadNoteImageQueryResult.builder()
                .imageBytes(noteImage.getImageBlob())
                .build();
    }

    private boolean userIsNoteOwner(final NoteImage noteImage) {
        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        final String ownerUsername = noteImage.getNote().getOwner().getUsername();
        return !username.equals(ownerUsername);
    }

    @Override
    public Class<DownloadNoteImageQuery> getEventType() {
        return DownloadNoteImageQuery.class;
    }

    @Override
    public Class<DownloadNoteImageQueryResult> getEventResultType() {
        return DownloadNoteImageQueryResult.class;
    }
}