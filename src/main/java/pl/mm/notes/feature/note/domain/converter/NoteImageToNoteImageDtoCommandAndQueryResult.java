package pl.mm.notes.feature.note.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Service;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.NoteImage;

@Service
public class NoteImageToNoteImageDtoCommandAndQueryResult implements Converter<NoteImage, NoteImageDtoCommandAndQueryResult> {
    @Override
    public NoteImageDtoCommandAndQueryResult convert(MappingContext<NoteImage, NoteImageDtoCommandAndQueryResult> context) {
        return NoteImageDtoCommandAndQueryResult.builder()
                .uuid(context.getSource().getUuid())
                .createdBy(context.getSource().getCreatedBy())
                .createdTimestamp(context.getSource().getCreatedTimestamp())
                .modifiedBy(context.getSource().getModifiedBy())
                .modificationTimestamp(context.getSource().getModificationTimestamp())
                .imageName(context.getSource().getImageName())
                .build();
    }
}
