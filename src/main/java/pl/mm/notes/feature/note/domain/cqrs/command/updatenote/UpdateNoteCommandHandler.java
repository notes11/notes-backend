package pl.mm.notes.feature.note.domain.cqrs.command.updatenote;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.NoteTableNameProviderService;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.repository.NoteRepository;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class UpdateNoteCommandHandler
        extends AbstractBaseInfoDbCommandHandler<UpdateNoteCommand, NoteDtoCommandAndQueryResult, NoteRepository> {

    private final UserRepository userRepository;

    protected UpdateNoteCommandHandler(NoteRepository repository,
                                       ModelMapper modelMapper,
                                       NoteTableNameProviderService tableNameProviderService,
                                       UserRepository userRepository) {
        super(repository, modelMapper, tableNameProviderService);
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public NoteDtoCommandAndQueryResult execute(UpdateNoteCommand command) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new Http404NotFoundRuntimeException("User with username '" + username + "' not found");
        }

        Optional<Note> noteOptional = repository().findByUuid(command.getNoteUuid());
        if (noteOptional.isEmpty()) {
            throw new Http404NotFoundRuntimeException("Note with UUID '" + command.getNoteUuid() + "' not found");
        }

        Note note = noteOptional.get();
        note = update(note, user.get().getUsername());
        note.setTitle(command.getTitle());
        note.setNoteText(command.getNoteText());
        note = repository().save(note);
        return modelMapper.map(note, NoteDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<UpdateNoteCommand> getEventType() {
        return UpdateNoteCommand.class;
    }

    @Override
    public Class<NoteDtoCommandAndQueryResult> getEventResultType() {
        return NoteDtoCommandAndQueryResult.class;
    }

}
