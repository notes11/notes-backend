package pl.mm.notes.feature.note.domain.cqrs.query.downloadnoteimage;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.query.QueryResult;

@Getter
@SuperBuilder
@AllArgsConstructor
public class DownloadNoteImageQueryResult implements QueryResult {
    private final byte[] imageBytes;
}
