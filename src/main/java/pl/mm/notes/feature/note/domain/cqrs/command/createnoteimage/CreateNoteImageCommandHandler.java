package pl.mm.notes.feature.note.domain.cqrs.command.createnoteimage;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.exception.Http500InternalServerErrorRuntimeException;
import pl.mm.notes.core.domain.service.table.NoteImageTableNameProviderService;
import pl.mm.notes.feature.note.domain.dto.NoteImageDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.entity.NoteImage;
import pl.mm.notes.feature.note.domain.repository.NoteImageRepository;
import pl.mm.notes.feature.note.domain.repository.NoteRepository;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.io.IOException;
import java.util.Optional;

@Service
public class CreateNoteImageCommandHandler
        extends AbstractBaseInfoDbCommandHandler<CreateNoteImageCommand, NoteImageDtoCommandAndQueryResult, NoteImageRepository> {

    private final UserRepository userRepository;
    private final NoteRepository noteRepository;

    public CreateNoteImageCommandHandler(NoteImageRepository repository,
                                         ModelMapper modelMapper,
                                         NoteImageTableNameProviderService tableNameProviderService,
                                         UserRepository userRepository,
                                         NoteRepository noteRepository) {
        super(repository, modelMapper, tableNameProviderService);
        this.userRepository = userRepository;
        this.noteRepository = noteRepository;
    }

    @Override
    public NoteImageDtoCommandAndQueryResult execute(CreateNoteImageCommand createNoteImageCommand) {
        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        final Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new Http404NotFoundRuntimeException("User with username '" + username + "' not found");
        }

        final Optional<Note> noteOptional = noteRepository.findByUuid(createNoteImageCommand.getNoteUuid());
        if (noteOptional.isEmpty()) {
            throw new Http404NotFoundRuntimeException(String.format("Note with uuid '%s' note found", createNoteImageCommand.getNoteUuid()));
        }


        NoteImage noteImage = new NoteImage();
        noteImage = create(noteImage, username, username);
        noteImage.setNote(noteOptional.get());
        noteImage.setImageName(createNoteImageCommand.getImage().getOriginalFilename());
        try {
            noteImage.setImageBlob(createNoteImageCommand.getImage().getBytes());
        } catch (IOException e) {
            final String message = String.format("Failed to save image '%s'", createNoteImageCommand.getImage().getOriginalFilename());
            throw new Http500InternalServerErrorRuntimeException(message);
        }
        noteImage = repository().save(noteImage);
        return modelMapper.map(noteImage, NoteImageDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<CreateNoteImageCommand> getEventType() {
        return CreateNoteImageCommand.class;
    }

    @Override
    public Class<NoteImageDtoCommandAndQueryResult> getEventResultType() {
        return NoteImageDtoCommandAndQueryResult.class;
    }
}
