package pl.mm.notes.feature.note.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.dto.BaseInfoDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@SuperBuilder
public class NoteDtoCommandAndQueryResult extends BaseInfoDtoCommandAndQueryResult {

    private String title;
    private String noteText;
    private Boolean isPublic;
    private UserDtoCommandAndQueryResult owner;

    public NoteDtoCommandAndQueryResult(String title, String noteText, Boolean isPublic) {
        this.title = title;
        this.noteText = noteText;
        this.isPublic = isPublic;
    }

    public NoteDtoCommandAndQueryResult(UUID uuid, String createdBy, Instant createdTimestamp, String modifiedBy,
                                        Instant modificationTimestamp, String title, String noteText, Boolean isPublic) {
        super(uuid, createdBy, createdTimestamp, modifiedBy, modificationTimestamp);
        this.title = title;
        this.noteText = noteText;
        this.isPublic = isPublic;
    }
}
