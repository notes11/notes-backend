package pl.mm.notes.feature.note.domain.cqrs.query.findnotes;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.note.domain.dto.NoteDtoCommandAndQueryResult;
import pl.mm.notes.feature.note.domain.search.specification.NoteSpecification;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.repository.NoteRepository;

import java.util.ArrayList;

@Service
public class FindNotesQueryHandler
        extends AbstractBaseInfoDbQueryHandler<FindNotesQuery, PageQueryResult<NoteDtoCommandAndQueryResult>, NoteRepository> {

    public FindNotesQueryHandler(NoteRepository repository,
                                 ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Transactional(readOnly = true)
    @Override
    public PageQueryResult<NoteDtoCommandAndQueryResult> execute(FindNotesQuery query) {
        final String username = SecurityContextHolder.getContext().getAuthentication().getName();
        final NoteSpecification specification = new NoteSpecification(username, query.getFilter());
        Page<Note> notes = repository.findAll(specification, query.getPageable());
        Page<NoteDtoCommandAndQueryResult> results = notes.map(note -> modelMapper.map(note, NoteDtoCommandAndQueryResult.class));
        return new PageQueryResult<>(results.getContent(), query.getPageable(), notes.getTotalElements());
    }

    @Override
    public Class<FindNotesQuery> getEventType() {
        return FindNotesQuery.class;
    }

    @Override
    public Class<PageQueryResult<NoteDtoCommandAndQueryResult>> getEventResultType() {
        PageQueryResult<NoteDtoCommandAndQueryResult> result = new PageQueryResult<>(new ArrayList<>());
        //noinspection unchecked
        return (Class<PageQueryResult<NoteDtoCommandAndQueryResult>>) result.getClass();
    }

}
