package pl.mm.notes.feature.note.domain.search.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.search.filter.BaseFilter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@SuperBuilder
public final class NoteImageFilter extends BaseFilter {
    private static final long serialVersionUID = -2676321951889913906L;
    private String imageName;
}
