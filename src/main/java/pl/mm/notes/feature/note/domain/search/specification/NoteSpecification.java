package pl.mm.notes.feature.note.domain.search.specification;

import pl.mm.notes.core.domain.search.specification.BaseFilterSpecification;
import pl.mm.notes.feature.note.domain.search.filter.NoteFilter;
import pl.mm.notes.feature.note.domain.entity.Note;
import pl.mm.notes.feature.note.domain.entity.Note_;
import pl.mm.notes.feature.user.domain.entity.User_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.StringUtils.hasText;

public class NoteSpecification extends BaseFilterSpecification<NoteFilter, Note> {
    private static final long serialVersionUID = -8604375722686823348L;

    private final String owner;

    public NoteSpecification(String owner, NoteFilter filter) {
        super(filter);
        this.owner = owner;
    }

    @Override
    public Predicate toPredicate(Root<Note> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
        final Predicate predicateFromSuper = super.toPredicate(r, q, cb);
        final List<Predicate> predicates = new ArrayList<>();
        if (predicateFromSuper != null) {
            predicates.add(predicateFromSuper);
        }

        Predicate predicate = cb.equal(r.get(Note_.owner).get(User_.username), owner);
        predicate = cb.or(predicate, cb.equal(r.get(Note_.isPublic), true));
        predicates.add(predicate);

        if (hasText(filter.getTitle())) {
            predicates.add(cb.like(r.get(Note_.title), filter.getTitle() + "%"));
        }

        if (hasText(filter.getNoteText())) {
            predicates.add(cb.like(r.get(Note_.noteText), "%" + filter.getNoteText() + "%"));
        }

        return cb.and(predicates.toArray(Predicate[]::new));
    }

}
