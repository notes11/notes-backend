package pl.mm.notes.feature.user.domain.cqrs.command.changepassword;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@AllArgsConstructor
@Builder
public final class ChangePasswordCommand implements Command {

    @NotNull(message = "'userUuid' cannot be empty.")
    private final UUID userUuid;

    @Length(min = 5, max = 80, message = "'password' need contains at least 5 characters and at most 80 characters.")
    @NotBlank(message = "'password' cannot be empty.")
    private final String newPassword;

}
