package pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.notes.core.domain.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Builder
@AllArgsConstructor
public final class CreateUserAccountCommand implements Command {

    @Length(min = 5, max = 50, message = "'username' need contains at least 5 characters and at most 50 characters.")
    @NotBlank(message = "'username' cannot be blank.")
    private final String username;

    @Length(min = 5, max = 80, message = "'password' need contains at least 5 characters and at most 80 characters.")
    @NotBlank(message = "'password' cannot be empty.")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private final String password;

    @Length(min = 5, max = 100, message = "'addressEmail' need contains at least 5 characters and at most 100 characters.")
    @Email(message = "Please provide a valid email address.")
    @NotBlank(message = "'addressEmail' cannot be empty.")
    private final String addressEmail;

    @Builder.Default
    @JsonIgnore
    private final Set<String> rolesToAssign = new LinkedHashSet<>(Collections.singleton(Role.USER));

}
