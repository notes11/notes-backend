package pl.mm.notes.feature.user.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;

import java.util.stream.Collectors;

@Component
public class UserToUserDtoCommandAndQueryResult implements Converter<User, UserDtoCommandAndQueryResult> {
    @Override
    public UserDtoCommandAndQueryResult convert(MappingContext<User, UserDtoCommandAndQueryResult> mappingContext) {
        return UserDtoCommandAndQueryResult.builder()
                .uuid(mappingContext.getSource().getUuid())
                .createdBy(mappingContext.getSource().getCreatedBy())
                .createdTimestamp(mappingContext.getSource().getCreatedTimestamp())
                .modifiedBy(mappingContext.getSource().getModifiedBy())
                .modificationTimestamp(mappingContext.getSource().getModificationTimestamp())
                .username(mappingContext.getSource().getUsername())
                .firstName(mappingContext.getSource().getFirstName())
                .lastName(mappingContext.getSource().getLastName())
                .addressEmail(mappingContext.getSource().getAddressEmail())
                .isActive(mappingContext.getSource().getIsActive())
                .roles(mappingContext.getSource().getRoles().stream()
                        .map(role -> RoleDtoCommandAndQueryResult.builder()
                                .uuid(role.getUuid())
                                .createdBy(role.getCreatedBy())
                                .createdTimestamp(role.getCreatedTimestamp())
                                .modifiedBy(role.getModifiedBy())
                                .modificationTimestamp(role.getModificationTimestamp())
                                .roleName(role.getRoleName())
                                .build())
                        .collect(Collectors.toUnmodifiableList()))
                .build();
    }
}
