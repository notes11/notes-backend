package pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.exception.Http500InternalServerErrorRuntimeException;
import pl.mm.notes.core.domain.service.table.UserTableNameProviderService;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.command.addnewaclsid.AddNewAclSidCommand;
import pl.mm.notes.feature.acl.aclsid.domain.cqrs.command.addnewaclsid.AddNewAclSidCommandHandler;
import pl.mm.notes.feature.role.domain.entity.Role;
import pl.mm.notes.feature.role.domain.repository.RoleRepository;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CreateUserAccountCommandHandler
        extends AbstractBaseInfoDbCommandHandler<CreateUserAccountCommand, UserDtoCommandAndQueryResult, UserRepository> {

    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final AddNewAclSidCommandHandler commandHandler;

    @Autowired
    protected CreateUserAccountCommandHandler(UserRepository repository,
                                              ModelMapper modelMapper,
                                              UserTableNameProviderService tableNameProviderService,
                                              PasswordEncoder passwordEncoder,
                                              RoleRepository roleRepository,
                                              AddNewAclSidCommandHandler commandHandler) {
        super(repository, modelMapper, tableNameProviderService);
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.commandHandler = commandHandler;
    }

    @Transactional
    @Override
    public UserDtoCommandAndQueryResult execute(CreateUserAccountCommand command) {
        User user = modelMapper.map(command, User.class);
        user = create(user, command.getUsername(), command.getUsername());
        user.setPassword(passwordEncoder.encode(command.getPassword()));
        user.setIsActive(false);
        assignRoles(command, user);
        user = repository().save(user);
        commandHandler.execute(AddNewAclSidCommand.builder()
                .principal(true)
                .sid(user.getUsername())
                .build());
        return modelMapper.map(user, UserDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<CreateUserAccountCommand> getEventType() {
        return CreateUserAccountCommand.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }

    private void assignRoles(final CreateUserAccountCommand command, User user) {
        final Set<Role> foundRoles = roleRepository.findByRoleNameIn(command.getRolesToAssign());
        if (foundRoles.size() < command.getRolesToAssign().size()) {
            Set<String> missingRoles = command.getRolesToAssign().stream()
                    .filter(roleName -> foundRoles.stream().filter(role -> role.getRoleName().equals(roleName)).isParallel())
                    .collect(Collectors.toSet());
            throw new Http404NotFoundRuntimeException(String.format("Role(s) '%s' not found", String.join(",", missingRoles)));
        } else if (foundRoles.size() > command.getRolesToAssign().size()) {
            throw new Http500InternalServerErrorRuntimeException("Found too many roles");
        } else {
            user.getRoles().addAll(foundRoles);
        }
    }

}
