package pl.mm.notes.feature.user.domain.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.mm.notes.core.domain.repositroy.BaseInfoRepository;
import pl.mm.notes.feature.user.domain.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseInfoRepository<User, Long> {

    Optional<User> findByUsername(@Param("username") String username);

}
