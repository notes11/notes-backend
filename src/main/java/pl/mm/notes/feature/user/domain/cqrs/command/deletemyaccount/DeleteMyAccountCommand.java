package pl.mm.notes.feature.user.domain.cqrs.command.deletemyaccount;

import org.springframework.security.core.context.SecurityContextHolder;
import pl.mm.jutilities.cqrs.command.Command;

public final class DeleteMyAccountCommand implements Command {

    public String getMyUsername() {
        if (SecurityContextHolder.getContext().getAuthentication() != null)
            return SecurityContextHolder.getContext().getAuthentication().getName();
        return null;
    }

}
