package pl.mm.notes.feature.user.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.feature.user.domain.entity.User;

@Component
public class CreateUserAccountCommandToUserConverter implements Converter<CreateUserAccountCommand, User> {

    @Override
    public User convert(MappingContext<CreateUserAccountCommand, User> context) {
        return User.builder()
                .username(context.getSource().getUsername())
                .password(context.getSource().getPassword())
                .addressEmail(context.getSource().getAddressEmail())
                .build();
    }
}
