package pl.mm.notes.feature.user.domain.cqrs.command.changepassword;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.UserTableNameProviderService;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class ChangePasswordCommandHandler
        extends AbstractBaseInfoDbCommandHandler<ChangePasswordCommand, UserDtoCommandAndQueryResult, UserRepository> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    protected ChangePasswordCommandHandler(UserRepository repository,
                                           ModelMapper modelMapper,
                                           UserTableNameProviderService tableNameProviderService,
                                           PasswordEncoder passwordEncoder) {
        super(repository, modelMapper, tableNameProviderService);
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    @Override
    public UserDtoCommandAndQueryResult execute(ChangePasswordCommand command) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> userOptional = repository().findByUuid(command.getUserUuid());
        if (userOptional.isEmpty()) {
            throw new Http404NotFoundRuntimeException("User with UUID '" + command.getUserUuid() + "' not found");
        }
        User user = userOptional.get();
        user = update(user, username);
        user.setPassword(passwordEncoder.encode(command.getNewPassword()));
        user = repository().save(user);
        return modelMapper.map(user, UserDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<ChangePasswordCommand> getEventType() {
        return ChangePasswordCommand.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }

}
