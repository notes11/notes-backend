package pl.mm.notes.feature.user.domain.cqrs.command.updateuser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import pl.mm.jutilities.cqrs.command.Command;

import javax.validation.constraints.NotBlank;

@Getter
@Builder
@AllArgsConstructor
public final class UpdateUserCommand implements Command {

    @Length(max = 50, message = "'firstName' can contains at most 50 characters.")
    @NotBlank(message = "'firstName' cannot be blank.")
    private final String firstName;

    @Length(max = 50, message = "'lastName' can contains at most 50 characters.")
    @NotBlank(message = "'lastName' cannot be blank.")
    private final String lastName;

}
