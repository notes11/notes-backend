package pl.mm.notes.feature.user.domain.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import pl.mm.notes.feature.role.domain.converter.RoleToRoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;

import java.util.stream.Collectors;

@Component
public class UserToUserQueryResultDto implements Converter<User, UserDtoCommandAndQueryResult> {

    private final RoleToRoleDtoCommandAndQueryResult roleConverter;

    public UserToUserQueryResultDto(RoleToRoleDtoCommandAndQueryResult roleToRoleDtoCommandAndQueryResult) {
        this.roleConverter = roleToRoleDtoCommandAndQueryResult;
    }

    @Override
    public UserDtoCommandAndQueryResult convert(MappingContext<User, UserDtoCommandAndQueryResult> context) {
        return UserDtoCommandAndQueryResult.builder()
                .uuid(context.getSource().getUuid())
                .createdBy(context.getSource().getCreatedBy())
                .createdTimestamp(context.getSource().getCreatedTimestamp())
                .modifiedBy(context.getSource().getModifiedBy())
                .modificationTimestamp(context.getSource().getModificationTimestamp())
                .username(context.getSource().getUsername())
                .firstName(context.getSource().getFirstName())
                .lastName(context.getSource().getLastName())
                .addressEmail(context.getSource().getAddressEmail())
                .isActive(context.getSource().getIsActive())
                .roles(context.getSource().getRoles().stream()
                        .map(role -> roleConverter.convert(context.create(role, RoleDtoCommandAndQueryResult.class)))
                        .collect(Collectors.toUnmodifiableList()))
                .build();
    }
}
