package pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import pl.mm.jutilities.cqrs.query.Query;

@Getter
@AllArgsConstructor
@Builder
public final class FindUserByUsernameQuery implements Query {

    private final String username;

}
