package pl.mm.notes.feature.user.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.dto.BaseInfoDtoCommandAndQueryResult;
import pl.mm.notes.feature.role.domain.dto.RoleDtoCommandAndQueryResult;

import java.util.Collection;

@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@SuperBuilder
public class UserDtoCommandAndQueryResult extends BaseInfoDtoCommandAndQueryResult {
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String addressEmail;
    private final Boolean isActive;
    private final Collection<RoleDtoCommandAndQueryResult> roles;
}
