package pl.mm.notes.feature.user.domain.cqrs.command.assignroletouser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import pl.mm.jutilities.cqrs.command.Command;

import java.util.UUID;

@Getter
@AllArgsConstructor
@Builder
public final class AssignRoleToUserCommand implements Command {
    private final UUID userUuid;
    private final String roleName;
}
