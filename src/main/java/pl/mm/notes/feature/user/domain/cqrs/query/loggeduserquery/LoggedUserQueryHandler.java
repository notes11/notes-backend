package pl.mm.notes.feature.user.domain.cqrs.query.loggeduserquery;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class LoggedUserQueryHandler
        extends AbstractBaseInfoDbQueryHandler<LoggedUserQuery, UserDtoCommandAndQueryResult, UserRepository> {

    @Autowired
    public LoggedUserQueryHandler(UserRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Transactional(readOnly = true)
    @Override
    public UserDtoCommandAndQueryResult execute(LoggedUserQuery query) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> user = repository.findByUsername(username);
        if (user.isPresent())
            return modelMapper.map(user.get(), UserDtoCommandAndQueryResult.class);
        throw new Http404NotFoundRuntimeException("User '" + username + "' not found");
    }

    @Override
    public Class<LoggedUserQuery> getEventType() {
        return LoggedUserQuery.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }
}
