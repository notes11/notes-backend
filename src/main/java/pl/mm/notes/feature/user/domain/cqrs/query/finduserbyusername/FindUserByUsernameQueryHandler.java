package pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class FindUserByUsernameQueryHandler
        extends AbstractBaseInfoDbQueryHandler<FindUserByUsernameQuery, UserDtoCommandAndQueryResult, UserRepository> {

    protected FindUserByUsernameQueryHandler(UserRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Transactional(readOnly = true)
    @Override
    public UserDtoCommandAndQueryResult execute(FindUserByUsernameQuery query) {
        Optional<User> user = repository.findByUsername(query.getUsername());
        if (user.isEmpty())
            throw new Http404NotFoundRuntimeException("User with username '" + query.getUsername() + "' not found");
        return modelMapper.map(user.get(), UserDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<FindUserByUsernameQuery> getEventType() {
        return FindUserByUsernameQuery.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }

}
