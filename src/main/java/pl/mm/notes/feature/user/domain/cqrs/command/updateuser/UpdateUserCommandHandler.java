package pl.mm.notes.feature.user.domain.cqrs.command.updateuser;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.UserTableNameProviderService;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class UpdateUserCommandHandler
        extends AbstractBaseInfoDbCommandHandler<UpdateUserCommand, UserDtoCommandAndQueryResult, UserRepository> {

    @Autowired
    protected UpdateUserCommandHandler(UserRepository repository,
                                       ModelMapper modelMapper,
                                       UserTableNameProviderService tableNameProviderService) {
        super(repository, modelMapper, tableNameProviderService);
    }

    @Transactional
    @Override
    public UserDtoCommandAndQueryResult execute(UpdateUserCommand command) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> userToUpdate = repository().findByUsername(username);
        if (userToUpdate.isPresent()) {
            User user = userToUpdate.get();
            user = update(user, username);
            user.setFirstName(command.getFirstName());
            user.setLastName(command.getLastName());
            user = repository().save(user);
            return modelMapper.map(user, UserDtoCommandAndQueryResult.class);
        } else {
            throw new Http404NotFoundRuntimeException("User '" + username + "' to update not found");
        }
    }

    @Override
    public Class<UpdateUserCommand> getEventType() {
        return UpdateUserCommand.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }

}
