package pl.mm.notes.feature.user.domain.cqrs.command.assignroletouser;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.UserTableNameProviderService;
import pl.mm.notes.feature.role.domain.entity.Role;
import pl.mm.notes.feature.role.domain.repository.RoleRepository;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class AssignRoleToUserCommandHandler
        extends AbstractBaseInfoDbCommandHandler<AssignRoleToUserCommand, UserDtoCommandAndQueryResult, UserRepository> {

    private final RoleRepository roleRepository;

    @Autowired
    protected AssignRoleToUserCommandHandler(UserRepository repository,
                                             ModelMapper modelMapper,
                                             UserTableNameProviderService tableNameProviderService,
                                             RoleRepository roleRepository) {
        super(repository, modelMapper, tableNameProviderService);
        this.roleRepository = roleRepository;
    }

    @Transactional
    @Override
    public UserDtoCommandAndQueryResult execute(AssignRoleToUserCommand command) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Optional<User> userOptional = repository().findByUuid(command.getUserUuid());
        if (userOptional.isEmpty())
            throw new Http404NotFoundRuntimeException("User with UUID '" + command.getUserUuid() + "' not found");

        Optional<Role> roleOptional = roleRepository.findByRoleName(command.getRoleName());
        if (roleOptional.isEmpty())
            throw new Http404NotFoundRuntimeException("Role '" + command.getRoleName() + "' not found");

        User user = userOptional.get();
        Role role = roleOptional.get();

        user = update(user, username);
        user.getRoles().add(role);
        user = repository().save(user);
        return modelMapper.map(user, UserDtoCommandAndQueryResult.class);
    }

    @Override
    public Class<AssignRoleToUserCommand> getEventType() {
        return AssignRoleToUserCommand.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }

}
