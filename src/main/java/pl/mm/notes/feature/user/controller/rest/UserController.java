package pl.mm.notes.feature.user.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.user.domain.cqrs.command.assignroletouser.AssignRoleToUserCommand;
import pl.mm.notes.feature.user.domain.cqrs.command.changepassword.ChangePasswordCommand;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.feature.user.domain.cqrs.command.deletemyaccount.DeleteMyAccountCommand;
import pl.mm.notes.feature.user.domain.cqrs.command.updateuser.UpdateUserCommand;
import pl.mm.notes.feature.user.domain.cqrs.query.loggeduserquery.LoggedUserQuery;
import pl.mm.notes.feature.user.domain.cqrs.query.searchusers.SearchUsersQuery;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

import java.util.UUID;

import static pl.mm.notes.core.domain.Role.ADMIN;

@RequestMapping("/user")
@RestController
public class UserController {
    private final CommandDispatcher commandDispatcher;
    private final QueryDispatcher queryDispatcher;

    @Autowired
    public UserController(@Qualifier("commandDispatcher") CommandDispatcher commandDispatcher,
                          @Qualifier("queryDispatcher") QueryDispatcher queryDispatcher) {
        this.commandDispatcher = commandDispatcher;
        this.queryDispatcher = queryDispatcher;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("isAnonymous()")
    @PostMapping
    public UserDtoCommandAndQueryResult createUserAccount(@Validated @RequestBody CreateUserAccountCommand createUser) {
        return commandDispatcher.execute(CreateUserAccountCommand.builder()
                .username(createUser.getUsername())
                .password(createUser.getPassword())
                .addressEmail(createUser.getAddressEmail())
                .build(), UserDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping
    public UserDtoCommandAndQueryResult getInformationAboutLoggedUser() {
        return queryDispatcher.execute(new LoggedUserQuery(), UserDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated() and hasAuthority('" + ADMIN + "')")
    @GetMapping("/search")
    public PageQueryResult<UserDtoCommandAndQueryResult> searchUsers(Pageable pageable) {
        //noinspection unchecked
        return queryDispatcher.execute(new SearchUsersQuery(pageable), PageQueryResult.class);
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping
    public UserDtoCommandAndQueryResult updateUser(@Validated @RequestBody UpdateUserCommand updateUserCommand) {
        return commandDispatcher.execute(updateUserCommand, UserDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated() and hasAuthority('" + ADMIN + "')")
    @PatchMapping("/{userUuid}/assign-role/{roleName}")
    public UserDtoCommandAndQueryResult assignRoleToUser(@PathVariable UUID userUuid, @PathVariable String roleName) {
        return commandDispatcher.execute(AssignRoleToUserCommand.builder()
                .userUuid(userUuid)
                .roleName(roleName)
                .build(), UserDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated()")
    @PatchMapping("/{userUuid}/change-password")
    public UserDtoCommandAndQueryResult changePassword(@PathVariable UUID userUuid, @RequestBody String newPassword) {
        return commandDispatcher.execute(ChangePasswordCommand.builder()
                .userUuid(userUuid)
                .newPassword(newPassword)
                .build(), UserDtoCommandAndQueryResult.class);
    }

    @PreAuthorize("isAuthenticated()")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping
    public UserDtoCommandAndQueryResult deleteMyAccount() {
        return commandDispatcher.execute(new DeleteMyAccountCommand(), UserDtoCommandAndQueryResult.class);
    }
}
