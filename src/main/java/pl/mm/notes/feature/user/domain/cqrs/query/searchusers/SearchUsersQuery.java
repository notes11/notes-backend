package pl.mm.notes.feature.user.domain.cqrs.query.searchusers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.domain.Pageable;
import pl.mm.jutilities.cqrs.query.Query;

@Getter
@AllArgsConstructor
@Builder
public final class SearchUsersQuery implements Query {
    private final Pageable pageable;
}
