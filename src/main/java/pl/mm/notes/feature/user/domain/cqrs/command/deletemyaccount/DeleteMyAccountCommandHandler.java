package pl.mm.notes.feature.user.domain.cqrs.command.deletemyaccount;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.command.AbstractBaseInfoDbCommandHandler;
import pl.mm.notes.core.domain.exception.Http404NotFoundRuntimeException;
import pl.mm.notes.core.domain.service.table.UserTableNameProviderService;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.Optional;

@Service
public class DeleteMyAccountCommandHandler
        extends AbstractBaseInfoDbCommandHandler<DeleteMyAccountCommand, UserDtoCommandAndQueryResult, UserRepository> {

    @Autowired
    protected DeleteMyAccountCommandHandler(UserRepository repository,
                                            ModelMapper modelMapper,
                                            UserTableNameProviderService tableNameProviderService) {
        super(repository, modelMapper, tableNameProviderService);
    }

    @Transactional
    @Override
    public UserDtoCommandAndQueryResult execute(DeleteMyAccountCommand command) {
        Optional<User> userToDelete = repository().findByUsername(command.getMyUsername());
        if (userToDelete.isPresent()) {
            repository().delete(userToDelete.get());
            return modelMapper.map(userToDelete.get(), UserDtoCommandAndQueryResult.class);
        } else {
            throw new Http404NotFoundRuntimeException("User with username '" + command.getMyUsername() + "' not found");
        }
    }

    @Override
    public Class<DeleteMyAccountCommand> getEventType() {
        return DeleteMyAccountCommand.class;
    }

    @Override
    public Class<UserDtoCommandAndQueryResult> getEventResultType() {
        return UserDtoCommandAndQueryResult.class;
    }

}
