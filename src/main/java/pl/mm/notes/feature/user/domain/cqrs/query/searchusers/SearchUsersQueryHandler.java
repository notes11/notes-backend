package pl.mm.notes.feature.user.domain.cqrs.query.searchusers;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.notes.core.domain.cqrs.query.AbstractBaseInfoDbQueryHandler;
import pl.mm.notes.core.domain.cqrs.query.PageQueryResult;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;
import pl.mm.notes.feature.user.domain.entity.User;
import pl.mm.notes.feature.user.domain.repository.UserRepository;

import java.util.ArrayList;

@Component
public class SearchUsersQueryHandler
        extends AbstractBaseInfoDbQueryHandler<SearchUsersQuery, PageQueryResult<UserDtoCommandAndQueryResult>, UserRepository> {

    protected SearchUsersQueryHandler(UserRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
    }

    @Transactional(readOnly = true)
    @Override
    public PageQueryResult<UserDtoCommandAndQueryResult> execute(SearchUsersQuery query) {
        Page<User> users = repository.findAll(query.getPageable());
        Page<UserDtoCommandAndQueryResult> result = users.map(user -> modelMapper.map(user, UserDtoCommandAndQueryResult.class));
        return new PageQueryResult<>(result.getContent(), query.getPageable(), users.getTotalElements());
    }

    @Override
    public Class<SearchUsersQuery> getEventType() {
        return SearchUsersQuery.class;
    }

    @Override
    public Class<PageQueryResult<UserDtoCommandAndQueryResult>> getEventResultType() {
        PageQueryResult<UserDtoCommandAndQueryResult> result = new PageQueryResult<>(new ArrayList<>());
        //noinspection unchecked
        return (Class<PageQueryResult<UserDtoCommandAndQueryResult>>) result.getClass();
    }

}
