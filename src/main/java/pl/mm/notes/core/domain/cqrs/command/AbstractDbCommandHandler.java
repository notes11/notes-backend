package pl.mm.notes.core.domain.cqrs.command;

import org.modelmapper.ModelMapper;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.command.CommandResult;

public abstract class AbstractDbCommandHandler
        <C extends Command, R extends CommandResult, RepositoryType extends Repository<?, ?>>
        implements CommandHandler<C, R> {

    private final RepositoryType repository;
    protected final ModelMapper modelMapper;

    protected AbstractDbCommandHandler(RepositoryType repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    protected final RepositoryType repository() {
        return repository;
    }

    @Transactional
    @Override
    public abstract R execute(final C command);
}
