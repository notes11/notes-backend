package pl.mm.notes.core.domain.cqrs.query;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Pageable;
import pl.mm.jutilities.cqrs.query.Query;
import pl.mm.notes.core.domain.search.filter.Filter;

@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor
@SuperBuilder
public abstract class AbstractFilterQuery<F extends Filter> implements Query {
    private final Pageable pageable;
    private final F filter;
}
