package pl.mm.notes.core.domain.search.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class BaseFilter implements Filter, Serializable {
    private static final long serialVersionUID = -436703975090825424L;

    private UUID uuid;

    private String createdBy;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Instant createdTimestampAfter;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Instant createdTimestampBefore;

    private String modifiedBy;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Instant modifiedTimestampAfter;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Instant modifiedTimestampBefore;

}
