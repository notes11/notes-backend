package pl.mm.notes.core.domain.exception;

import org.springframework.http.HttpStatus;

public class Http404NotFoundRuntimeException extends AbstractHttpNotesRuntimeException {
    private static final long serialVersionUID = -838810257269409579L;

    public Http404NotFoundRuntimeException() {
        super(HttpStatus.NOT_FOUND);
    }

    public Http404NotFoundRuntimeException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }

    public Http404NotFoundRuntimeException(String message, Throwable cause) {
        super(message, cause, HttpStatus.NOT_FOUND);
    }

    public Http404NotFoundRuntimeException(Throwable cause) {
        super(cause, HttpStatus.NOT_FOUND);
    }

    public Http404NotFoundRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, HttpStatus.NOT_FOUND);
    }
}
