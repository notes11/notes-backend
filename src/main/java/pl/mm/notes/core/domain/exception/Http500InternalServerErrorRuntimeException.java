package pl.mm.notes.core.domain.exception;

import org.springframework.http.HttpStatus;

public class Http500InternalServerErrorRuntimeException extends AbstractHttpNotesRuntimeException {
    private static final long serialVersionUID = 3470757705394810878L;

    public Http500InternalServerErrorRuntimeException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    public Http500InternalServerErrorRuntimeException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public Http500InternalServerErrorRuntimeException(String message, Throwable cause) {
        super(message, cause, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public Http500InternalServerErrorRuntimeException(Throwable cause) {
        super(cause, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public Http500InternalServerErrorRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
