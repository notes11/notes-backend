package pl.mm.notes.core.domain.service.table;

import org.springframework.stereotype.Service;
import pl.mm.notes.feature.user.domain.entity.User;

@Service
public class UserTableNameProviderService extends AbstractTableNameProviderService {
    public UserTableNameProviderService() {
        super(User.class);
    }
}
