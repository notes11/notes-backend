package pl.mm.notes.core.domain;

import org.springframework.security.core.context.SecurityContextHolder;

public abstract class NotesApplicationGeneral {

    public static final String SYSTEM_USERNAME = "system";

    private NotesApplicationGeneral() {
    }

    public static String getUsername() {
        if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null) {
            return SecurityContextHolder.getContext().getAuthentication().getName();
        } else {
            return SYSTEM_USERNAME;
        }
    }

}
