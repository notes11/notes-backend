package pl.mm.notes.core.domain.service.table;

public interface TableNameProviderService {

    String getTableName();

}
