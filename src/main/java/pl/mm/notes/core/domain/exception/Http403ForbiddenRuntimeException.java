package pl.mm.notes.core.domain.exception;

import org.springframework.http.HttpStatus;

public class Http403ForbiddenRuntimeException extends AbstractHttpNotesRuntimeException {
    private static final long serialVersionUID = 5540863153530611579L;

    public Http403ForbiddenRuntimeException() {
        super(HttpStatus.FORBIDDEN);
    }
    
    public Http403ForbiddenRuntimeException(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }

    public Http403ForbiddenRuntimeException(String message, Throwable cause) {
        super(message, cause, HttpStatus.NOT_FOUND);
    }

    public Http403ForbiddenRuntimeException(Throwable cause) {
        super(cause, HttpStatus.NOT_FOUND);
    }

    public Http403ForbiddenRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, HttpStatus.NOT_FOUND);
    }

}
