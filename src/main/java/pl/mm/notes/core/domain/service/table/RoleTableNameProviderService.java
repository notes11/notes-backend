package pl.mm.notes.core.domain.service.table;

import org.springframework.stereotype.Service;
import pl.mm.notes.feature.role.domain.entity.Role;

@Service
public class RoleTableNameProviderService extends AbstractTableNameProviderService {
    public RoleTableNameProviderService() {
        super(Role.class);
    }
}
