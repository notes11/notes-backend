package pl.mm.notes.core.domain.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.notes.core.domain.Role;
import pl.mm.notes.feature.user.domain.cqrs.command.createuseraccount.CreateUserAccountCommand;
import pl.mm.notes.feature.user.domain.cqrs.query.finduserbyusername.FindUserByUsernameQuery;
import pl.mm.notes.feature.user.domain.dto.UserDtoCommandAndQueryResult;

import java.util.Set;
import java.util.UUID;

@DependsOn({"addCommandHandlersAfterStartupComponent", "addQueryHandlersAfterStartupComponent"})
@Component
public class AdminAccountInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private final Logger logger = LoggerFactory.getLogger(AdminAccountInitializer.class);

    @Value("${app.admin.username}")
    private String adminUsername;
    @Value("${app.admin.password}")
    private String adminPassword;

    private final QueryDispatcher queryDispatcher;
    private final CommandDispatcher commandDispatcher;

    @Autowired
    public AdminAccountInitializer(@Qualifier("queryDispatcher") QueryDispatcher queryDispatcher,
                                   @Qualifier("commandDispatcher") CommandDispatcher commandDispatcher) {
        this.queryDispatcher = queryDispatcher;
        this.commandDispatcher = commandDispatcher;
    }

    @Override
    public void onApplicationEvent(@SuppressWarnings("NullableProblems") ContextRefreshedEvent contextRefreshedEvent) {
        createAdminAccountIfNotExists();
    }

    private void createAdminAccountIfNotExists() {
        if (!adminAccountExists(adminUsername)) {
            String password;
            if (adminPassword.isBlank()) {
                password = UUID.randomUUID().toString();
            } else {
                password = adminPassword;
            }
            String noAddressEmail = "noaddressemail@sys.com";

            CreateUserAccountCommand command = CreateUserAccountCommand.builder()
                    .username(adminUsername)
                    .password(password)
                    .addressEmail(noAddressEmail)
                    .rolesToAssign(Set.of(Role.USER, Role.ADMIN))
                    .build();
            this.commandDispatcher.execute(command, UserDtoCommandAndQueryResult.class);

            logger.info("Administration account created: " + command);
            logger.info("Please change password as soon as possible for created administration account");
        } else {
            logger.info("Administration account already exists");
        }
    }

    private boolean adminAccountExists(String adminUsername) {
        boolean isExist = false;
        try {
            UserDtoCommandAndQueryResult userQueryResult = queryDispatcher.execute(FindUserByUsernameQuery.builder()
                    .username(adminUsername)
                    .build(), UserDtoCommandAndQueryResult.class);
            if (userQueryResult != null)
                isExist = true;
        } catch (Exception ignored) {
        }
        return isExist;
    }

}
