package pl.mm.notes.core.domain.repositroy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mm.notes.core.domain.entity.BaseInfo;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface BaseInfoRepository<T extends BaseInfo, ID extends Long> extends JpaRepository<T, ID> {

    Optional<T> findByUuid(UUID uuid);

}
