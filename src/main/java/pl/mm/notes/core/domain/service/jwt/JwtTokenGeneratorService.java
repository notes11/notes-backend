package pl.mm.notes.core.domain.service.jwt;

import org.springframework.security.core.Authentication;

public interface JwtTokenGeneratorService {

    String generateJwtToken(Authentication authResult);

}
