package pl.mm.notes.core.domain.cqrs.query;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import pl.mm.notes.core.domain.search.filter.BaseFilter;

@Getter
@AllArgsConstructor
@SuperBuilder
public class BaseFilterQuery<F extends BaseFilter> extends AbstractFilterQuery<F> {
}
