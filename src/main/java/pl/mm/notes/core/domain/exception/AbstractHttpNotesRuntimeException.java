package pl.mm.notes.core.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;
import pl.mm.notes.core.controller.advice.CustomResponseEntityExceptionHandler;

/**
 * Extending this class will allow for a return exception as an HTTP error to the client.
 * It will be handled by {@link CustomResponseEntityExceptionHandler#handleAbstractHttpNotesRuntimeException(AbstractHttpNotesRuntimeException, WebRequest)}
 *
 * @see CustomResponseEntityExceptionHandler#handleAbstractHttpNotesRuntimeException(AbstractHttpNotesRuntimeException, WebRequest)
 */
public abstract class AbstractHttpNotesRuntimeException extends RuntimeException {
    private static final long serialVersionUID = -8644899519763734882L;
    private final HttpStatus httpStatus;

    protected AbstractHttpNotesRuntimeException(HttpStatus httpStatus) {
        super();
        this.httpStatus = httpStatus;
    }

    protected AbstractHttpNotesRuntimeException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    protected AbstractHttpNotesRuntimeException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    protected AbstractHttpNotesRuntimeException(Throwable cause, HttpStatus httpStatus) {
        super(cause);
        this.httpStatus = httpStatus;
    }

    protected AbstractHttpNotesRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, HttpStatus httpStatus) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
