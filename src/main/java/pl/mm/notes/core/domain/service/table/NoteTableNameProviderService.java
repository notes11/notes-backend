package pl.mm.notes.core.domain.service.table;

import org.springframework.stereotype.Service;
import pl.mm.notes.feature.note.domain.entity.Note;

@Service
public class NoteTableNameProviderService extends AbstractTableNameProviderService {
    public NoteTableNameProviderService() {
        super(Note.class);
    }
}
