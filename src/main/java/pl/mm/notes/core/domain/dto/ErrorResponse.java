package pl.mm.notes.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResponse {

    @Builder.Default
    private ZonedDateTime timestamp = ZonedDateTime.now();
    private int status;
    private String error;
    private String message;
    private String path;

}
