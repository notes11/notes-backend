package pl.mm.notes.core.domain.service.table;

import org.springframework.stereotype.Service;
import pl.mm.notes.feature.note.domain.entity.NoteImage;

@Service
public class NoteImageTableNameProviderService extends AbstractTableNameProviderService {
    protected NoteImageTableNameProviderService() {
        super(NoteImage.class);
    }
}
