package pl.mm.notes.core.domain.component;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class JwtConfiguration {

    private final String secretKey;
    private final Long tokenExpirationTime;
    private final String header;
    private final String tokenPrefix;

    private final ClaimsJws claimsJws = new ClaimsJws();

    public JwtConfiguration(@Value("${app.jwt.secret.key}") String secretKey,
                            @Value("${app.jwt.token.expatriation.time}") Long tokenExpirationTime,
                            @Value("${app.jwt.token.header.name}") String header,
                            @Value("${app.jwt.token.prefix}") String tokenPrefix) {
        this.secretKey = secretKey;
        this.tokenExpirationTime = tokenExpirationTime;
        this.header = header;
        this.tokenPrefix = tokenPrefix;
    }

    @Getter
    public static class ClaimsJws {

        private final String username = "username";
        private final String roles = "roles";

    }

}
