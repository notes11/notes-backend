package pl.mm.notes.core.domain.cqrs.query;

import org.modelmapper.ModelMapper;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.jutilities.cqrs.query.Query;
import pl.mm.jutilities.cqrs.query.QueryHandler;
import pl.mm.jutilities.cqrs.query.QueryResult;
import pl.mm.notes.core.domain.entity.BaseInfo;

public abstract class AbstractBaseInfoDbQueryHandler<Q extends Query, R extends QueryResult, RepositoryType extends
        CrudRepository<? extends BaseInfo, Long>> implements QueryHandler<Q, R> {

    protected final RepositoryType repository;
    protected final ModelMapper modelMapper;

    protected AbstractBaseInfoDbQueryHandler(RepositoryType repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public abstract R execute(Q query);
}
