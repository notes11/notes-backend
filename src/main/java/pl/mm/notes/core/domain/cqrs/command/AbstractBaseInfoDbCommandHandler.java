package pl.mm.notes.core.domain.cqrs.command;

import org.modelmapper.ModelMapper;
import org.springframework.data.repository.CrudRepository;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.notes.core.domain.entity.BaseInfo;
import pl.mm.notes.core.domain.service.table.TableNameProviderService;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.UUID;

public abstract class AbstractBaseInfoDbCommandHandler
        <C extends Command, R extends CommandResult, RepositoryType extends CrudRepository<? extends BaseInfo, Long>>
        extends AbstractDbCommandHandler<C, R, RepositoryType> {

    private final TableNameProviderService tableNameProviderService;

    protected AbstractBaseInfoDbCommandHandler(RepositoryType repository,
                                               ModelMapper modelMapper,
                                               TableNameProviderService tableNameProviderService) {
        super(repository, modelMapper);
        this.tableNameProviderService = tableNameProviderService;
    }

    protected <T extends BaseInfo> T create(T dbObject, String createdBy, String modifiedBy) {
        dbObject.setUuid(UUID.randomUUID());
        dbObject.setCreatedBy(createdBy);
        dbObject.setCreatedTimestamp(Instant.now());
        dbObject.setModifiedBy(modifiedBy);
        dbObject.setModificationTimestamp(Instant.now());
        dbObject.setEntryRelatedToTable(tableNameProviderService.getTableName());
        return dbObject;
    }

    protected <T extends BaseInfo> T update(T dbObject, String modifiedBy) {
        dbObject.setModifiedBy(modifiedBy);
        dbObject.setModificationTimestamp(Instant.now());
        return dbObject;
    }
}
