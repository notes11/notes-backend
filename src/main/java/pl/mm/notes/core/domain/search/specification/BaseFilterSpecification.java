package pl.mm.notes.core.domain.search.specification;

import org.springframework.data.jpa.domain.Specification;
import pl.mm.notes.core.domain.search.filter.BaseFilter;
import pl.mm.notes.core.domain.entity.BaseInfo;
import pl.mm.notes.core.domain.entity.BaseInfo_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class BaseFilterSpecification<F extends BaseFilter, T extends BaseInfo> implements Specification<T> {
    private static final long serialVersionUID = -7212386732361419225L;

    protected final F filter;

    public BaseFilterSpecification(F filter) {
        this.filter = filter;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public Predicate toPredicate(Root<T> r, CriteriaQuery<?> q, CriteriaBuilder cb) {
        final List<Predicate> predicates = new ArrayList<>();

        if (filter.getUuid() != null) {
            predicates.add(cb.equal(r.get(BaseInfo_.uuid), filter.getUuid()));
        }

        if (filter.getCreatedBy() != null) {
            predicates.add(cb.like(r.get(BaseInfo_.createdBy), filter.getCreatedBy() + "%"));
        }

        if (filter.getCreatedTimestampAfter() != null) {
            predicates.add(cb.greaterThanOrEqualTo(r.get(BaseInfo_.createdTimestamp), Instant.from(filter.getCreatedTimestampAfter())));
        }

        if (filter.getCreatedTimestampBefore() != null) {
            predicates.add(cb.lessThanOrEqualTo(r.get(BaseInfo_.createdTimestamp), Instant.from(filter.getCreatedTimestampBefore())));
        }

        if (filter.getModifiedBy() != null) {
            predicates.add(cb.like(r.get(BaseInfo_.modifiedBy), filter.getModifiedBy() + "%"));
        }

        if (filter.getModifiedTimestampAfter() != null) {
            predicates.add(cb.greaterThanOrEqualTo(r.get(BaseInfo_.modificationTimestamp), Instant.from(filter.getModifiedTimestampAfter())));
        }

        if (filter.getModifiedTimestampBefore() != null) {
            predicates.add(cb.lessThanOrEqualTo(r.get(BaseInfo_.modificationTimestamp), Instant.from(filter.getModifiedTimestampBefore())));
        }

        return cb.and(predicates.toArray(new Predicate[0]));
    }

}
