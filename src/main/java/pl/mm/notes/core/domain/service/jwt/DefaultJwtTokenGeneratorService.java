package pl.mm.notes.core.domain.service.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.component.JwtConfiguration;
import org.springframework.security.core.userdetails.User;

import java.util.Date;

@Service
public class DefaultJwtTokenGeneratorService implements JwtTokenGeneratorService {

    private final JwtConfiguration jwtConfiguration;

    public DefaultJwtTokenGeneratorService(JwtConfiguration jwtConfiguration) {
        this.jwtConfiguration = jwtConfiguration;
    }

    @Override
    public String generateJwtToken(Authentication authResult) {
        long currentTimeMillis = System.currentTimeMillis();
        User user = ((User) authResult.getPrincipal());

        return Jwts.builder()
                .setSubject(user.getUsername())
                .claim(jwtConfiguration.getClaimsJws().getUsername(), user.getUsername())
                .claim(jwtConfiguration.getClaimsJws().getRoles(), user.getAuthorities())
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + jwtConfiguration.getTokenExpirationTime()))
                .signWith(SignatureAlgorithm.HS256, jwtConfiguration.getSecretKey().getBytes())
                .compact();
    }

}

