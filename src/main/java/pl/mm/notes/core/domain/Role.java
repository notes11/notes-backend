package pl.mm.notes.core.domain;

public abstract class Role {

    private Role() {
    }

    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";

    public static final String[] ROLES = {ADMIN, USER};

}
