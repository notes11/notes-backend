package pl.mm.notes.core.domain.cqrs.query;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import pl.mm.jutilities.cqrs.query.QueryResult;

import java.util.List;

public class PageQueryResult<T> extends PageImpl<T> implements QueryResult {

    private static final long serialVersionUID = 7426799878391298963L;

    public PageQueryResult(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public PageQueryResult(List<T> content) {
        super(content);
    }
}
