package pl.mm.notes.core.domain.service.table;

import org.springframework.stereotype.Service;
import pl.mm.notes.core.domain.entity.BaseInfo;

@Service
public class BaseInfoTableNameProviderService extends AbstractTableNameProviderService {
    public BaseInfoTableNameProviderService() {
        super(BaseInfo.class);
    }
}
