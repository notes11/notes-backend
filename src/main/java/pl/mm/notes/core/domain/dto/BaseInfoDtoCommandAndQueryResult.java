package pl.mm.notes.core.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.query.QueryResult;

import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor(force = true)
@SuperBuilder
public class BaseInfoDtoCommandAndQueryResult implements CommandResult, QueryResult {
    private final UUID uuid;
    private final String createdBy;
    private final Instant createdTimestamp;
    private final String modifiedBy;
    private final Instant modificationTimestamp;
}
