package pl.mm.notes.core.domain.service.table;

import javax.persistence.Table;

public abstract class AbstractTableNameProviderService implements TableNameProviderService {

    private final Class<?> clazz;

    protected AbstractTableNameProviderService(Class<?> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String getTableName() {
        Table table = clazz.getAnnotation(Table.class);
        return table.name();
    }

}
