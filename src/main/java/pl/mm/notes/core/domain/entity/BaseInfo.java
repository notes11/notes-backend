package pl.mm.notes.core.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import pl.mm.notes.core.domain.entity.converter.UUIDConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.Instant;
import java.util.UUID;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "base_info")
public class BaseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "Id", unique = true, nullable = false, updatable = false)
    protected Long id;

    @Convert(converter = UUIDConverter.class)
    @Column(name = "Uuid", unique = true, nullable = false, updatable = false, length = 36)
    protected UUID uuid;

    @Version
    private Long version;

    @Column(name = "CreatedBy", nullable = false, updatable = false, length = 50)
    protected String createdBy;

    @CreationTimestamp
    @Column(name = "CreatedTimestamp", nullable = false, updatable = false)
    protected Instant createdTimestamp;

    @Column(name = "ModifiedBy", length = 50)
    protected String modifiedBy;

    @UpdateTimestamp
    @Column(name = "ModificationTimestamp")
    protected Instant modificationTimestamp;

    @Column(name = "EntryRelatedToTable", nullable = false, length = 50)
    protected String entryRelatedToTable;


}
