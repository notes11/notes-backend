package pl.mm.notes.core.controller.advice;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.mm.notes.core.domain.dto.ErrorResponse;
import pl.mm.notes.core.domain.exception.AbstractHttpNotesRuntimeException;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @SuppressWarnings("NullableProblems")
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest webRequest) {
        final List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getDefaultMessage());
        }

        final ErrorResponse apiError = ErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .error(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .message(String.join(System.lineSeparator(), errors))
                .path(((ServletWebRequest) webRequest).getRequest().getRequestURI())
                .build();
        return handleExceptionInternal(ex, apiError, headers, HttpStatus.BAD_REQUEST, webRequest);
    }

    @SuppressWarnings("NullableProblems")
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest webRequest) {
        final ErrorResponse apiError = ErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .error(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .message(ex.getLocalizedMessage())
                .path(((ServletWebRequest) webRequest).getRequest().getRequestURI())
                .build();
        return handleExceptionInternal(ex, apiError, headers, HttpStatus.BAD_REQUEST, webRequest);
    }

    @ExceptionHandler({AbstractHttpNotesRuntimeException.class})
    public ResponseEntity<Object> handleAbstractHttpNotesRuntimeException(AbstractHttpNotesRuntimeException exception,
                                                                          WebRequest webRequest) {
        final ErrorResponse apiError = ErrorResponse.builder()
                .status(exception.getHttpStatus().value())
                .error(exception.getHttpStatus().getReasonPhrase())
                .message(exception.getLocalizedMessage())
                .path(((ServletWebRequest) webRequest).getRequest().getRequestURI())
                .build();
        return new ResponseEntity<>(apiError, exception.getHttpStatus());
    }

}
