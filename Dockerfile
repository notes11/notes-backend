FROM openjdk:11-jre
COPY target/*.jar notes.jar
ENTRYPOINT ["java", "-jar", "notes.jar"]